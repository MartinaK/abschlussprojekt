﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;

namespace LaFamilia.Data
{
    public class DBConnectionSingleton
    {
        private static DBConnectionSingleton dbInstance = null;

        //**************************************************************************
        #region constructor


        private DBConnectionSingleton(string connectString)
        {
            this.ConnectString = connectString;
            this.Connection = new NpgsqlConnection(ConnectString);
        }

        #endregion

        //**************************************************************************
        #region properties

        private NpgsqlConnection connection;

        public NpgsqlConnection Connection
        {
            get { return connection; }
            set { connection = value; }
        }

        private string connectString;

        public string ConnectString
        {
            get { return connectString; }
            set { connectString = value; }
        }


        #endregion

        //**************************************************************************
        #region static methods

        public static DBConnectionSingleton GetDbInstance(string connectString)
        {
            if (dbInstance == null)
            {
                dbInstance = new DBConnectionSingleton(connectString);
            }
            return dbInstance;
        }

        #endregion

        //**************************************************************************
        #region public methods

        public int ExecuteCommand(string sqlCommand, params KeyValuePair<string, object>[] parameters)
        {
            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = this.connection;
            command.CommandText = sqlCommand;

            if (parameters != null)
            {
                foreach (KeyValuePair<string, object> parameter in parameters)
                    command.Parameters.AddWithValue(parameter.Key, (parameter.Value == null ? DBNull.Value : parameter.Value));
            }

            this.connection.Open();
            int ret = command.ExecuteNonQuery();
            this.connection.Close();

            return ret;
        }

        public decimal GetId(string tableName)
        {
            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = this.connection;
            command.CommandText = "select nextval('" + tableName + "_seq')";
            this.connection.Open();
						decimal ret = (decimal)((long)command.ExecuteScalar());
            this.connection.Close();

            return ret;
        }

				public T ExecuteQueryValue<T>(string sqlCommand, params KeyValuePair<string, object>[] parameters)
				{
					NpgsqlCommand command = new NpgsqlCommand();
					command.Connection = this.connection;
					command.CommandText = sqlCommand;

					if (parameters != null)
					{
						foreach (KeyValuePair<string, object> parameter in parameters)
							command.Parameters.AddWithValue(parameter.Key, parameter.Value);
					}

					this.connection.Open();

					object ret = command.ExecuteScalar();

				
					this.connection.Close();

					if (ret == DBNull.Value)
						return default(T);

					return (T)ret;
				}

        public List<T> ExecuteQuery<T>(string sqlCommand, params KeyValuePair<string, object>[] parameters)
        {
            return this.ExecuteQuery<T>(null, sqlCommand, parameters);
        }

        public List<T> ExecuteQuery<T>(DBItem parent, string sqlCommand, params KeyValuePair<string, object>[] parameters)
        {
            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = this.connection;
            command.CommandText = sqlCommand;

            if (parameters != null)
            {
                foreach (KeyValuePair<string, object> parameter in parameters)
                    command.Parameters.AddWithValue(parameter.Key, parameter.Value);
            }

            this.connection.Open();


            NpgsqlDataReader reader = command.ExecuteReader();
            List<T> retlist = new List<T>();
            Dictionary<string, object> row = null;

            while (reader.Read())
            {
                row = new Dictionary<string, object>();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    row.Add(reader.GetName(i), (reader.IsDBNull(i) ? null : reader.GetValue(i)));
                }

                if (parent == null)
                    retlist.Add((T)Activator.CreateInstance(typeof(T), this, row));
                else
                    retlist.Add((T)Activator.CreateInstance(typeof(T), parent, row));
            }
            reader.Close();
            this.connection.Close();

            return retlist;
        }

        #endregion
    }
}
