﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LaFamilia.Data
{
    //abstrakte Klasse - keine Instanz! - Person wird abgeleitet
    public abstract class DBItem
    {
        protected DBConnectionSingleton connection;
        protected Dictionary<string, object> rowData;
        protected DBItem parent;

        //**************************************************************************
        #region constructors

        public DBItem()
        {

        }

        public DBItem(DBConnectionSingleton connection, Dictionary<string, object> rowData)
		{
			this.connection = connection;
			this.rowData = rowData;
		}

        public DBItem(DBItem parent, Dictionary<string, object> rowData)
        {
            this.connection = parent.Connection;
            this.parent = parent;
            this.rowData = rowData;
        }

        public DBItem(DBItem parent)
        {
            this.connection = parent.Connection;
            this.parent = parent;
        }

        #endregion

        //**************************************************************************
        #region properties

        public Dictionary<string, object> RowData
        {
            get { return this.rowData; }
        }

        public DBConnectionSingleton Connection
        {
            get { return this.connection; }
        }

        #endregion

        //**************************************************************************
        #region public methods

        protected T GetValue<T>(string columnName)
        {
            if (rowData == null)
                return default(T);
            else
            {
                if (!rowData.ContainsKey(columnName))
                    rowData.Add(columnName, null);

                if (rowData[columnName] == null)
                    return default(T);
                else
                    return (T)rowData[columnName];
            }

        }

        protected void SetValue(string columnName, object val)
        {
            if (rowData == null)
            {
                this.rowData = new Dictionary<string, object>();
                this.rowData.Add(columnName, val);
            }
            else
                this.rowData[columnName] = val;
        }

        #endregion
    }
}
