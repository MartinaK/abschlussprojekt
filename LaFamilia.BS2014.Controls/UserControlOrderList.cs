﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Controls
{
    public partial class UserControlOrderList : UserControl
    {
        private OrderStatus currentFilter = OrderStatus.Status;
        private Person employee;
        private List<Order> showableOrders;
        private Order order;

        //********************************************************************************************
        #region constructors

        public UserControlOrderList(List<Order> showableOrders)
        {
            InitializeComponent();

            this.showableOrders = showableOrders;

            this.comboBoxStatus.DataSource = Enum.GetValues(typeof(OrderStatus));

            this.comboBoxName.Visible = false;

            if (showableOrders != null)
                LoadData(showableOrders);

            this.panelFilter.Size = new Size(525, 32);
                this.listViewOrder.BorderStyle = BorderStyle.None;
            this.linkLabelResetFilter.Location = new Point(230, 4);
        }

        public UserControlOrderList(List<Order> showableOrders, Person employee)
        {
            InitializeComponent();

            this.showableOrders = showableOrders;
            this.employee = employee;

            this.comboBoxStatus.DataSource = Enum.GetValues(typeof(OrderStatus));

            List<Person> persons = Person.GetList();
            foreach (Person person in persons.Where(p => p.IsCustomer == true))
            {
                this.comboBoxName.Items.Add(person.Lastname);
            }

            if (showableOrders != null)
                LoadData(showableOrders);

        }


        #endregion

        //*********************************************************************************
        #region properties

        #endregion


        //*********************************************************************************
        #region private methods

        private void LoadData(List<Order> showableOrders)
        {
            ListViewItem item = null;
            this.listViewOrder.Items.Clear();

            foreach (Order order in showableOrders)
            {
                item = new ListViewItem();
                item.Tag = order;
                item.Text = order.OrderNr.ToString();
                item.SubItems.Add(order.Person.IdentificationNumber.ToString());
                item.SubItems.Add(order.Person.Firstname + " " + order.Person.Lastname);
                item.SubItems.Add(order.OrderDate.Value.ToShortDateString());
                item.SubItems.Add(order.Status);
                item.SubItems.Add(order.LastUpdate.Value.ToShortDateString());
                item.SubItems.Add(order.NameShortcut);
                this.listViewOrder.Items.Add(item);
            }
        }

        #endregion


        private void listViewOrder_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewItem item = this.listViewOrder.GetItemAt(e.X, e.Y);

            Order order = (Order)item.Tag;

            FormOrder orderForm = new FormOrder(order, this.employee);
            orderForm.Show();
        }

        //Filter comboboxen
        private void comboBoxStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            OrderStatus filter = (OrderStatus)this.comboBoxStatus.SelectedItem;
            this.currentFilter = filter;

            this.showableOrders = Order.GetList();

            if (filter == OrderStatus.Status)
            {
                this.comboBoxStatus.Text = "Status";
                LoadData(this.showableOrders);
                return;
            }

            this.showableOrders = this.showableOrders.Where(o => (o.StatusValue & filter) == o.StatusValue).ToList<Order>();

            LoadData(this.showableOrders);
        }

        private void comboBoxName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string filterName = this.comboBoxName.SelectedItem.ToString();
            this.showableOrders = this.showableOrders.Where(o => o.Person.Lastname.Contains(filterName)).ToList<Order>();
            LoadData(this.showableOrders);
        }

        private void linkLabelResetFilter_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.comboBoxName.Text = "Nachname";
            this.comboBoxStatus.Text = "Status";
            LoadData(Order.GetList());
        }


        //*********************************************************************************
        #region public methods

        #endregion
    }

}
