﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Controls
{
	public partial class FormArticleDetails : Form
	{
		private Article article;
		private Category selectedCat;


		private UserControlArticleVariant AddAVControl()
		{
            if (this.article == null)
                this.article = new Article();
            
			UserControlArticleVariant ucArticleVariant = new UserControlArticleVariant(this.article);
			ucArticleVariant.ArticleVariantDeleted += new EventHandler(ucArticleVariant_ArticleVariantDeleted);
			ucArticleVariant.ArticleVariantCreated += new EventHandler(ucArticleVariant_ArticleVariantCreated);

			this.flowLayoutPanelVariants.Controls.Add(ucArticleVariant);
			return ucArticleVariant;
		}

		//***********************************************************************************
		#region constructors

		public FormArticleDetails(Category selectedCat)
		{
			InitializeComponent();

			this.selectedCat = selectedCat;

			UserControlHeader ucHeader = new UserControlHeader();
			this.panelHead.Controls.Add(ucHeader);

			UserControlFooter ucFooter = new UserControlFooter();
			this.panelFooter.Controls.Add(ucFooter);

			foreach (Supplier supplier in Supplier.GetList())
			{
				this.comboBoxSupplier.Items.Add(supplier.Company);

			}

			AddAVControl();
		}


		public FormArticleDetails(Article article)
		{
			InitializeComponent();

			this.article = article;

			UserControlHeader ucHeader = new UserControlHeader();
			this.panelHead.Controls.Add(ucHeader);

			UserControlFooter ucFooter = new UserControlFooter();
			this.panelFooter.Controls.Add(ucFooter);

			if (this.article.Type == "O")
				this.checkBoxOutlet.Checked = true;
			if (this.article.Type == "A")
				this.checkBoxAktion.Checked = true;

			this.pictureBox1.Image = this.article.ArticleImg;
			this.textBoxBrand.Text = this.article.Brand;
			this.textBoxArtNr.Text = this.article.ArticleNumber;
			this.textBoxTitle.Text = this.article.Title;
			this.textBoxPrice.Text = this.article.Price.ToString();
			this.textBoxUVP.Text = this.article.Uvp.ToString();
			this.comboBoxSupplier.Text = this.article.Supplier.Company;

			UserControlArticleVariant ucArticleVariant = null;
			if (this.article.Variants != null)
			{
				foreach (Variant v in this.article.Variants)
				{
					ucArticleVariant = new UserControlArticleVariant(v);
					ucArticleVariant.ArticleVariantDeleted += new EventHandler(ucArticleVariant_ArticleVariantDeleted);
					ucArticleVariant.ArticleVariantCreated += new EventHandler(ucArticleVariant_ArticleVariantCreated);
					this.flowLayoutPanelVariants.Controls.Add(ucArticleVariant);
				}
			}
			AddAVControl();
		}

		void ucArticleVariant_ArticleVariantCreated(object sender, EventArgs e)
		{
			AddAVControl().Focus();
		}

		//wird aufgerufen sobald in uc eine variante gelöscht wird
		void ucArticleVariant_ArticleVariantDeleted(object sender, EventArgs e)
		{
			UserControlArticleVariant ucArticleVariant = sender as UserControlArticleVariant;
			this.flowLayoutPanelVariants.Controls.Remove(ucArticleVariant);
		}

		#endregion

		//****************************************************************************
		#region properties


		#endregion

		//****************************************************************************
		#region events

		private void buttonSave_Click(object sender, EventArgs e)
		{
			this.textBoxArtNr.Focus();
			if (this.article == null)
				this.article = new Article();

			this.article.ArticleImg = null;
			if (this.pictureBox1 != null)
				this.article.ArticleImg = this.pictureBox1.Image;

			this.article.ArticleNumber = this.textBoxArtNr.Text;
			this.article.Title = this.textBoxTitle.Text;
			this.article.Brand = this.textBoxBrand.Text;
			if (this.checkBoxAktion.Checked)
				this.article.Type = "A";
			if (this.checkBoxOutlet.Checked)
				this.article.Type = "O";

			this.article.Uvp = Convert.ToDecimal(this.textBoxUVP.Text);
			this.article.Price = Convert.ToDecimal(this.textBoxPrice.Text);

			if (this.article.Status == null)
				this.article.Status = ArticleStatus.Verfügbar.ToString();


			foreach (Supplier supplier in Supplier.GetList().Where(s => s.Company == this.comboBoxSupplier.Text))
			{
				this.article.SupplierId = supplier.SupplierId;
			}


			this.article.Save();

			foreach (UserControlArticleVariant ucArticleVariant in this.flowLayoutPanelVariants.Controls)
			{
				if (ucArticleVariant.Variant != null)
				{
					ucArticleVariant.Variant.Save();

				}
			}

			if (this.selectedCat != null)
				this.selectedCat.ArticleAdd(this.article);

			this.article.RefreshCollections();

			this.DialogResult = DialogResult.OK;
		}


		private void pictureBox1_Click(object sender, EventArgs e)
		{
			OpenFileDialog dialog = new OpenFileDialog();
			if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				this.pictureBox1.Image = Image.FromFile(dialog.FileName);
			}
		}

		#endregion


	}
}
