﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Controls
{
	public partial class UserControlOrderedArticle : UserControl
	{

		private ArticleOrdered articleOrdered;


        public UserControlOrderedArticle(ArticleOrdered articleOrdered, bool admin)
		{
			InitializeComponent();

			this.articleOrdered = articleOrdered;

            if (!admin)
                this.textBoxCount.Enabled = false;

            this.textBoxCount.Text = this.articleOrdered.Quantity.ToString();
			this.labelNr.Text = this.articleOrdered.ArticleNumber.ToString();
			this.labelDescription.Text = this.articleOrdered.Title;
			this.labelPrice.Text = this.articleOrdered.Price * this.articleOrdered.Quantity + " € ";
            this.labelSize.Text = this.articleOrdered.Variant.AttriVal;
		}

		public int Save()
		{
			return this.articleOrdered.Quantity = int.Parse(this.textBoxCount.Text);
		}

	}
}
