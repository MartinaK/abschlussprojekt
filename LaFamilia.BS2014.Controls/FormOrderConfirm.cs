﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Controls
{
    public partial class FormOrderConfirm : Form
    {
        private Order order;

        //***************************************************************************
        #region constructors

        public FormOrderConfirm(Order order)
        {
            InitializeComponent();

            this.order = order;

            UserControlHeader ucHeader = new UserControlHeader();
            this.panelHeader.Controls.Add(ucHeader);

            UserControlFooter ucFooter = new UserControlFooter();
            this.panelFooter.Controls.Add(ucFooter);

            this.textBoxFirstName.Text = this.order.Person.Firstname;
            this.textBoxLastName.Text = this.order.Person.Lastname;
            this.textBoxStreet.Text = this.order.Person.Address.Street;
            this.textBoxPostalcode.Text = this.order.Person.Address.Postalcode;
            this.textBoxLocation.Text = this.order.Person.Address.Location;
            this.textBoxCountry.Text = this.order.Person.Address.Country;
            this.labelSum.Text = this.order.TotalCosts + " €";
            this.labelDeliveryDate.Text = DateTime.Now.AddDays(3).ToShortDateString();


            foreach (ArticleOrdered article in this.order.ArticlesOrdered)
            {
                UserControlOrderedArticle ucOrderedArticle = new UserControlOrderedArticle(article, true);
                this.flowLayoutPanelArticles.Controls.Add(ucOrderedArticle);
            }
        }

        #endregion

       

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.order.Status = OrderStatus.Offen.ToString();
            if (!String.IsNullOrEmpty(this.textBoxFirstName.Text) &&
                !String.IsNullOrEmpty(this.textBoxLastName.Text) &&
                !String.IsNullOrEmpty(this.textBoxStreet.Text) &&
                !String.IsNullOrEmpty(this.textBoxPostalcode.Text) &&
                !String.IsNullOrEmpty(this.textBoxLocation.Text) &&
                !String.IsNullOrEmpty(this.textBoxCountry.Text))
            {
                this.order.Person.Firstname = this.textBoxFirstName.Text;
                this.order.Person.Lastname = this.textBoxLastName.Text;
                this.order.Person.Address.Street = this.textBoxStreet.Text;
                this.order.Person.Address.Postalcode = this.textBoxPostalcode.Text;
                this.order.Person.Address.Location = this.textBoxLocation.Text;
                this.order.Person.Address.Country = this.textBoxCountry.Text;
                this.order.OrderDate = DateTime.Now;
                this.order.DeliveryDate = DateTime.Now.AddDays(3);
                this.order.LastUpdate = DateTime.Now;

                this.order.Save();
                FormThankYou thankyouForm = new FormThankYou(this.order);
                thankyouForm.Show();
                this.Close();
                this.DialogResult = DialogResult.OK;
                return;
            }
            this.labelNotification.Text = "Bitte geben Sie eine Adresse ein";
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
            FormShoppingCart shoppingCartForm = new FormShoppingCart(this.order);
            shoppingCartForm.Show();
            //this.DialogResult = DialogResult.Cancel;
        }


    }
}
