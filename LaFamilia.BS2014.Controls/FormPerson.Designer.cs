﻿namespace LaFamilia.BS2014.Controls
{
    partial class FormPerson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.panelHeader = new System.Windows.Forms.Panel();
			this.panelFooter = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.textBoxFirstName = new System.Windows.Forms.TextBox();
			this.textBoxLastName = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.buttonResetPassword = new System.Windows.Forms.Button();
			this.textBoxLoginName = new System.Windows.Forms.TextBox();
			this.textBoxStreet = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.textBoxPostalcode = new System.Windows.Forms.TextBox();
			this.labelPostalCode = new System.Windows.Forms.Label();
			this.textBoxCountry = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.textBoxLocation = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.textBoxPhone = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.textBoxMail = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.buttonSave = new System.Windows.Forms.Button();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.groupBoxPersonData = new System.Windows.Forms.GroupBox();
			this.label16 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.labelStreet = new System.Windows.Forms.Label();
			this.panelPersonType = new System.Windows.Forms.Panel();
			this.checkBoxEmployee = new System.Windows.Forms.CheckBox();
			this.checkBoxCustomer = new System.Windows.Forms.CheckBox();
			this.labelLastName = new System.Windows.Forms.Label();
			this.labelFirstName = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.textBoxIDNr = new System.Windows.Forms.TextBox();
			this.groupBoxLogin = new System.Windows.Forms.GroupBox();
			this.label12 = new System.Windows.Forms.Label();
			this.textBoxPasswordNew = new System.Windows.Forms.TextBox();
			this.textBoxPasswordOld = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.panelButtons = new System.Windows.Forms.Panel();
			this.groupBoxPersonData.SuspendLayout();
			this.panelPersonType.SuspendLayout();
			this.groupBoxLogin.SuspendLayout();
			this.panelButtons.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelHeader
			// 
			this.panelHeader.Location = new System.Drawing.Point(0, -1);
			this.panelHeader.Name = "panelHeader";
			this.panelHeader.Size = new System.Drawing.Size(438, 95);
			this.panelHeader.TabIndex = 0;
			// 
			// panelFooter
			// 
			this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panelFooter.Location = new System.Drawing.Point(0, 566);
			this.panelFooter.Name = "panelFooter";
			this.panelFooter.Size = new System.Drawing.Size(438, 25);
			this.panelFooter.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(15, 103);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(57, 15);
			this.label1.TabIndex = 0;
			this.label1.Text = "Vorname";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(177, 103);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(68, 15);
			this.label2.TabIndex = 1;
			this.label2.Text = "Nachname";
			// 
			// textBoxFirstName
			// 
			this.textBoxFirstName.Location = new System.Drawing.Point(12, 121);
			this.textBoxFirstName.Name = "textBoxFirstName";
			this.textBoxFirstName.Size = new System.Drawing.Size(150, 21);
			this.textBoxFirstName.TabIndex = 1;
			// 
			// textBoxLastName
			// 
			this.textBoxLastName.Location = new System.Drawing.Point(174, 121);
			this.textBoxLastName.Name = "textBoxLastName";
			this.textBoxLastName.Size = new System.Drawing.Size(150, 21);
			this.textBoxLastName.TabIndex = 2;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(16, 29);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(72, 15);
			this.label3.TabIndex = 4;
			this.label3.Text = "LoginName";
			// 
			// buttonResetPassword
			// 
			this.buttonResetPassword.BackColor = System.Drawing.Color.MediumVioletRed;
			this.buttonResetPassword.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.buttonResetPassword.ForeColor = System.Drawing.Color.White;
			this.buttonResetPassword.Location = new System.Drawing.Point(172, 47);
			this.buttonResetPassword.Name = "buttonResetPassword";
			this.buttonResetPassword.Size = new System.Drawing.Size(157, 23);
			this.buttonResetPassword.TabIndex = 1;
			this.buttonResetPassword.Text = "Passwort zurücksetzen";
			this.buttonResetPassword.UseVisualStyleBackColor = false;
			this.buttonResetPassword.Click += new System.EventHandler(this.buttonResetPassword_Click);
			// 
			// textBoxLoginName
			// 
			this.textBoxLoginName.Location = new System.Drawing.Point(12, 49);
			this.textBoxLoginName.Name = "textBoxLoginName";
			this.textBoxLoginName.Size = new System.Drawing.Size(148, 21);
			this.textBoxLoginName.TabIndex = 0;
			// 
			// textBoxStreet
			// 
			this.textBoxStreet.Location = new System.Drawing.Point(12, 166);
			this.textBoxStreet.Name = "textBoxStreet";
			this.textBoxStreet.Size = new System.Drawing.Size(315, 21);
			this.textBoxStreet.TabIndex = 3;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(15, 148);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(43, 15);
			this.label4.TabIndex = 8;
			this.label4.Text = "Straße";
			// 
			// textBoxPostalcode
			// 
			this.textBoxPostalcode.Location = new System.Drawing.Point(12, 211);
			this.textBoxPostalcode.Name = "textBoxPostalcode";
			this.textBoxPostalcode.Size = new System.Drawing.Size(79, 21);
			this.textBoxPostalcode.TabIndex = 4;
			// 
			// labelPostalCode
			// 
			this.labelPostalCode.AutoSize = true;
			this.labelPostalCode.Location = new System.Drawing.Point(12, 193);
			this.labelPostalCode.Name = "labelPostalCode";
			this.labelPostalCode.Size = new System.Drawing.Size(24, 15);
			this.labelPostalCode.TabIndex = 12;
			this.labelPostalCode.Text = "Plz";
			// 
			// textBoxCountry
			// 
			this.textBoxCountry.Location = new System.Drawing.Point(265, 211);
			this.textBoxCountry.Name = "textBoxCountry";
			this.textBoxCountry.Size = new System.Drawing.Size(141, 21);
			this.textBoxCountry.TabIndex = 6;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(269, 193);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(35, 15);
			this.label6.TabIndex = 14;
			this.label6.Text = "Land";
			// 
			// textBoxLocation
			// 
			this.textBoxLocation.Location = new System.Drawing.Point(108, 211);
			this.textBoxLocation.Name = "textBoxLocation";
			this.textBoxLocation.Size = new System.Drawing.Size(141, 21);
			this.textBoxLocation.TabIndex = 5;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(111, 192);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(23, 15);
			this.label7.TabIndex = 16;
			this.label7.Text = "Ort";
			// 
			// textBoxPhone
			// 
			this.textBoxPhone.Location = new System.Drawing.Point(12, 254);
			this.textBoxPhone.Name = "textBoxPhone";
			this.textBoxPhone.Size = new System.Drawing.Size(141, 21);
			this.textBoxPhone.TabIndex = 7;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(15, 235);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(48, 15);
			this.label8.TabIndex = 18;
			this.label8.Text = "Telefon";
			// 
			// textBoxMail
			// 
			this.textBoxMail.Location = new System.Drawing.Point(165, 254);
			this.textBoxMail.Name = "textBoxMail";
			this.textBoxMail.Size = new System.Drawing.Size(241, 21);
			this.textBoxMail.TabIndex = 8;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(169, 235);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(31, 15);
			this.label9.TabIndex = 20;
			this.label9.Text = "Mail";
			// 
			// buttonSave
			// 
			this.buttonSave.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.buttonSave.ForeColor = System.Drawing.Color.Black;
			this.buttonSave.Location = new System.Drawing.Point(4, 6);
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.Size = new System.Drawing.Size(81, 25);
			this.buttonSave.TabIndex = 0;
			this.buttonSave.Text = "Speichern";
			this.buttonSave.UseVisualStyleBackColor = false;
			this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
			// 
			// buttonCancel
			// 
			this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.buttonCancel.Location = new System.Drawing.Point(91, 6);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(81, 25);
			this.buttonCancel.TabIndex = 1;
			this.buttonCancel.Text = "Abbrechen";
			this.buttonCancel.UseVisualStyleBackColor = true;
			this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
			// 
			// groupBoxPersonData
			// 
			this.groupBoxPersonData.Controls.Add(this.label16);
			this.groupBoxPersonData.Controls.Add(this.label15);
			this.groupBoxPersonData.Controls.Add(this.label14);
			this.groupBoxPersonData.Controls.Add(this.labelStreet);
			this.groupBoxPersonData.Controls.Add(this.panelPersonType);
			this.groupBoxPersonData.Controls.Add(this.labelLastName);
			this.groupBoxPersonData.Controls.Add(this.labelFirstName);
			this.groupBoxPersonData.Controls.Add(this.label5);
			this.groupBoxPersonData.Controls.Add(this.textBoxIDNr);
			this.groupBoxPersonData.Controls.Add(this.label1);
			this.groupBoxPersonData.Controls.Add(this.label2);
			this.groupBoxPersonData.Controls.Add(this.textBoxFirstName);
			this.groupBoxPersonData.Controls.Add(this.textBoxLastName);
			this.groupBoxPersonData.Controls.Add(this.label4);
			this.groupBoxPersonData.Controls.Add(this.textBoxMail);
			this.groupBoxPersonData.Controls.Add(this.textBoxStreet);
			this.groupBoxPersonData.Controls.Add(this.label9);
			this.groupBoxPersonData.Controls.Add(this.textBoxPhone);
			this.groupBoxPersonData.Controls.Add(this.label8);
			this.groupBoxPersonData.Controls.Add(this.labelPostalCode);
			this.groupBoxPersonData.Controls.Add(this.textBoxLocation);
			this.groupBoxPersonData.Controls.Add(this.textBoxPostalcode);
			this.groupBoxPersonData.Controls.Add(this.label7);
			this.groupBoxPersonData.Controls.Add(this.label6);
			this.groupBoxPersonData.Controls.Add(this.textBoxCountry);
			this.groupBoxPersonData.Location = new System.Drawing.Point(12, 100);
			this.groupBoxPersonData.Name = "groupBoxPersonData";
			this.groupBoxPersonData.Size = new System.Drawing.Size(414, 286);
			this.groupBoxPersonData.TabIndex = 27;
			this.groupBoxPersonData.TabStop = false;
			this.groupBoxPersonData.Text = "Personen Daten";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.ForeColor = System.Drawing.Color.MediumVioletRed;
			this.label16.Location = new System.Drawing.Point(302, 192);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(12, 15);
			this.label16.TabIndex = 38;
			this.label16.Text = "*";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.ForeColor = System.Drawing.Color.MediumVioletRed;
			this.label15.Location = new System.Drawing.Point(133, 193);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(12, 15);
			this.label15.TabIndex = 37;
			this.label15.Text = "*";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.ForeColor = System.Drawing.Color.MediumVioletRed;
			this.label14.Location = new System.Drawing.Point(35, 193);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(12, 15);
			this.label14.TabIndex = 36;
			this.label14.Text = "*";
			// 
			// labelStreet
			// 
			this.labelStreet.AutoSize = true;
			this.labelStreet.ForeColor = System.Drawing.Color.MediumVioletRed;
			this.labelStreet.Location = new System.Drawing.Point(56, 148);
			this.labelStreet.Name = "labelStreet";
			this.labelStreet.Size = new System.Drawing.Size(12, 15);
			this.labelStreet.TabIndex = 35;
			this.labelStreet.Text = "*";
			// 
			// panelPersonType
			// 
			this.panelPersonType.Controls.Add(this.checkBoxEmployee);
			this.panelPersonType.Controls.Add(this.checkBoxCustomer);
			this.panelPersonType.Location = new System.Drawing.Point(9, 23);
			this.panelPersonType.Name = "panelPersonType";
			this.panelPersonType.Size = new System.Drawing.Size(161, 26);
			this.panelPersonType.TabIndex = 0;
			// 
			// checkBoxEmployee
			// 
			this.checkBoxEmployee.AutoSize = true;
			this.checkBoxEmployee.Location = new System.Drawing.Point(71, 4);
			this.checkBoxEmployee.Name = "checkBoxEmployee";
			this.checkBoxEmployee.Size = new System.Drawing.Size(85, 19);
			this.checkBoxEmployee.TabIndex = 32;
			this.checkBoxEmployee.Text = "Mitarbeiter";
			this.checkBoxEmployee.UseVisualStyleBackColor = true;
			this.checkBoxEmployee.CheckedChanged += new System.EventHandler(this.checkBoxEmployee_CheckedChanged);
			// 
			// checkBoxCustomer
			// 
			this.checkBoxCustomer.AutoSize = true;
			this.checkBoxCustomer.Location = new System.Drawing.Point(6, 4);
			this.checkBoxCustomer.Name = "checkBoxCustomer";
			this.checkBoxCustomer.Size = new System.Drawing.Size(62, 19);
			this.checkBoxCustomer.TabIndex = 33;
			this.checkBoxCustomer.Text = "Kunde";
			this.checkBoxCustomer.UseVisualStyleBackColor = true;
			this.checkBoxCustomer.CheckedChanged += new System.EventHandler(this.checkBoxCustomer_CheckedChanged);
			// 
			// labelLastName
			// 
			this.labelLastName.AutoSize = true;
			this.labelLastName.ForeColor = System.Drawing.Color.MediumVioletRed;
			this.labelLastName.Location = new System.Drawing.Point(244, 102);
			this.labelLastName.Name = "labelLastName";
			this.labelLastName.Size = new System.Drawing.Size(12, 15);
			this.labelLastName.TabIndex = 31;
			this.labelLastName.Text = "*";
			// 
			// labelFirstName
			// 
			this.labelFirstName.AutoSize = true;
			this.labelFirstName.ForeColor = System.Drawing.Color.MediumVioletRed;
			this.labelFirstName.Location = new System.Drawing.Point(70, 103);
			this.labelFirstName.Name = "labelFirstName";
			this.labelFirstName.Size = new System.Drawing.Size(12, 15);
			this.labelFirstName.TabIndex = 30;
			this.labelFirstName.Text = "*";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(15, 57);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(122, 15);
			this.label5.TabIndex = 29;
			this.label5.Text = "Identifikatiosnummer";
			// 
			// textBoxIDNr
			// 
			this.textBoxIDNr.Enabled = false;
			this.textBoxIDNr.Location = new System.Drawing.Point(12, 75);
			this.textBoxIDNr.Name = "textBoxIDNr";
			this.textBoxIDNr.Size = new System.Drawing.Size(150, 21);
			this.textBoxIDNr.TabIndex = 28;
			// 
			// groupBoxLogin
			// 
			this.groupBoxLogin.Controls.Add(this.label12);
			this.groupBoxLogin.Controls.Add(this.textBoxPasswordNew);
			this.groupBoxLogin.Controls.Add(this.textBoxPasswordOld);
			this.groupBoxLogin.Controls.Add(this.label3);
			this.groupBoxLogin.Controls.Add(this.label11);
			this.groupBoxLogin.Controls.Add(this.buttonResetPassword);
			this.groupBoxLogin.Controls.Add(this.textBoxLoginName);
			this.groupBoxLogin.Location = new System.Drawing.Point(12, 387);
			this.groupBoxLogin.Name = "groupBoxLogin";
			this.groupBoxLogin.Size = new System.Drawing.Size(414, 135);
			this.groupBoxLogin.TabIndex = 28;
			this.groupBoxLogin.TabStop = false;
			this.groupBoxLogin.Text = "Login Daten";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(178, 79);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(96, 15);
			this.label12.TabIndex = 10;
			this.label12.Text = "Neues Passwort";
			// 
			// textBoxPasswordNew
			// 
			this.textBoxPasswordNew.Location = new System.Drawing.Point(174, 99);
			this.textBoxPasswordNew.Name = "textBoxPasswordNew";
			this.textBoxPasswordNew.PasswordChar = '*';
			this.textBoxPasswordNew.Size = new System.Drawing.Size(153, 21);
			this.textBoxPasswordNew.TabIndex = 2;
			// 
			// textBoxPasswordOld
			// 
			this.textBoxPasswordOld.Location = new System.Drawing.Point(12, 99);
			this.textBoxPasswordOld.Name = "textBoxPasswordOld";
			this.textBoxPasswordOld.PasswordChar = '*';
			this.textBoxPasswordOld.Size = new System.Drawing.Size(150, 21);
			this.textBoxPasswordOld.TabIndex = 1;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(15, 79);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(86, 15);
			this.label11.TabIndex = 8;
			this.label11.Text = "Altes Passwort";
			// 
			// panelButtons
			// 
			this.panelButtons.Controls.Add(this.buttonSave);
			this.panelButtons.Controls.Add(this.buttonCancel);
			this.panelButtons.Location = new System.Drawing.Point(249, 528);
			this.panelButtons.Name = "panelButtons";
			this.panelButtons.Size = new System.Drawing.Size(177, 32);
			this.panelButtons.TabIndex = 29;
			// 
			// FormPerson
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.ClientSize = new System.Drawing.Size(438, 591);
			this.Controls.Add(this.panelButtons);
			this.Controls.Add(this.groupBoxLogin);
			this.Controls.Add(this.groupBoxPersonData);
			this.Controls.Add(this.panelFooter);
			this.Controls.Add(this.panelHeader);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Name = "FormPerson";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "LaFamilia | BS2014 | Personendaten";
			this.groupBoxPersonData.ResumeLayout(false);
			this.groupBoxPersonData.PerformLayout();
			this.panelPersonType.ResumeLayout(false);
			this.panelPersonType.PerformLayout();
			this.groupBoxLogin.ResumeLayout(false);
			this.groupBoxLogin.PerformLayout();
			this.panelButtons.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Panel panelFooter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxFirstName;
        private System.Windows.Forms.TextBox textBoxLastName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonResetPassword;
        private System.Windows.Forms.TextBox textBoxLoginName;
        private System.Windows.Forms.TextBox textBoxStreet;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxPostalcode;
        private System.Windows.Forms.Label labelPostalCode;
        private System.Windows.Forms.TextBox textBoxCountry;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxLocation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxPhone;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxMail;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.GroupBox groupBoxPersonData;
        private System.Windows.Forms.GroupBox groupBoxLogin;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxPasswordNew;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxPasswordOld;
        private System.Windows.Forms.TextBox textBoxIDNr;
        private System.Windows.Forms.Panel panelButtons;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelFirstName;
        private System.Windows.Forms.Label labelLastName;
        private System.Windows.Forms.CheckBox checkBoxEmployee;
        private System.Windows.Forms.CheckBox checkBoxCustomer;
        private System.Windows.Forms.Panel panelPersonType;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelStreet;
    }
}