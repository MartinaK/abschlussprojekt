﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;
using System.Threading;

namespace LaFamilia.BS2014.Controls
{
    public partial class FormPerson : Form
    {
        private string id;

        //**********************************************************************
        #region constructor

        public FormPerson(bool admin)
        {
            InitializeComponent();

            this.admin = admin;

            UserControlHeader ucHeader = new UserControlHeader();
            this.panelHeader.Controls.Add(ucHeader);

            UserControlFooter ucFooter = new UserControlFooter();
            this.panelFooter.Controls.Add(ucFooter);

        }

        public FormPerson(bool admin, Person person)
        {
            InitializeComponent();

            this.admin = admin;
            this.person = person;

            this.buttonResetPassword.Visible = false;
            this.panelPersonType.Visible = false;

            this.textBoxIDNr.Text = this.person.IdentificationNumber.ToString();
            this.checkBoxCustomer.Checked = this.person.IsCustomer.HasValue ? this.person.IsCustomer.Value : false;
            this.checkBoxEmployee.Checked = this.person.IsEmployee.HasValue ? this.person.IsEmployee.Value : false;
            this.textBoxFirstName.Text = this.person.Firstname;
            this.textBoxLastName.Text = this.person.Lastname;
            this.textBoxStreet.Text = this.person.Address.Street;
            this.textBoxPostalcode.Text = this.person.Address.Postalcode;
            this.textBoxLocation.Text = this.person.Address.Location;
            this.textBoxCountry.Text = this.person.Address.Country;
            this.textBoxPhone.Text = this.person.Phone;
            this.textBoxMail.Text = this.person.Mail;
            this.textBoxLoginName.Text = this.person.Username;

            if (this.person.Address != null)
                this.address = this.person.Address;

            UserControlHeader ucHeader = new UserControlHeader();
            this.panelHeader.Controls.Add(ucHeader);

            UserControlFooter ucFooter = new UserControlFooter();
            this.panelFooter.Controls.Add(ucFooter);
        }


        #endregion

        //**********************************************************************
        #region properties

        private Person person;

        public Person Person
        {
            get { return person; }
            set
            {
                person = value;
            }
        }

        private bool admin;

        public bool Admin
        {
            get { return admin; }
            set
            {
                admin = value;

                if (admin)
                {
                    this.buttonResetPassword.Visible = true;
                    this.panelPersonType.Visible = true;
                    this.groupBoxLogin.Location = new Point(12, 345);
                    this.groupBoxLogin.Height = 69;
                    this.panelButtons.Location = new Point(249, 419);
                    this.Height = 528;
                }
            }
        }

        private Address address;

        public Address Address
        {
            get { return address; }
            set { address = value; }
        }



        #endregion

        //**********************************************************************
        #region public methods


        #endregion

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (this.person == null)
                this.person = new Person();

            this.textBoxIDNr.Text = id;
            this.person.IdentificationNumber = this.textBoxIDNr.Text;
            this.person.IsCustomer = this.checkBoxCustomer.Checked;
            this.person.IsEmployee = this.checkBoxEmployee.Checked;
            this.person.Firstname = this.textBoxFirstName.Text;
            this.person.Lastname = this.textBoxLastName.Text;
            this.person.Phone = this.textBoxPhone.Text;
            this.person.Mail = this.textBoxMail.Text;
            this.person.Username = this.textBoxLoginName.Text;


            if (this.address == null)
                this.Address = new Address();

            this.address.PersonId = this.person.PersonId;
            this.address.Street = this.textBoxStreet.Text;
            this.address.Postalcode = this.textBoxPostalcode.Text;
            this.address.Location = this.textBoxLocation.Text;
            this.address.Country = this.textBoxCountry.Text;


            if ((this.checkBoxCustomer.Checked == false && this.checkBoxEmployee.Checked == false) ||
                String.IsNullOrEmpty(this.textBoxFirstName.Text) ||
                String.IsNullOrEmpty(this.textBoxLastName.Text) ||
                String.IsNullOrEmpty(this.textBoxLoginName.Text) ||
                String.IsNullOrEmpty(this.textBoxStreet.Text) ||
                String.IsNullOrEmpty(this.textBoxPostalcode.Text) ||
                String.IsNullOrEmpty(this.textBoxLocation.Text) ||
                String.IsNullOrEmpty(this.textBoxCountry.Text))
            {
                MessageBox.Show("Bitte geben Sie alle Pflichtfelder (*) ein!");
                return;
            }

            //Logindaten ändern
            if (!String.IsNullOrEmpty(this.textBoxLoginName.Text) &&
                !String.IsNullOrEmpty(this.textBoxPasswordNew.Text))
            {
                if (this.person.Password == null && this.textBoxPasswordOld.Text == String.Empty)
                    this.person.Password = GetMd5Hash(this.textBoxPasswordNew.Text);

                if ((this.person.Username == this.textBoxLoginName.Text) && VerifyMd5Hash(GetMd5Hash(this.textBoxPasswordOld.Text), this.Person.Password))
                {
                    this.person.Password = GetMd5Hash(this.textBoxPasswordNew.Text);
                    MessageBox.Show("Passwort geändert!");
                }
 
            }
            this.address.Save();
            this.person.Address = this.address;

            this.person.Save();
            this.address.PersonId = this.person.PersonId;
            this.address.Save();
            this.Hide();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //**********************************************************************
        #region private methods

        private string GetMd5Hash(string value)
        {
            //erstellt eine Instanz vom md5 Algorithmus
            var md5 = System.Security.Cryptography.MD5.Create();

            //konvertiert den string in ein byte Array
            byte[] data = Encoding.Default.GetBytes(value);
            //erzeugt den Hash
            byte[] md5HashBytes = md5.ComputeHash(data);

            //erzeugt einen StringBuilder um die bytes zu speichern und erzeugt daraus einen String
            StringBuilder builder = new StringBuilder(md5HashBytes.Length * 2);

            //formatiert ein jedes einzelne Byte als hexadezimal string
            foreach (var md5Byte in md5HashBytes)
                builder.Append(md5Byte.ToString("X2"));
            return builder.ToString();
        }

        private bool VerifyMd5Hash(string input, string hash)
        {
            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (comparer.Compare(input, hash) != 0)
            {
                return false;
            }

            return true;

        }

        private string CreateID(string personType)
        {
            //Id Array - jede Kundennr. startet mit 2
            int[] id = new int[6];
            Random zahlen = new Random();

            if (personType == "customer")
            {
                id[0] = 2;

                for (int i = 1; i < id.Length; i++)
                    id[i] = zahlen.Next(1, 10);

                return String.Join("", new List<int>(id).ConvertAll(i => i.ToString()).ToArray());
            }

            id[0] = 1;

            for (int i = 1; i < id.Length; i++)
                id[i] = zahlen.Next(1, 10);

            return String.Join("", new List<int>(id).ConvertAll(i => i.ToString()).ToArray());
        }

        #endregion

        private void checkBoxCustomer_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBoxCustomer.Checked)
            {
                id = CreateID("customer");

                foreach (Person person in Person.GetList().Where(p => p.IsCustomer.HasValue == true))
                {
                    //wenn die IdentificationNumber schon existiert erzeuge eine neue ID
                    if (person.IdentificationNumber.Equals(id))
                        CreateID("customer");
                }
                this.textBoxIDNr.Text = id;
                return;
            }
            this.textBoxIDNr.Text = String.Empty;
        }

        private void checkBoxEmployee_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBoxEmployee.Checked)
            {
                id = CreateID("employee");

                foreach (Person person in Person.GetList().Where(p => p.IsEmployee.HasValue == true))
                {
                    //wenn die IdentificationNumber schon existiert erzeuge eine neue ID
                    if (person.IdentificationNumber.Equals(id))
                        CreateID("employee");
                }
                this.textBoxIDNr.Text = id;
                return;
            }
            this.textBoxIDNr.Text = String.Empty;
        }

        private void buttonResetPassword_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(this.textBoxLoginName.Text) &&
                !String.IsNullOrEmpty(this.textBoxPasswordOld.Text))
            {
                if ((this.person.Username == this.textBoxLoginName.Text) && (this.person.Password == this.textBoxPasswordOld.Text))
                    this.person.Password = String.Empty;
            }
        }


    }
}
