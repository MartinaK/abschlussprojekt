﻿namespace LaFamilia.BS2014.Controls
{
    partial class FormInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelHeader = new System.Windows.Forms.Panel();
            this.panelFooter = new System.Windows.Forms.Panel();
            this.panelOrderDetails = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.textBoxLastName = new System.Windows.Forms.TextBox();
            this.textBoxStreet = new System.Windows.Forms.TextBox();
            this.textBoxPostalcode = new System.Windows.Forms.TextBox();
            this.textBoxCountry = new System.Windows.Forms.TextBox();
            this.textBoxLocation = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelIdentificationNr = new System.Windows.Forms.Label();
            this.labelInvoiceDate = new System.Windows.Forms.Label();
            this.labelIdentificationNr_Text = new System.Windows.Forms.Label();
            this.labelInvoiceNr = new System.Windows.Forms.Label();
            this.labelInvoiceNr_Text = new System.Windows.Forms.Label();
            this.labelInvoiceDate_Text = new System.Windows.Forms.Label();
            this.flowLayoutPanelArticles = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelOrder_Nr = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBoxPrint = new System.Windows.Forms.PictureBox();
            this.labelStatus = new System.Windows.Forms.Label();
            this.comboBoxStatus = new System.Windows.Forms.ComboBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.printDocumentInvoice = new System.Drawing.Printing.PrintDocument();
            this.panelOrderDetails.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // panelHeader
            // 
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(660, 95);
            this.panelHeader.TabIndex = 32;
            // 
            // panelFooter
            // 
            this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFooter.Location = new System.Drawing.Point(0, 717);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(660, 29);
            this.panelFooter.TabIndex = 33;
            // 
            // panelOrderDetails
            // 
            this.panelOrderDetails.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelOrderDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelOrderDetails.Controls.Add(this.label1);
            this.panelOrderDetails.Controls.Add(this.panel2);
            this.panelOrderDetails.Controls.Add(this.panel3);
            this.panelOrderDetails.Controls.Add(this.flowLayoutPanelArticles);
            this.panelOrderDetails.Controls.Add(this.tableLayoutPanel1);
            this.panelOrderDetails.Controls.Add(this.labelOrder_Nr);
            this.panelOrderDetails.Controls.Add(this.panel1);
            this.panelOrderDetails.Location = new System.Drawing.Point(7, 105);
            this.panelOrderDetails.Name = "panelOrderDetails";
            this.panelOrderDetails.Size = new System.Drawing.Size(646, 575);
            this.panelOrderDetails.TabIndex = 34;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 20);
            this.label1.TabIndex = 39;
            this.label1.Text = "Rechnung";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.textBoxFirstName);
            this.panel2.Controls.Add(this.textBoxLastName);
            this.panel2.Controls.Add(this.textBoxStreet);
            this.panel2.Controls.Add(this.textBoxPostalcode);
            this.panel2.Controls.Add(this.textBoxCountry);
            this.panel2.Controls.Add(this.textBoxLocation);
            this.panel2.Location = new System.Drawing.Point(405, 10);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(227, 135);
            this.panel2.TabIndex = 38;
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Location = new System.Drawing.Point(3, 4);
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(95, 21);
            this.textBoxFirstName.TabIndex = 0;
            // 
            // textBoxLastName
            // 
            this.textBoxLastName.Location = new System.Drawing.Point(108, 4);
            this.textBoxLastName.Name = "textBoxLastName";
            this.textBoxLastName.Size = new System.Drawing.Size(111, 21);
            this.textBoxLastName.TabIndex = 1;
            // 
            // textBoxStreet
            // 
            this.textBoxStreet.Location = new System.Drawing.Point(3, 36);
            this.textBoxStreet.Name = "textBoxStreet";
            this.textBoxStreet.Size = new System.Drawing.Size(216, 21);
            this.textBoxStreet.TabIndex = 2;
            // 
            // textBoxPostalcode
            // 
            this.textBoxPostalcode.Location = new System.Drawing.Point(3, 69);
            this.textBoxPostalcode.Name = "textBoxPostalcode";
            this.textBoxPostalcode.Size = new System.Drawing.Size(61, 21);
            this.textBoxPostalcode.TabIndex = 3;
            // 
            // textBoxCountry
            // 
            this.textBoxCountry.Location = new System.Drawing.Point(3, 102);
            this.textBoxCountry.Name = "textBoxCountry";
            this.textBoxCountry.Size = new System.Drawing.Size(216, 21);
            this.textBoxCountry.TabIndex = 5;
            // 
            // textBoxLocation
            // 
            this.textBoxLocation.Location = new System.Drawing.Point(74, 69);
            this.textBoxLocation.Name = "textBoxLocation";
            this.textBoxLocation.Size = new System.Drawing.Size(145, 21);
            this.textBoxLocation.TabIndex = 4;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.labelIdentificationNr);
            this.panel3.Controls.Add(this.labelInvoiceDate);
            this.panel3.Controls.Add(this.labelIdentificationNr_Text);
            this.panel3.Controls.Add(this.labelInvoiceNr);
            this.panel3.Controls.Add(this.labelInvoiceNr_Text);
            this.panel3.Controls.Add(this.labelInvoiceDate_Text);
            this.panel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(9, 10);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(237, 72);
            this.panel3.TabIndex = 25;
            // 
            // labelIdentificationNr
            // 
            this.labelIdentificationNr.AutoSize = true;
            this.labelIdentificationNr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIdentificationNr.ForeColor = System.Drawing.Color.Black;
            this.labelIdentificationNr.Location = new System.Drawing.Point(3, 7);
            this.labelIdentificationNr.Name = "labelIdentificationNr";
            this.labelIdentificationNr.Size = new System.Drawing.Size(77, 15);
            this.labelIdentificationNr.TabIndex = 2;
            this.labelIdentificationNr.Text = "Kundennr.:";
            // 
            // labelInvoiceDate
            // 
            this.labelInvoiceDate.AutoSize = true;
            this.labelInvoiceDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInvoiceDate.ForeColor = System.Drawing.Color.Black;
            this.labelInvoiceDate.Location = new System.Drawing.Point(4, 47);
            this.labelInvoiceDate.Name = "labelInvoiceDate";
            this.labelInvoiceDate.Size = new System.Drawing.Size(108, 15);
            this.labelInvoiceDate.TabIndex = 17;
            this.labelInvoiceDate.Text = "Rechnungsdatum:";
            // 
            // labelIdentificationNr_Text
            // 
            this.labelIdentificationNr_Text.AutoSize = true;
            this.labelIdentificationNr_Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIdentificationNr_Text.ForeColor = System.Drawing.Color.Black;
            this.labelIdentificationNr_Text.Location = new System.Drawing.Point(118, 7);
            this.labelIdentificationNr_Text.Name = "labelIdentificationNr_Text";
            this.labelIdentificationNr_Text.Size = new System.Drawing.Size(37, 15);
            this.labelIdentificationNr_Text.TabIndex = 10;
            this.labelIdentificationNr_Text.Text = "xxxxx";
            // 
            // labelInvoiceNr
            // 
            this.labelInvoiceNr.AutoSize = true;
            this.labelInvoiceNr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInvoiceNr.ForeColor = System.Drawing.Color.Black;
            this.labelInvoiceNr.Location = new System.Drawing.Point(3, 27);
            this.labelInvoiceNr.Name = "labelInvoiceNr";
            this.labelInvoiceNr.Size = new System.Drawing.Size(100, 15);
            this.labelInvoiceNr.TabIndex = 14;
            this.labelInvoiceNr.Text = "Rechnungsnr.:";
            // 
            // labelInvoiceNr_Text
            // 
            this.labelInvoiceNr_Text.AutoSize = true;
            this.labelInvoiceNr_Text.ForeColor = System.Drawing.Color.Black;
            this.labelInvoiceNr_Text.Location = new System.Drawing.Point(119, 27);
            this.labelInvoiceNr_Text.Name = "labelInvoiceNr_Text";
            this.labelInvoiceNr_Text.Size = new System.Drawing.Size(37, 15);
            this.labelInvoiceNr_Text.TabIndex = 23;
            this.labelInvoiceNr_Text.Text = "xxxxx";
            // 
            // labelInvoiceDate_Text
            // 
            this.labelInvoiceDate_Text.AutoSize = true;
            this.labelInvoiceDate_Text.Location = new System.Drawing.Point(121, 47);
            this.labelInvoiceDate_Text.Name = "labelInvoiceDate_Text";
            this.labelInvoiceDate_Text.Size = new System.Drawing.Size(31, 15);
            this.labelInvoiceDate_Text.TabIndex = 22;
            this.labelInvoiceDate_Text.Text = "xxxx";
            // 
            // flowLayoutPanelArticles
            // 
            this.flowLayoutPanelArticles.Location = new System.Drawing.Point(8, 222);
            this.flowLayoutPanelArticles.Name = "flowLayoutPanelArticles";
            this.flowLayoutPanelArticles.Size = new System.Drawing.Size(624, 348);
            this.flowLayoutPanelArticles.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.DimGray;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 58F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 117F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 292F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 4, 0);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(9, 189);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(623, 33);
            this.tableLayoutPanel1.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(474, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 26);
            this.label4.TabIndex = 3;
            this.label4.Text = "Gr.";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(181, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(286, 31);
            this.label5.TabIndex = 1;
            this.label5.Text = "Beschreibung";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(63, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 26);
            this.label6.TabIndex = 5;
            this.label6.Text = "Artikelnr.";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(4, 1);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 26);
            this.label10.TabIndex = 6;
            this.label10.Text = "Anz.";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(550, 1);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 26);
            this.label7.TabIndex = 4;
            this.label7.Text = "Preis";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelOrder_Nr
            // 
            this.labelOrder_Nr.AutoSize = true;
            this.labelOrder_Nr.Location = new System.Drawing.Point(3, 28);
            this.labelOrder_Nr.Name = "labelOrder_Nr";
            this.labelOrder_Nr.Size = new System.Drawing.Size(0, 15);
            this.labelOrder_Nr.TabIndex = 21;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.pictureBoxPrint);
            this.panel1.Controls.Add(this.labelStatus);
            this.panel1.Controls.Add(this.comboBoxStatus);
            this.panel1.Location = new System.Drawing.Point(9, 154);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(623, 36);
            this.panel1.TabIndex = 36;
            // 
            // pictureBoxPrint
            // 
            this.pictureBoxPrint.BackColor = System.Drawing.Color.White;
            this.pictureBoxPrint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxPrint.Image = global::LaFamilia.BS2014.Controls.Properties.Resources.Drucker;
            this.pictureBoxPrint.Location = new System.Drawing.Point(590, 6);
            this.pictureBoxPrint.Name = "pictureBoxPrint";
            this.pictureBoxPrint.Size = new System.Drawing.Size(25, 25);
            this.pictureBoxPrint.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxPrint.TabIndex = 36;
            this.pictureBoxPrint.TabStop = false;
            this.pictureBoxPrint.Click += new System.EventHandler(this.pictureBoxPrint_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(5, 11);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(86, 15);
            this.labelStatus.TabIndex = 35;
            this.labelStatus.Text = "Status ändern:";
            // 
            // comboBoxStatus
            // 
            this.comboBoxStatus.FormattingEnabled = true;
            this.comboBoxStatus.Location = new System.Drawing.Point(97, 8);
            this.comboBoxStatus.Name = "comboBoxStatus";
            this.comboBoxStatus.Size = new System.Drawing.Size(140, 23);
            this.comboBoxStatus.TabIndex = 0;
            this.comboBoxStatus.Text = "Status";
            // 
            // buttonSave
            // 
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSave.Location = new System.Drawing.Point(494, 688);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 0;
            this.buttonSave.Text = "Speichern";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCancel.Location = new System.Drawing.Point(578, 688);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Abbrechen";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // printDocumentInvoice
            // 
            this.printDocumentInvoice.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocumentInvoice_PrintPage);
            // 
            // FormInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 746);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.panelOrderDetails);
            this.Controls.Add(this.panelFooter);
            this.Controls.Add(this.panelHeader);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormInvoice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LaFamilia | BS2014 | Rechnungsnr.:";
            this.panelOrderDetails.ResumeLayout(false);
            this.panelOrderDetails.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPrint)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Panel panelFooter;
        private System.Windows.Forms.Panel panelOrderDetails;
        private System.Windows.Forms.TextBox textBoxLastName;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelArticles;
        private System.Windows.Forms.TextBox textBoxCountry;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxLocation;
        private System.Windows.Forms.TextBox textBoxPostalcode;
        private System.Windows.Forms.TextBox textBoxStreet;
        private System.Windows.Forms.TextBox textBoxFirstName;
        private System.Windows.Forms.Label labelInvoiceDate_Text;
        private System.Windows.Forms.Label labelOrder_Nr;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelIdentificationNr;
        private System.Windows.Forms.Label labelIdentificationNr_Text;
        private System.Windows.Forms.Label labelInvoiceNr;
        private System.Windows.Forms.Label labelInvoiceNr_Text;
        private System.Windows.Forms.Label labelInvoiceDate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.ComboBox comboBoxStatus;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonCancel;
        private System.Drawing.Printing.PrintDocument printDocumentInvoice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBoxPrint;
    }
}