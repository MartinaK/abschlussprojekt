﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Controls
{
	public partial class UserControlOrderDetails : UserControl
	{
        private Order order;

		public UserControlOrderDetails()
		{
			InitializeComponent();
		}

        public UserControlOrderDetails(Order order)
        {
            InitializeComponent();
            this.order = order;

            this.labelName.Text = this.order.Person.Firstname + " " + this.order.Person.Lastname;

            
        }

	}
}
