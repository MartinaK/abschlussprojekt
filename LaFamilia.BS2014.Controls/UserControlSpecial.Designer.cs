﻿namespace LaFamilia.BS2014.Controls
{
    partial class UserControlSpecial
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
			this.pictureBoxSpecial = new System.Windows.Forms.PictureBox();
			this.label_name = new System.Windows.Forms.Label();
			this.label_description = new System.Windows.Forms.Label();
			this.label_dateFrom = new System.Windows.Forms.Label();
			this.label_dateTo = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpecial)).BeginInit();
			this.SuspendLayout();
			// 
			// pictureBoxSpecial
			// 
			this.pictureBoxSpecial.Cursor = System.Windows.Forms.Cursors.Arrow;
			this.pictureBoxSpecial.Location = new System.Drawing.Point(3, 3);
			this.pictureBoxSpecial.Name = "pictureBoxSpecial";
			this.pictureBoxSpecial.Size = new System.Drawing.Size(136, 39);
			this.pictureBoxSpecial.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBoxSpecial.TabIndex = 0;
			this.pictureBoxSpecial.TabStop = false;
			// 
			// label_name
			// 
			this.label_name.AutoSize = true;
			this.label_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label_name.Location = new System.Drawing.Point(183, 16);
			this.label_name.Name = "label_name";
			this.label_name.Size = new System.Drawing.Size(89, 15);
			this.label_name.TabIndex = 1;
			this.label_name.Text = "Aktionsname";
			// 
			// label_description
			// 
			this.label_description.AutoSize = true;
			this.label_description.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label_description.Location = new System.Drawing.Point(287, 18);
			this.label_description.Name = "label_description";
			this.label_description.Size = new System.Drawing.Size(72, 13);
			this.label_description.TabIndex = 2;
			this.label_description.Text = "Beschreibung";
			// 
			// label_dateFrom
			// 
			this.label_dateFrom.AutoSize = true;
			this.label_dateFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label_dateFrom.Location = new System.Drawing.Point(494, 18);
			this.label_dateFrom.Name = "label_dateFrom";
			this.label_dateFrom.Size = new System.Drawing.Size(59, 13);
			this.label_dateFrom.TabIndex = 3;
			this.label_dateFrom.Text = "Datum von";
			// 
			// label_dateTo
			// 
			this.label_dateTo.AutoSize = true;
			this.label_dateTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label_dateTo.Location = new System.Drawing.Point(549, 18);
			this.label_dateTo.Name = "label_dateTo";
			this.label_dateTo.Size = new System.Drawing.Size(54, 13);
			this.label_dateTo.TabIndex = 4;
			this.label_dateTo.Text = "Datum bis";
			// 
			// UserControlSpecial
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.label_dateTo);
			this.Controls.Add(this.label_dateFrom);
			this.Controls.Add(this.label_description);
			this.Controls.Add(this.label_name);
			this.Controls.Add(this.pictureBoxSpecial);
			this.Cursor = System.Windows.Forms.Cursors.Hand;
			this.Name = "UserControlSpecial";
			this.Size = new System.Drawing.Size(723, 47);
			this.Click += new System.EventHandler(this.UserControlSpecial_Click);
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpecial)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxSpecial;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.Label label_description;
        private System.Windows.Forms.Label label_dateFrom;
        private System.Windows.Forms.Label label_dateTo;
    }
}
