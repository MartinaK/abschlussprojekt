﻿namespace LaFamilia.BS2014.Controls
{
    partial class FormOrder
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelOrderDetails = new System.Windows.Forms.Panel();
            this.labelOrder = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxPostalcode = new System.Windows.Forms.TextBox();
            this.textBoxLastName = new System.Windows.Forms.TextBox();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.textBoxStreet = new System.Windows.Forms.TextBox();
            this.textBoxCountry = new System.Windows.Forms.TextBox();
            this.textBoxLocation = new System.Windows.Forms.TextBox();
            this.flowLayoutPanelArticles = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelOrder_Date = new System.Windows.Forms.Label();
            this.labelOrder_Nr = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelIdentificationNr = new System.Windows.Forms.Label();
            this.labelIdentificationNr_Text = new System.Windows.Forms.Label();
            this.labelOrderNr = new System.Windows.Forms.Label();
            this.labelOrderNr_Text = new System.Windows.Forms.Label();
            this.labelOrderDate = new System.Windows.Forms.Label();
            this.labelOrderDate_Text = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBoxPrint = new System.Windows.Forms.PictureBox();
            this.buttonCreateInvoice = new System.Windows.Forms.Button();
            this.labelStatus = new System.Windows.Forms.Label();
            this.comboBoxStatus = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panelFooter = new System.Windows.Forms.Panel();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.printDocumentOrder = new System.Drawing.Printing.PrintDocument();
            this.panelOrderDetails.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // panelOrderDetails
            // 
            this.panelOrderDetails.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelOrderDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelOrderDetails.Controls.Add(this.labelOrder);
            this.panelOrderDetails.Controls.Add(this.panel2);
            this.panelOrderDetails.Controls.Add(this.flowLayoutPanelArticles);
            this.panelOrderDetails.Controls.Add(this.tableLayoutPanel1);
            this.panelOrderDetails.Controls.Add(this.labelOrder_Date);
            this.panelOrderDetails.Controls.Add(this.labelOrder_Nr);
            this.panelOrderDetails.Controls.Add(this.panel3);
            this.panelOrderDetails.Controls.Add(this.panel1);
            this.panelOrderDetails.Location = new System.Drawing.Point(15, 99);
            this.panelOrderDetails.Name = "panelOrderDetails";
            this.panelOrderDetails.Size = new System.Drawing.Size(633, 587);
            this.panelOrderDetails.TabIndex = 9;
            // 
            // labelOrder
            // 
            this.labelOrder.AutoSize = true;
            this.labelOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOrder.ForeColor = System.Drawing.Color.DimGray;
            this.labelOrder.Location = new System.Drawing.Point(10, 111);
            this.labelOrder.Name = "labelOrder";
            this.labelOrder.Size = new System.Drawing.Size(86, 18);
            this.labelOrder.TabIndex = 37;
            this.labelOrder.Text = "Bestellung";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.textBoxPostalcode);
            this.panel2.Controls.Add(this.textBoxLastName);
            this.panel2.Controls.Add(this.textBoxFirstName);
            this.panel2.Controls.Add(this.textBoxStreet);
            this.panel2.Controls.Add(this.textBoxCountry);
            this.panel2.Controls.Add(this.textBoxLocation);
            this.panel2.Location = new System.Drawing.Point(426, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(199, 123);
            this.panel2.TabIndex = 0;
            // 
            // textBoxPostalcode
            // 
            this.textBoxPostalcode.Location = new System.Drawing.Point(3, 63);
            this.textBoxPostalcode.Name = "textBoxPostalcode";
            this.textBoxPostalcode.Size = new System.Drawing.Size(53, 21);
            this.textBoxPostalcode.TabIndex = 3;
            // 
            // textBoxLastName
            // 
            this.textBoxLastName.Location = new System.Drawing.Point(93, 7);
            this.textBoxLastName.Name = "textBoxLastName";
            this.textBoxLastName.Size = new System.Drawing.Size(96, 21);
            this.textBoxLastName.TabIndex = 1;
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Location = new System.Drawing.Point(3, 7);
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(82, 21);
            this.textBoxFirstName.TabIndex = 0;
            // 
            // textBoxStreet
            // 
            this.textBoxStreet.Location = new System.Drawing.Point(3, 34);
            this.textBoxStreet.Name = "textBoxStreet";
            this.textBoxStreet.Size = new System.Drawing.Size(186, 21);
            this.textBoxStreet.TabIndex = 2;
            // 
            // textBoxCountry
            // 
            this.textBoxCountry.Location = new System.Drawing.Point(3, 92);
            this.textBoxCountry.Name = "textBoxCountry";
            this.textBoxCountry.Size = new System.Drawing.Size(186, 21);
            this.textBoxCountry.TabIndex = 5;
            // 
            // textBoxLocation
            // 
            this.textBoxLocation.Location = new System.Drawing.Point(64, 63);
            this.textBoxLocation.Name = "textBoxLocation";
            this.textBoxLocation.Size = new System.Drawing.Size(125, 21);
            this.textBoxLocation.TabIndex = 4;
            // 
            // flowLayoutPanelArticles
            // 
            this.flowLayoutPanelArticles.Location = new System.Drawing.Point(8, 201);
            this.flowLayoutPanelArticles.Name = "flowLayoutPanelArticles";
            this.flowLayoutPanelArticles.Size = new System.Drawing.Size(617, 379);
            this.flowLayoutPanelArticles.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.DimGray;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 336F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.01493F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.98507F));
            this.tableLayoutPanel1.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 4, 0);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(9, 182);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(616, 29);
            this.tableLayoutPanel1.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(485, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Gr.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(148, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 15);
            this.label5.TabIndex = 1;
            this.label5.Text = "Beschreibung";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(57, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "Artikelnr.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(4, 1);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 15);
            this.label10.TabIndex = 6;
            this.label10.Text = "Anz.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(548, 1);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 15);
            this.label7.TabIndex = 4;
            this.label7.Text = "Preis";
            // 
            // labelOrder_Date
            // 
            this.labelOrder_Date.AutoSize = true;
            this.labelOrder_Date.Location = new System.Drawing.Point(3, 77);
            this.labelOrder_Date.Name = "labelOrder_Date";
            this.labelOrder_Date.Size = new System.Drawing.Size(0, 15);
            this.labelOrder_Date.TabIndex = 22;
            // 
            // labelOrder_Nr
            // 
            this.labelOrder_Nr.AutoSize = true;
            this.labelOrder_Nr.Location = new System.Drawing.Point(3, 24);
            this.labelOrder_Nr.Name = "labelOrder_Nr";
            this.labelOrder_Nr.Size = new System.Drawing.Size(0, 15);
            this.labelOrder_Nr.TabIndex = 21;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.labelIdentificationNr);
            this.panel3.Controls.Add(this.labelIdentificationNr_Text);
            this.panel3.Controls.Add(this.labelOrderNr);
            this.panel3.Controls.Add(this.labelOrderNr_Text);
            this.panel3.Controls.Add(this.labelOrderDate);
            this.panel3.Controls.Add(this.labelOrderDate_Text);
            this.panel3.Location = new System.Drawing.Point(6, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(217, 70);
            this.panel3.TabIndex = 25;
            // 
            // labelIdentificationNr
            // 
            this.labelIdentificationNr.AutoSize = true;
            this.labelIdentificationNr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIdentificationNr.ForeColor = System.Drawing.Color.Black;
            this.labelIdentificationNr.Location = new System.Drawing.Point(3, 6);
            this.labelIdentificationNr.Name = "labelIdentificationNr";
            this.labelIdentificationNr.Size = new System.Drawing.Size(77, 15);
            this.labelIdentificationNr.TabIndex = 3;
            this.labelIdentificationNr.Text = "Kundennr.:";
            // 
            // labelIdentificationNr_Text
            // 
            this.labelIdentificationNr_Text.AutoSize = true;
            this.labelIdentificationNr_Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIdentificationNr_Text.ForeColor = System.Drawing.Color.Black;
            this.labelIdentificationNr_Text.Location = new System.Drawing.Point(107, 7);
            this.labelIdentificationNr_Text.Name = "labelIdentificationNr_Text";
            this.labelIdentificationNr_Text.Size = new System.Drawing.Size(37, 13);
            this.labelIdentificationNr_Text.TabIndex = 10;
            this.labelIdentificationNr_Text.Text = "xxxxx";
            // 
            // labelOrderNr
            // 
            this.labelOrderNr.AutoSize = true;
            this.labelOrderNr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOrderNr.ForeColor = System.Drawing.Color.Black;
            this.labelOrderNr.Location = new System.Drawing.Point(3, 27);
            this.labelOrderNr.Name = "labelOrderNr";
            this.labelOrderNr.Size = new System.Drawing.Size(72, 15);
            this.labelOrderNr.TabIndex = 14;
            this.labelOrderNr.Text = "Bestellnr.:";
            // 
            // labelOrderNr_Text
            // 
            this.labelOrderNr_Text.AutoSize = true;
            this.labelOrderNr_Text.ForeColor = System.Drawing.Color.Black;
            this.labelOrderNr_Text.Location = new System.Drawing.Point(107, 27);
            this.labelOrderNr_Text.Name = "labelOrderNr_Text";
            this.labelOrderNr_Text.Size = new System.Drawing.Size(37, 15);
            this.labelOrderNr_Text.TabIndex = 23;
            this.labelOrderNr_Text.Text = "xxxxx";
            // 
            // labelOrderDate
            // 
            this.labelOrderDate.AutoSize = true;
            this.labelOrderDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOrderDate.ForeColor = System.Drawing.Color.Black;
            this.labelOrderDate.Location = new System.Drawing.Point(3, 46);
            this.labelOrderDate.Name = "labelOrderDate";
            this.labelOrderDate.Size = new System.Drawing.Size(82, 15);
            this.labelOrderDate.TabIndex = 17;
            this.labelOrderDate.Text = "Bestelldatum:";
            // 
            // labelOrderDate_Text
            // 
            this.labelOrderDate_Text.AutoSize = true;
            this.labelOrderDate_Text.ForeColor = System.Drawing.Color.Black;
            this.labelOrderDate_Text.Location = new System.Drawing.Point(106, 46);
            this.labelOrderDate_Text.Name = "labelOrderDate_Text";
            this.labelOrderDate_Text.Size = new System.Drawing.Size(43, 15);
            this.labelOrderDate_Text.TabIndex = 24;
            this.labelOrderDate_Text.Text = "xxxxxx";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.pictureBoxPrint);
            this.panel1.Controls.Add(this.buttonCreateInvoice);
            this.panel1.Controls.Add(this.labelStatus);
            this.panel1.Controls.Add(this.comboBoxStatus);
            this.panel1.Location = new System.Drawing.Point(9, 144);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(616, 36);
            this.panel1.TabIndex = 0;
            // 
            // pictureBoxPrint
            // 
            this.pictureBoxPrint.BackColor = System.Drawing.Color.White;
            this.pictureBoxPrint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxPrint.Image = global::LaFamilia.BS2014.Controls.Properties.Resources.Drucker;
            this.pictureBoxPrint.Location = new System.Drawing.Point(581, 5);
            this.pictureBoxPrint.Name = "pictureBoxPrint";
            this.pictureBoxPrint.Size = new System.Drawing.Size(25, 25);
            this.pictureBoxPrint.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxPrint.TabIndex = 37;
            this.pictureBoxPrint.TabStop = false;
            this.pictureBoxPrint.Click += new System.EventHandler(this.pictureBoxPrint_Click);
            // 
            // buttonCreateInvoice
            // 
            this.buttonCreateInvoice.BackColor = System.Drawing.Color.MediumVioletRed;
            this.buttonCreateInvoice.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCreateInvoice.ForeColor = System.Drawing.Color.White;
            this.buttonCreateInvoice.Location = new System.Drawing.Point(431, 5);
            this.buttonCreateInvoice.Name = "buttonCreateInvoice";
            this.buttonCreateInvoice.Size = new System.Drawing.Size(138, 24);
            this.buttonCreateInvoice.TabIndex = 1;
            this.buttonCreateInvoice.Text = "Rechnung erstellen";
            this.buttonCreateInvoice.UseVisualStyleBackColor = false;
            this.buttonCreateInvoice.Click += new System.EventHandler(this.buttonCreateInvoice_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(4, 11);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(83, 15);
            this.labelStatus.TabIndex = 35;
            this.labelStatus.Text = "Status ändern";
            // 
            // comboBoxStatus
            // 
            this.comboBoxStatus.FormattingEnabled = true;
            this.comboBoxStatus.Location = new System.Drawing.Point(93, 7);
            this.comboBoxStatus.Name = "comboBoxStatus";
            this.comboBoxStatus.Size = new System.Drawing.Size(121, 23);
            this.comboBoxStatus.TabIndex = 0;
            this.comboBoxStatus.Text = "Status";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(105, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Beschreibung";
            // 
            // panelFooter
            // 
            this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFooter.Location = new System.Drawing.Point(0, 721);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(660, 25);
            this.panelFooter.TabIndex = 28;
            // 
            // panelHeader
            // 
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(660, 91);
            this.panelHeader.TabIndex = 31;
            // 
            // buttonOK
            // 
            this.buttonOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonOK.Location = new System.Drawing.Point(492, 692);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCancel.Location = new System.Drawing.Point(573, 692);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Abbrechen";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // printDocumentOrder
            // 
            this.printDocumentOrder.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocumentOrder_PrintPage);
            // 
            // FormOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(660, 746);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.panelFooter);
            this.Controls.Add(this.panelOrderDetails);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormOrder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LaFamilia | BS2014 | Bestellung Nr.";
            this.panelOrderDetails.ResumeLayout(false);
            this.panelOrderDetails.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPrint)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelOrderDetails;
        private System.Windows.Forms.Label labelOrderDate;
        private System.Windows.Forms.Label labelIdentificationNr;
        private System.Windows.Forms.Label labelOrderNr;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelOrder_Date;
        private System.Windows.Forms.Label labelOrder_Nr;
        private System.Windows.Forms.Label labelOrderDate_Text;
        private System.Windows.Forms.Label labelOrderNr_Text;
        private System.Windows.Forms.Label labelIdentificationNr_Text;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panelFooter;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.TextBox textBoxCountry;
        private System.Windows.Forms.TextBox textBoxLocation;
        private System.Windows.Forms.TextBox textBoxPostalcode;
        private System.Windows.Forms.TextBox textBoxStreet;
        private System.Windows.Forms.TextBox textBoxFirstName;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelArticles;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.ComboBox comboBoxStatus;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxLastName;
        private System.Windows.Forms.Button buttonCreateInvoice;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelOrder;
        private System.Windows.Forms.PictureBox pictureBoxPrint;
        private System.Drawing.Printing.PrintDocument printDocumentOrder;
    }
}