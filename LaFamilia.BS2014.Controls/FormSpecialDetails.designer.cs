﻿namespace LaFamilia.BS2014.Administration
{
    partial class FormSpecialDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelHeader = new System.Windows.Forms.Panel();
            this.panelFooter = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.richTextBoxDescription = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTimePickerDateFrom = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerDateTo = new System.Windows.Forms.DateTimePicker();
            this.groupBoxAddArticle = new System.Windows.Forms.GroupBox();
            this.comboBoxAllBrands = new System.Windows.Forms.ComboBox();
            this.listViewArticles = new System.Windows.Forms.ListView();
            this.columnHeaderArticlenr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderTitle = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderBrand = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBoxDetails = new System.Windows.Forms.GroupBox();
            this.pictureBoxSpecialImg = new System.Windows.Forms.PictureBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.groupBoxAddArticle.SuspendLayout();
            this.groupBoxDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpecialImg)).BeginInit();
            this.SuspendLayout();
            // 
            // panelHeader
            // 
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(661, 82);
            this.panelHeader.TabIndex = 32;
            // 
            // panelFooter
            // 
            this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFooter.Location = new System.Drawing.Point(0, 611);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(661, 29);
            this.panelFooter.TabIndex = 29;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(245, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 15);
            this.label1.TabIndex = 33;
            this.label1.Text = "Aktionsname";
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Location = new System.Drawing.Point(248, 47);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.Size = new System.Drawing.Size(248, 21);
            this.textBoxTitle.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(245, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 15);
            this.label2.TabIndex = 35;
            this.label2.Text = "Beschreibung";
            // 
            // richTextBoxDescription
            // 
            this.richTextBoxDescription.Location = new System.Drawing.Point(248, 139);
            this.richTextBoxDescription.Name = "richTextBoxDescription";
            this.richTextBoxDescription.Size = new System.Drawing.Size(375, 26);
            this.richTextBoxDescription.TabIndex = 3;
            this.richTextBoxDescription.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(245, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 15);
            this.label3.TabIndex = 37;
            this.label3.Text = "Datum von";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(380, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 15);
            this.label4.TabIndex = 38;
            this.label4.Text = "Datum bis";
            // 
            // dateTimePickerDateFrom
            // 
            this.dateTimePickerDateFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerDateFrom.Location = new System.Drawing.Point(248, 97);
            this.dateTimePickerDateFrom.Name = "dateTimePickerDateFrom";
            this.dateTimePickerDateFrom.Size = new System.Drawing.Size(110, 21);
            this.dateTimePickerDateFrom.TabIndex = 1;
            // 
            // dateTimePickerDateTo
            // 
            this.dateTimePickerDateTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerDateTo.Location = new System.Drawing.Point(383, 97);
            this.dateTimePickerDateTo.Name = "dateTimePickerDateTo";
            this.dateTimePickerDateTo.Size = new System.Drawing.Size(110, 21);
            this.dateTimePickerDateTo.TabIndex = 2;
            // 
            // groupBoxAddArticle
            // 
            this.groupBoxAddArticle.Controls.Add(this.comboBoxAllBrands);
            this.groupBoxAddArticle.Controls.Add(this.listViewArticles);
            this.groupBoxAddArticle.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBoxAddArticle.Location = new System.Drawing.Point(12, 270);
            this.groupBoxAddArticle.Name = "groupBoxAddArticle";
            this.groupBoxAddArticle.Size = new System.Drawing.Size(636, 303);
            this.groupBoxAddArticle.TabIndex = 48;
            this.groupBoxAddArticle.TabStop = false;
            this.groupBoxAddArticle.Text = "Artikel hinzufügen";
            // 
            // comboBoxAllBrands
            // 
            this.comboBoxAllBrands.FormattingEnabled = true;
            this.comboBoxAllBrands.Location = new System.Drawing.Point(451, 27);
            this.comboBoxAllBrands.Name = "comboBoxAllBrands";
            this.comboBoxAllBrands.Size = new System.Drawing.Size(172, 23);
            this.comboBoxAllBrands.TabIndex = 50;
            this.comboBoxAllBrands.Text = "Alle Marken";
            this.comboBoxAllBrands.SelectedIndexChanged += new System.EventHandler(this.comboBoxAllBrands_SelectedIndexChanged);
            // 
            // listViewArticles
            // 
            this.listViewArticles.CheckBoxes = true;
            this.listViewArticles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderArticlenr,
            this.columnHeaderTitle,
            this.columnHeaderBrand});
            this.listViewArticles.FullRowSelect = true;
            this.listViewArticles.Location = new System.Drawing.Point(13, 56);
            this.listViewArticles.Name = "listViewArticles";
            this.listViewArticles.Size = new System.Drawing.Size(610, 218);
            this.listViewArticles.TabIndex = 49;
            this.listViewArticles.UseCompatibleStateImageBehavior = false;
            this.listViewArticles.View = System.Windows.Forms.View.Details;
            this.listViewArticles.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listViewArticles_ItemChecked);
            // 
            // columnHeaderArticlenr
            // 
            this.columnHeaderArticlenr.Text = "Artikel-Nr.";
            this.columnHeaderArticlenr.Width = 98;
            // 
            // columnHeaderTitle
            // 
            this.columnHeaderTitle.Text = "Artikelname";
            this.columnHeaderTitle.Width = 144;
            // 
            // columnHeaderBrand
            // 
            this.columnHeaderBrand.Text = "Marke";
            this.columnHeaderBrand.Width = 132;
            // 
            // groupBoxDetails
            // 
            this.groupBoxDetails.Controls.Add(this.pictureBoxSpecialImg);
            this.groupBoxDetails.Controls.Add(this.richTextBoxDescription);
            this.groupBoxDetails.Controls.Add(this.label1);
            this.groupBoxDetails.Controls.Add(this.dateTimePickerDateTo);
            this.groupBoxDetails.Controls.Add(this.textBoxTitle);
            this.groupBoxDetails.Controls.Add(this.dateTimePickerDateFrom);
            this.groupBoxDetails.Controls.Add(this.label2);
            this.groupBoxDetails.Controls.Add(this.label4);
            this.groupBoxDetails.Controls.Add(this.label3);
            this.groupBoxDetails.Location = new System.Drawing.Point(12, 88);
            this.groupBoxDetails.Name = "groupBoxDetails";
            this.groupBoxDetails.Size = new System.Drawing.Size(636, 176);
            this.groupBoxDetails.TabIndex = 0;
            this.groupBoxDetails.TabStop = false;
            this.groupBoxDetails.Text = "Details";
            // 
            // pictureBoxSpecialImg
            // 
            this.pictureBoxSpecialImg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxSpecialImg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxSpecialImg.Location = new System.Drawing.Point(13, 30);
            this.pictureBoxSpecialImg.Name = "pictureBoxSpecialImg";
            this.pictureBoxSpecialImg.Size = new System.Drawing.Size(219, 135);
            this.pictureBoxSpecialImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxSpecialImg.TabIndex = 41;
            this.pictureBoxSpecialImg.TabStop = false;
            this.pictureBoxSpecialImg.Click += new System.EventHandler(this.pictureBoxSpecialImg_Click_1);
            // 
            // buttonSave
            // 
            this.buttonSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSave.Location = new System.Drawing.Point(491, 579);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 1;
            this.buttonSave.Text = "Speichern";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCancel.Location = new System.Drawing.Point(573, 579);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Abbrechen";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // FormSpecialDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(661, 640);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.groupBoxDetails);
            this.Controls.Add(this.groupBoxAddArticle);
            this.Controls.Add(this.panelFooter);
            this.Controls.Add(this.panelHeader);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormSpecialDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "La Familia | BS2014 | Neue Aktion";
            this.groupBoxAddArticle.ResumeLayout(false);
            this.groupBoxDetails.ResumeLayout(false);
            this.groupBoxDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpecialImg)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Panel panelFooter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox richTextBoxDescription;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dateTimePickerDateFrom;
				private System.Windows.Forms.DateTimePicker dateTimePickerDateTo;
        private System.Windows.Forms.GroupBox groupBoxAddArticle;
        private System.Windows.Forms.GroupBox groupBoxDetails;
        private System.Windows.Forms.Button buttonSave;
				private System.Windows.Forms.Button buttonCancel;
                private System.Windows.Forms.PictureBox pictureBoxSpecialImg;
				private System.Windows.Forms.ListView listViewArticles;
				private System.Windows.Forms.ComboBox comboBoxAllBrands;
				private System.Windows.Forms.ColumnHeader columnHeaderArticlenr;
                private System.Windows.Forms.ColumnHeader columnHeaderTitle;
                private System.Windows.Forms.ColumnHeader columnHeaderBrand;
    }
}