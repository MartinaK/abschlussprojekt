﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Controls
{
    public partial class UserControlArticleThumb : UserControl
    {
        Image imgThumb = null;
        private bool admin;
        Person person;

        public event EventHandler ShowArticleDetails;

        //*************************************************************************************
        #region constructors 

        public UserControlArticleThumb(Article article, bool admin, Person person)
        {
            InitializeComponent();

            this.article = article;
            this.admin = admin;
            this.person = person;

            if (this.article.ArticleImg != null)
                {
                    imgThumb = this.article.ArticleImg.GetThumbnailImage(100, 100, null, new IntPtr());
                    this.Refresh();
                }

            this.pictureBoxImage.Image = imgThumb;
            this.labelName.Text = this.article.Title;
            this.labelBrand.Text = this.article.Brand;
            this.labelPreis.Text = "Preis: " + this.article.Price.ToString() + " € ";
            this.labelUVP.Text = "UVP: " + this.article.Uvp.ToString() + " € ";
        }

        #endregion

        private Article article;

        public Article Article
        {
            get { return article; }
            set { article = value; }
        }
        
        private void pictureBoxImage_Click(object sender, EventArgs e)
        {
            if (admin)
            {
                FormArticleDetails articleDetailsForm = new FormArticleDetails(this.article);

                if (articleDetailsForm.ShowDialog() == DialogResult.OK)
                { }
                return;
            }

            if (ShowArticleDetails != null)
                ShowArticleDetails(this, EventArgs.Empty);
        }


    }
}
