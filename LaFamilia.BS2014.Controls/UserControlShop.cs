﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Controls
{
	public partial class UserControlShop : UserControl
	{
		private List<Article> showableArticles;
		private UserControlArticleThumb ucArticleThumb;
		private bool admin;
		private Person person;
		private Special special;

		//************************************************************************************
		#region constructors

		//Outlet
		public UserControlShop(Person person, bool admin)
		{
			InitializeComponent();

			this.admin = admin;
			this.person = person;

			foreach (Article article in Article.GetList())
			{
				if (!this.comboBoxBrand.Items.Contains(article.Brand))
					this.comboBoxBrand.Items.Add(article.Brand);

			}

			List<Category> catTree = Category.GetList(true);

			BuildCategoryTree(this.treeViewOutlet.Nodes, catTree);

		}

		public UserControlShop(Special special, Person person, bool admin)
		{
			InitializeComponent();

			this.admin = admin;
			this.person = person;
			this.special = special;

			this.pictureBoxImage.Image = this.special.PictureImg;

			foreach (Article article in Article.GetList().Where(a => a.SpecialId == this.special.SpecialId))
			{
				if (!this.comboBoxBrand.Items.Contains(article.Brand))
					this.comboBoxBrand.Items.Add(article.Brand);
			}

			List<Category> catTree = Category.GetList(true);

			BuildSpecialCategoryTree(this.treeViewOutlet.Nodes, catTree);

			if (this.special != null)
			{
				for (int idx =  this.treeViewOutlet.Nodes.Count - 1; idx >= 0; idx--)
				{
					if (!HasArticles(this.treeViewOutlet.Nodes[idx]))
						this.treeViewOutlet.Nodes.RemoveAt(idx);
				}
			}
		}

		private bool HasArticles(TreeNode node)
		{
			bool ok = false;
			bool oneOk = false;
			Category cat = (Category)node.Tag;
			if (cat.Articles != null && cat.Articles.Length > 0)
				return cat.Articles.Where(a => a.SpecialId == this.special.SpecialId).ToList<Article>().Count > 0;
			else
			{
				for (int idx = node.Nodes.Count - 1; idx >= 0; idx--)
				{
					ok = HasArticles(node.Nodes[idx]);
					if (!ok)
						node.Nodes.RemoveAt(idx);

					if (ok)
						oneOk = true;
				}
				return oneOk;
			}
		}


		#endregion

		//************************************************************************************
		#region private methods

		private void BuildCategoryTree(TreeNodeCollection nodes, List<Category> categories)
		{
			TreeNode node = null;

			foreach (Category cat in categories)
			{
				node = new TreeNode(cat.Title);
				node.Tag = cat;
				nodes.Add(node);

				BuildCategoryTree(node.Nodes, cat.Categories);
			}
		}

		private void BuildSpecialCategoryTree(TreeNodeCollection nodes, List<Category> categories)
		{
			TreeNode node = null;

			foreach (Category cat in categories)
			{
				node = new TreeNode(cat.Title);
				node.Tag = cat;
				nodes.Add(node);

				BuildSpecialCategoryTree(node.Nodes, cat.Categories);
			}
		}
		#endregion

		private void treeViewOutlet_AfterSelect(object sender, TreeViewEventArgs e)
		{
			if (e.Node.Tag is Category)
			{
				Category cat = (Category)e.Node.Tag;

				if (this.special == null)
				{
					this.showableArticles = cat.Articles.Where(a => a.Type == "O").ToList<Article>();
					LoadData(this.showableArticles);
					return;
				}
				this.showableArticles = cat.Articles.Where(a => a.SpecialId == this.special.SpecialId).ToList<Article>();
				LoadData(this.showableArticles);
			}
		}

		private void LoadData(List<Article> showableArticles)
		{
			this.flowLayoutPanelArticles.Controls.Clear();

			if (showableArticles != null)
			{
				foreach (Article article in showableArticles)
				{
					ucArticleThumb = new UserControlArticleThumb(article, admin, this.person);
                    ucArticleThumb.ShowArticleDetails += new EventHandler(ucArticleThumb_ShowArticleDetails);
					this.flowLayoutPanelArticles.Controls.Add(ucArticleThumb);
				}
			}
		}

        void ucArticleThumb_ShowArticleDetails(object sender, EventArgs e)
        {
            UserControlArticleThumb ucThumb = sender as UserControlArticleThumb;

            this.flowLayoutPanelArticles.Controls.Clear();
            UserControlShopArticle ucShopArticle = new UserControlShopArticle(ucThumb.Article, this.person);
            this.flowLayoutPanelArticles.Controls.Add(ucShopArticle);
        }

		private void comboBoxSize_SelectedIndexChanged(object sender, EventArgs e)
		{
			//List<Article> filterArticles = new List<Article>();
			//string filterName = this.comboBoxSize.SelectedItem.ToString();
			//Category filterCategory = (Category)this.treeViewOutlet.SelectedNode.Tag;

			//foreach (Article article in filterCategory.Articles)
			//{
			//    foreach (Variant v in article.Variants.Where(v => v.Size.Equals(filterName)))
			//    {
			//        filterArticles.Add(article);
			//    }
			//}
			//LoadData(filterArticles);

		}

		private void comboBoxBrand_SelectedIndexChanged(object sender, EventArgs e)
		{
			string filterName = this.comboBoxBrand.SelectedItem.ToString();
			this.showableArticles = this.showableArticles.Where(a => a.Brand.Equals(filterName)).ToList<Article>();
			LoadData(this.showableArticles);
		}


	}
}
