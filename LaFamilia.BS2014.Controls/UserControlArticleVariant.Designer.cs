﻿namespace LaFamilia.BS2014.Controls
{
    partial class UserControlArticleVariant
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxStack = new System.Windows.Forms.TextBox();
            this.textBoxAttrival = new System.Windows.Forms.TextBox();
            this.pictureBoxDelete = new System.Windows.Forms.PictureBox();
            this.comboBoxAttribute = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDelete)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxStack
            // 
            this.textBoxStack.Location = new System.Drawing.Point(223, 6);
            this.textBoxStack.Name = "textBoxStack";
            this.textBoxStack.Size = new System.Drawing.Size(83, 21);
            this.textBoxStack.TabIndex = 1;
            this.textBoxStack.Leave += new System.EventHandler(this.textBoxStack_Leave);
            // 
            // textBoxAttrival
            // 
            this.textBoxAttrival.Location = new System.Drawing.Point(74, 6);
            this.textBoxAttrival.Name = "textBoxAttrival";
            this.textBoxAttrival.Size = new System.Drawing.Size(143, 21);
            this.textBoxAttrival.TabIndex = 0;
            // 
            // pictureBoxDelete
            // 
            this.pictureBoxDelete.Image = global::LaFamilia.BS2014.Controls.Properties.Resources.delete;
            this.pictureBoxDelete.Location = new System.Drawing.Point(350, 0);
            this.pictureBoxDelete.Name = "pictureBoxDelete";
            this.pictureBoxDelete.Size = new System.Drawing.Size(17, 27);
            this.pictureBoxDelete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxDelete.TabIndex = 69;
            this.pictureBoxDelete.TabStop = false;
            this.pictureBoxDelete.Click += new System.EventHandler(this.pictureBoxDelete_Click);
            // 
            // comboBoxAttribute
            // 
            this.comboBoxAttribute.FormattingEnabled = true;
            this.comboBoxAttribute.Items.AddRange(new object[] {
            "Größe"});
            this.comboBoxAttribute.Location = new System.Drawing.Point(3, 4);
            this.comboBoxAttribute.Name = "comboBoxAttribute";
            this.comboBoxAttribute.Size = new System.Drawing.Size(65, 23);
            this.comboBoxAttribute.TabIndex = 70;
            // 
            // UserControlArticleVariant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.comboBoxAttribute);
            this.Controls.Add(this.pictureBoxDelete);
            this.Controls.Add(this.textBoxStack);
            this.Controls.Add(this.textBoxAttrival);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "UserControlArticleVariant";
            this.Size = new System.Drawing.Size(386, 32);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDelete)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxDelete;
        private System.Windows.Forms.TextBox textBoxStack;
				private System.Windows.Forms.TextBox textBoxAttrival;
				private System.Windows.Forms.ComboBox comboBoxAttribute;
    }
}
