﻿namespace LaFamilia.BS2014.Controls
{
    partial class UserControlShoppingCartItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxArticleImg = new System.Windows.Forms.PictureBox();
            this.labelArticleTitle = new System.Windows.Forms.Label();
            this.labelPrice = new System.Windows.Forms.Label();
            this.numericUpDownQuantity = new System.Windows.Forms.NumericUpDown();
            this.comboBoxSize = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxArticleImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQuantity)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxArticleImg
            // 
            this.pictureBoxArticleImg.Location = new System.Drawing.Point(0, 1);
            this.pictureBoxArticleImg.Name = "pictureBoxArticleImg";
            this.pictureBoxArticleImg.Size = new System.Drawing.Size(100, 58);
            this.pictureBoxArticleImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxArticleImg.TabIndex = 0;
            this.pictureBoxArticleImg.TabStop = false;
            // 
            // labelArticleTitle
            // 
            this.labelArticleTitle.Location = new System.Drawing.Point(106, 0);
            this.labelArticleTitle.Name = "labelArticleTitle";
            this.labelArticleTitle.Size = new System.Drawing.Size(187, 58);
            this.labelArticleTitle.TabIndex = 1;
            this.labelArticleTitle.Text = "Artikelname";
            this.labelArticleTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPrice
            // 
            this.labelPrice.AutoSize = true;
            this.labelPrice.Location = new System.Drawing.Point(557, 25);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(35, 15);
            this.labelPrice.TabIndex = 2;
            this.labelPrice.Text = "Preis";
            // 
            // numericUpDownQuantity
            // 
            this.numericUpDownQuantity.Location = new System.Drawing.Point(487, 23);
            this.numericUpDownQuantity.Name = "numericUpDownQuantity";
            this.numericUpDownQuantity.Size = new System.Drawing.Size(49, 21);
            this.numericUpDownQuantity.TabIndex = 3;
            this.numericUpDownQuantity.ValueChanged += new System.EventHandler(this.numericUpDownQuantity_ValueChanged);
            // 
            // comboBoxSize
            // 
            this.comboBoxSize.FormattingEnabled = true;
            this.comboBoxSize.Location = new System.Drawing.Point(393, 22);
            this.comboBoxSize.Name = "comboBoxSize";
            this.comboBoxSize.Size = new System.Drawing.Size(75, 23);
            this.comboBoxSize.TabIndex = 5;
            this.comboBoxSize.SelectedIndexChanged += new System.EventHandler(this.comboBoxSize_SelectedIndexChanged);
            // 
            // UserControlShoppingCartItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.comboBoxSize);
            this.Controls.Add(this.numericUpDownQuantity);
            this.Controls.Add(this.labelPrice);
            this.Controls.Add(this.labelArticleTitle);
            this.Controls.Add(this.pictureBoxArticleImg);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "UserControlShoppingCartItem";
            this.Size = new System.Drawing.Size(613, 59);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxArticleImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQuantity)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxArticleImg;
        private System.Windows.Forms.Label labelArticleTitle;
        private System.Windows.Forms.Label labelPrice;
				private System.Windows.Forms.NumericUpDown numericUpDownQuantity;
                private System.Windows.Forms.ComboBox comboBoxSize;
    }
}
