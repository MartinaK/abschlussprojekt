﻿namespace LaFamilia.BS2014.Controls
{
    partial class UserControlShopArticle
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelUvp = new System.Windows.Forms.Label();
            this.labelPrice = new System.Windows.Forms.Label();
            this.labelArtikelnr = new System.Windows.Forms.Label();
            this.labelSupplyDate = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.numericUpDownQuantity = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxSize = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelBrand = new System.Windows.Forms.Label();
            this.pictureBoxAddToCart = new System.Windows.Forms.PictureBox();
            this.pictureBoxStatus = new System.Windows.Forms.PictureBox();
            this.pictureBoxArticleImg = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAddToCart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxArticleImg)).BeginInit();
            this.SuspendLayout();
            // 
            // labelUvp
            // 
            this.labelUvp.AutoSize = true;
            this.labelUvp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Strikeout, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUvp.Location = new System.Drawing.Point(264, 148);
            this.labelUvp.Name = "labelUvp";
            this.labelUvp.Size = new System.Drawing.Size(34, 15);
            this.labelUvp.TabIndex = 62;
            this.labelUvp.Text = "UVP:";
            // 
            // labelPrice
            // 
            this.labelPrice.AutoSize = true;
            this.labelPrice.Location = new System.Drawing.Point(369, 148);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(38, 15);
            this.labelPrice.TabIndex = 61;
            this.labelPrice.Text = "Preis:";
            // 
            // labelArtikelnr
            // 
            this.labelArtikelnr.AutoSize = true;
            this.labelArtikelnr.Location = new System.Drawing.Point(267, 14);
            this.labelArtikelnr.Name = "labelArtikelnr";
            this.labelArtikelnr.Size = new System.Drawing.Size(51, 15);
            this.labelArtikelnr.TabIndex = 60;
            this.labelArtikelnr.Text = "Artikelnr";
            // 
            // labelSupplyDate
            // 
            this.labelSupplyDate.AutoSize = true;
            this.labelSupplyDate.Location = new System.Drawing.Point(264, 303);
            this.labelSupplyDate.Name = "labelSupplyDate";
            this.labelSupplyDate.Size = new System.Drawing.Size(76, 15);
            this.labelSupplyDate.TabIndex = 58;
            this.labelSupplyDate.Text = "Lieferdatum:";
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(289, 271);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(60, 15);
            this.labelStatus.TabIndex = 57;
            this.labelStatus.Text = "Verfügbar";
            // 
            // numericUpDownQuantity
            // 
            this.numericUpDownQuantity.Location = new System.Drawing.Point(329, 227);
            this.numericUpDownQuantity.Name = "numericUpDownQuantity";
            this.numericUpDownQuantity.Size = new System.Drawing.Size(68, 21);
            this.numericUpDownQuantity.TabIndex = 56;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(264, 227);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 15);
            this.label2.TabIndex = 55;
            this.label2.Text = "Anzahl";
            // 
            // comboBoxSize
            // 
            this.comboBoxSize.FormattingEnabled = true;
            this.comboBoxSize.Location = new System.Drawing.Point(329, 188);
            this.comboBoxSize.Name = "comboBoxSize";
            this.comboBoxSize.Size = new System.Drawing.Size(67, 23);
            this.comboBoxSize.TabIndex = 54;
            this.comboBoxSize.SelectedIndexChanged += new System.EventHandler(this.comboBoxSize_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(264, 192);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 15);
            this.label1.TabIndex = 53;
            this.label1.Text = "Größe";
            // 
            // labelTitle
            // 
            this.labelTitle.Location = new System.Drawing.Point(267, 74);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(181, 63);
            this.labelTitle.TabIndex = 52;
            this.labelTitle.Text = "Artikelname";
            // 
            // labelBrand
            // 
            this.labelBrand.AutoSize = true;
            this.labelBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBrand.Location = new System.Drawing.Point(267, 40);
            this.labelBrand.Name = "labelBrand";
            this.labelBrand.Size = new System.Drawing.Size(45, 15);
            this.labelBrand.TabIndex = 51;
            this.labelBrand.Text = "Brand";
            // 
            // pictureBoxAddToCart
            // 
            this.pictureBoxAddToCart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxAddToCart.Image = global::LaFamilia.BS2014.Controls.Properties.Resources.warenkorb_button;
            this.pictureBoxAddToCart.Location = new System.Drawing.Point(440, 213);
            this.pictureBoxAddToCart.Name = "pictureBoxAddToCart";
            this.pictureBoxAddToCart.Size = new System.Drawing.Size(148, 35);
            this.pictureBoxAddToCart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxAddToCart.TabIndex = 63;
            this.pictureBoxAddToCart.TabStop = false;
            this.pictureBoxAddToCart.Click += new System.EventHandler(this.pictureBoxAddToCart_Click);
            // 
            // pictureBoxStatus
            // 
            this.pictureBoxStatus.Image = global::LaFamilia.BS2014.Controls.Properties.Resources.available;
            this.pictureBoxStatus.Location = new System.Drawing.Point(267, 271);
            this.pictureBoxStatus.Name = "pictureBoxStatus";
            this.pictureBoxStatus.Size = new System.Drawing.Size(17, 17);
            this.pictureBoxStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxStatus.TabIndex = 59;
            this.pictureBoxStatus.TabStop = false;
            // 
            // pictureBoxArticleImg
            // 
            this.pictureBoxArticleImg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxArticleImg.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureBoxArticleImg.Location = new System.Drawing.Point(13, 14);
            this.pictureBoxArticleImg.Name = "pictureBoxArticleImg";
            this.pictureBoxArticleImg.Size = new System.Drawing.Size(227, 301);
            this.pictureBoxArticleImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxArticleImg.TabIndex = 50;
            this.pictureBoxArticleImg.TabStop = false;
            // 
            // UserControlShopArticle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pictureBoxAddToCart);
            this.Controls.Add(this.labelUvp);
            this.Controls.Add(this.labelPrice);
            this.Controls.Add(this.labelArtikelnr);
            this.Controls.Add(this.pictureBoxStatus);
            this.Controls.Add(this.labelSupplyDate);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.numericUpDownQuantity);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxSize);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.labelBrand);
            this.Controls.Add(this.pictureBoxArticleImg);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "UserControlShopArticle";
            this.Size = new System.Drawing.Size(604, 330);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAddToCart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxArticleImg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxAddToCart;
        private System.Windows.Forms.Label labelUvp;
        private System.Windows.Forms.Label labelPrice;
        private System.Windows.Forms.Label labelArtikelnr;
        private System.Windows.Forms.PictureBox pictureBoxStatus;
        private System.Windows.Forms.Label labelSupplyDate;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.NumericUpDown numericUpDownQuantity;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxSize;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelBrand;
        private System.Windows.Forms.PictureBox pictureBoxArticleImg;
    }
}
