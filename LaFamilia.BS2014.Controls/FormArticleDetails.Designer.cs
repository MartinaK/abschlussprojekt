﻿namespace LaFamilia.BS2014.Controls
{
    partial class FormArticleDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSave = new System.Windows.Forms.Button();
            this.checkBoxAktion = new System.Windows.Forms.CheckBox();
            this.checkBoxOutlet = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxBrand = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxArtNr = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.textBoxPrice = new System.Windows.Forms.TextBox();
            this.labelPrice = new System.Windows.Forms.Label();
            this.labelUVP = new System.Windows.Forms.Label();
            this.comboBoxSupplier = new System.Windows.Forms.ComboBox();
            this.textBoxUVP = new System.Windows.Forms.TextBox();
            this.panelHead = new System.Windows.Forms.Panel();
            this.panelFooter = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBoxVariants = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.flowLayoutPanelVariants = new System.Windows.Forms.FlowLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.groupBoxVariants.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonSave
            // 
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSave.Location = new System.Drawing.Point(493, 464);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(87, 27);
            this.buttonSave.TabIndex = 7;
            this.buttonSave.Text = "Speichern";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // checkBoxAktion
            // 
            this.checkBoxAktion.AutoSize = true;
            this.checkBoxAktion.Location = new System.Drawing.Point(17, 8);
            this.checkBoxAktion.Name = "checkBoxAktion";
            this.checkBoxAktion.Size = new System.Drawing.Size(59, 19);
            this.checkBoxAktion.TabIndex = 30;
            this.checkBoxAktion.Text = "Aktion";
            this.checkBoxAktion.UseVisualStyleBackColor = true;
            // 
            // checkBoxOutlet
            // 
            this.checkBoxOutlet.AutoSize = true;
            this.checkBoxOutlet.Location = new System.Drawing.Point(102, 8);
            this.checkBoxOutlet.Name = "checkBoxOutlet";
            this.checkBoxOutlet.Size = new System.Drawing.Size(58, 19);
            this.checkBoxOutlet.TabIndex = 29;
            this.checkBoxOutlet.Text = "Outlet";
            this.checkBoxOutlet.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(188, 198);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 15);
            this.label7.TabIndex = 28;
            this.label7.Text = "Marke";
            // 
            // textBoxBrand
            // 
            this.textBoxBrand.Location = new System.Drawing.Point(191, 217);
            this.textBoxBrand.Name = "textBoxBrand";
            this.textBoxBrand.Size = new System.Drawing.Size(118, 21);
            this.textBoxBrand.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(190, 146);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 15);
            this.label6.TabIndex = 26;
            this.label6.Text = "Artikelnr";
            // 
            // textBoxArtNr
            // 
            this.textBoxArtNr.Location = new System.Drawing.Point(193, 164);
            this.textBoxArtNr.Name = "textBoxArtNr";
            this.textBoxArtNr.Size = new System.Drawing.Size(116, 21);
            this.textBoxArtNr.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(323, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 15);
            this.label5.TabIndex = 24;
            this.label5.Text = "Artikelname";
            // 
            // buttonCancel
            // 
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCancel.Location = new System.Drawing.Point(586, 464);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(87, 27);
            this.buttonCancel.TabIndex = 8;
            this.buttonCancel.Text = "Abbrechen";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Location = new System.Drawing.Point(326, 164);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.Size = new System.Drawing.Size(234, 21);
            this.textBoxTitle.TabIndex = 2;
            // 
            // textBoxPrice
            // 
            this.textBoxPrice.Location = new System.Drawing.Point(490, 265);
            this.textBoxPrice.Name = "textBoxPrice";
            this.textBoxPrice.Size = new System.Drawing.Size(70, 21);
            this.textBoxPrice.TabIndex = 6;
            // 
            // labelPrice
            // 
            this.labelPrice.AutoSize = true;
            this.labelPrice.Location = new System.Drawing.Point(487, 247);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(35, 15);
            this.labelPrice.TabIndex = 41;
            this.labelPrice.Text = "Preis";
            // 
            // labelUVP
            // 
            this.labelUVP.AutoSize = true;
            this.labelUVP.Location = new System.Drawing.Point(392, 247);
            this.labelUVP.Name = "labelUVP";
            this.labelUVP.Size = new System.Drawing.Size(31, 15);
            this.labelUVP.TabIndex = 43;
            this.labelUVP.Text = "UVP";
            // 
            // comboBoxSupplier
            // 
            this.comboBoxSupplier.FormattingEnabled = true;
            this.comboBoxSupplier.Location = new System.Drawing.Point(191, 260);
            this.comboBoxSupplier.Name = "comboBoxSupplier";
            this.comboBoxSupplier.Size = new System.Drawing.Size(178, 23);
            this.comboBoxSupplier.TabIndex = 4;
            this.comboBoxSupplier.Text = "Lieferant";
            // 
            // textBoxUVP
            // 
            this.textBoxUVP.Location = new System.Drawing.Point(395, 265);
            this.textBoxUVP.Name = "textBoxUVP";
            this.textBoxUVP.Size = new System.Drawing.Size(76, 21);
            this.textBoxUVP.TabIndex = 5;
            // 
            // panelHead
            // 
            this.panelHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHead.Location = new System.Drawing.Point(0, 0);
            this.panelHead.Name = "panelHead";
            this.panelHead.Size = new System.Drawing.Size(699, 95);
            this.panelHead.TabIndex = 53;
            // 
            // panelFooter
            // 
            this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFooter.Location = new System.Drawing.Point(0, 508);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(699, 30);
            this.panelFooter.TabIndex = 54;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkBoxAktion);
            this.panel1.Controls.Add(this.checkBoxOutlet);
            this.panel1.Location = new System.Drawing.Point(187, 111);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(192, 30);
            this.panel1.TabIndex = 0;
            // 
            // groupBoxVariants
            // 
            this.groupBoxVariants.Controls.Add(this.label1);
            this.groupBoxVariants.Controls.Add(this.label2);
            this.groupBoxVariants.Controls.Add(this.label4);
            this.groupBoxVariants.Controls.Add(this.flowLayoutPanelVariants);
            this.groupBoxVariants.Location = new System.Drawing.Point(185, 313);
            this.groupBoxVariants.Name = "groupBoxVariants";
            this.groupBoxVariants.Size = new System.Drawing.Size(488, 134);
            this.groupBoxVariants.TabIndex = 62;
            this.groupBoxVariants.TabStop = false;
            this.groupBoxVariants.Text = "Varianten";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(242, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 15);
            this.label1.TabIndex = 69;
            this.label1.Text = "Anzahl";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 15);
            this.label2.TabIndex = 71;
            this.label2.Text = "Attribut";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(92, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 15);
            this.label4.TabIndex = 70;
            this.label4.Text = "Wert";
            // 
            // flowLayoutPanelVariants
            // 
            this.flowLayoutPanelVariants.AutoScroll = true;
            this.flowLayoutPanelVariants.Location = new System.Drawing.Point(8, 40);
            this.flowLayoutPanelVariants.Name = "flowLayoutPanelVariants";
            this.flowLayoutPanelVariants.Size = new System.Drawing.Size(474, 82);
            this.flowLayoutPanelVariants.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(27, 111);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(141, 172);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 32;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // FormArticleDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 538);
            this.Controls.Add(this.groupBoxVariants);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelFooter);
            this.Controls.Add(this.panelHead);
            this.Controls.Add(this.textBoxUVP);
            this.Controls.Add(this.comboBoxSupplier);
            this.Controls.Add(this.labelUVP);
            this.Controls.Add(this.labelPrice);
            this.Controls.Add(this.textBoxPrice);
            this.Controls.Add(this.textBoxTitle);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxBrand);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxArtNr);
            this.Controls.Add(this.label5);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormArticleDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "La Familia | BS2014 | Artikel";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBoxVariants.ResumeLayout(false);
            this.groupBoxVariants.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.CheckBox checkBoxAktion;
        private System.Windows.Forms.CheckBox checkBoxOutlet;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxBrand;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxArtNr;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonCancel;
				private System.Windows.Forms.TextBox textBoxTitle;
        private System.Windows.Forms.TextBox textBoxPrice;
        private System.Windows.Forms.Label labelPrice;
        private System.Windows.Forms.Label labelUVP;
        private System.Windows.Forms.ComboBox comboBoxSupplier;
        private System.Windows.Forms.TextBox textBoxUVP;
        private System.Windows.Forms.Panel panelHead;
        private System.Windows.Forms.Panel panelFooter;
                private System.Windows.Forms.Panel panel1;
                private System.Windows.Forms.GroupBox groupBoxVariants;
                private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelVariants;
                private System.Windows.Forms.Label label1;
                private System.Windows.Forms.Label label2;
                private System.Windows.Forms.Label label4;
    }
}