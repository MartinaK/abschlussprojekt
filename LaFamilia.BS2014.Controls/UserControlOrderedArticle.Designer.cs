﻿namespace LaFamilia.BS2014.Controls
{
	partial class UserControlOrderedArticle
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.labelNr = new System.Windows.Forms.Label();
            this.labelDescription = new System.Windows.Forms.Label();
            this.labelPrice = new System.Windows.Forms.Label();
            this.tableLayoutPanelArticle = new System.Windows.Forms.TableLayoutPanel();
            this.labelSize = new System.Windows.Forms.Label();
            this.textBoxCount = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelArticle.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelNr
            // 
            this.labelNr.Location = new System.Drawing.Point(55, 1);
            this.labelNr.Name = "labelNr";
            this.labelNr.Size = new System.Drawing.Size(84, 30);
            this.labelNr.TabIndex = 1;
            this.labelNr.Text = "4654123";
            this.labelNr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDescription
            // 
            this.labelDescription.Location = new System.Drawing.Point(146, 1);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(270, 30);
            this.labelDescription.TabIndex = 2;
            this.labelDescription.Text = "Body Baby Blau";
            this.labelDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPrice
            // 
            this.labelPrice.Location = new System.Drawing.Point(546, 1);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(67, 30);
            this.labelPrice.TabIndex = 3;
            this.labelPrice.Text = "25 €";
            this.labelPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanelArticle
            // 
            this.tableLayoutPanelArticle.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanelArticle.ColumnCount = 5;
            this.tableLayoutPanelArticle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanelArticle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanelArticle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 336F));
            this.tableLayoutPanelArticle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelArticle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tableLayoutPanelArticle.Controls.Add(this.labelDescription, 2, 0);
            this.tableLayoutPanelArticle.Controls.Add(this.labelPrice, 4, 0);
            this.tableLayoutPanelArticle.Controls.Add(this.labelSize, 3, 0);
            this.tableLayoutPanelArticle.Controls.Add(this.textBoxCount, 0, 0);
            this.tableLayoutPanelArticle.Controls.Add(this.labelNr, 1, 0);
            this.tableLayoutPanelArticle.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelArticle.Name = "tableLayoutPanelArticle";
            this.tableLayoutPanelArticle.RowCount = 1;
            this.tableLayoutPanelArticle.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelArticle.Size = new System.Drawing.Size(614, 32);
            this.tableLayoutPanelArticle.TabIndex = 4;
            // 
            // labelSize
            // 
            this.labelSize.Location = new System.Drawing.Point(483, 1);
            this.labelSize.Name = "labelSize";
            this.labelSize.Size = new System.Drawing.Size(56, 30);
            this.labelSize.TabIndex = 4;
            this.labelSize.Text = "92";
            this.labelSize.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxCount
            // 
            this.textBoxCount.Location = new System.Drawing.Point(4, 4);
            this.textBoxCount.Name = "textBoxCount";
            this.textBoxCount.Size = new System.Drawing.Size(41, 21);
            this.textBoxCount.TabIndex = 0;
            // 
            // UserControlOrderedArticle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanelArticle);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "UserControlOrderedArticle";
            this.Size = new System.Drawing.Size(617, 34);
            this.tableLayoutPanelArticle.ResumeLayout(false);
            this.tableLayoutPanelArticle.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

        private System.Windows.Forms.Label labelNr;
		private System.Windows.Forms.Label labelDescription;
		private System.Windows.Forms.Label labelPrice;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanelArticle;
		private System.Windows.Forms.Label labelSize;
        private System.Windows.Forms.TextBox textBoxCount;
	}
}
