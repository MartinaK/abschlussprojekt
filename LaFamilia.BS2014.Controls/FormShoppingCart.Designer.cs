﻿namespace LaFamilia.BS2014.Controls
{
    partial class FormShoppingCart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.panelHeader = new System.Windows.Forms.Panel();
			this.panelFooter = new System.Windows.Forms.Panel();
			this.buttonOrder = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonDeleteCart = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.labelTotalCosts = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.labelShippingCosts = new System.Windows.Forms.Label();
			this.flowLayoutPanelArticles = new System.Windows.Forms.FlowLayoutPanel();
			this.label4 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.labelSum = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// panelHeader
			// 
			this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelHeader.Location = new System.Drawing.Point(0, 0);
			this.panelHeader.Name = "panelHeader";
			this.panelHeader.Size = new System.Drawing.Size(646, 122);
			this.panelHeader.TabIndex = 31;
			// 
			// panelFooter
			// 
			this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panelFooter.Location = new System.Drawing.Point(0, 646);
			this.panelFooter.Name = "panelFooter";
			this.panelFooter.Size = new System.Drawing.Size(646, 30);
			this.panelFooter.TabIndex = 35;
			// 
			// buttonOrder
			// 
			this.buttonOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonOrder.BackColor = System.Drawing.Color.LightSeaGreen;
			this.buttonOrder.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.buttonOrder.Location = new System.Drawing.Point(534, 606);
			this.buttonOrder.Name = "buttonOrder";
			this.buttonOrder.Size = new System.Drawing.Size(100, 29);
			this.buttonOrder.TabIndex = 36;
			this.buttonOrder.Text = "Bestellen";
			this.buttonOrder.UseVisualStyleBackColor = false;
			this.buttonOrder.Click += new System.EventHandler(this.buttonOrder_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.DimGray;
			this.label1.Location = new System.Drawing.Point(12, 134);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(96, 20);
			this.label1.TabIndex = 38;
			this.label1.Text = "Warenkorb";
			// 
			// buttonDeleteCart
			// 
			this.buttonDeleteCart.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonDeleteCart.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.buttonDeleteCart.Location = new System.Drawing.Point(488, 134);
			this.buttonDeleteCart.Name = "buttonDeleteCart";
			this.buttonDeleteCart.Size = new System.Drawing.Size(146, 27);
			this.buttonDeleteCart.TabIndex = 39;
			this.buttonDeleteCart.Text = "Warenkorb leeren";
			this.buttonDeleteCart.UseVisualStyleBackColor = false;
			this.buttonDeleteCart.Click += new System.EventHandler(this.buttonDeleteCart_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(557, 180);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(77, 15);
			this.label2.TabIndex = 41;
			this.label2.Text = "Gesamtpreis";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(502, 180);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(46, 15);
			this.label3.TabIndex = 42;
			this.label3.Text = "Menge";
			// 
			// labelTotalCosts
			// 
			this.labelTotalCosts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.labelTotalCosts.AutoSize = true;
			this.labelTotalCosts.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelTotalCosts.Location = new System.Drawing.Point(497, 556);
			this.labelTotalCosts.Name = "labelTotalCosts";
			this.labelTotalCosts.Size = new System.Drawing.Size(60, 15);
			this.labelTotalCosts.TabIndex = 43;
			this.labelTotalCosts.Text = "Summe:";
			// 
			// button2
			// 
			this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.button2.Location = new System.Drawing.Point(390, 606);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(138, 29);
			this.button2.TabIndex = 44;
			this.button2.Text = "Weiter Einkaufen";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.buttonBackToShop_Click);
			// 
			// labelShippingCosts
			// 
			this.labelShippingCosts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.labelShippingCosts.AutoSize = true;
			this.labelShippingCosts.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelShippingCosts.Location = new System.Drawing.Point(466, 532);
			this.labelShippingCosts.Name = "labelShippingCosts";
			this.labelShippingCosts.Size = new System.Drawing.Size(91, 15);
			this.labelShippingCosts.TabIndex = 45;
			this.labelShippingCosts.Text = "Versandkosten:";
			// 
			// flowLayoutPanelArticles
			// 
			this.flowLayoutPanelArticles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.flowLayoutPanelArticles.AutoScroll = true;
			this.flowLayoutPanelArticles.Location = new System.Drawing.Point(14, 198);
			this.flowLayoutPanelArticles.Name = "flowLayoutPanelArticles";
			this.flowLayoutPanelArticles.Size = new System.Drawing.Size(619, 331);
			this.flowLayoutPanelArticles.TabIndex = 46;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(411, 180);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(41, 15);
			this.label4.TabIndex = 47;
			this.label4.Text = "Größe";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(590, 532);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(41, 15);
			this.label6.TabIndex = 49;
			this.label6.Text = "4,95 €";
			// 
			// labelSum
			// 
			this.labelSum.Location = new System.Drawing.Point(563, 556);
			this.labelSum.Name = "labelSum";
			this.labelSum.Size = new System.Drawing.Size(68, 15);
			this.labelSum.TabIndex = 50;
			this.labelSum.Text = "0,00 €";
			this.labelSum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// FormShoppingCart
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.ClientSize = new System.Drawing.Size(646, 676);
			this.Controls.Add(this.labelSum);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.flowLayoutPanelArticles);
			this.Controls.Add(this.labelShippingCosts);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.labelTotalCosts);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.buttonDeleteCart);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.buttonOrder);
			this.Controls.Add(this.panelFooter);
			this.Controls.Add(this.panelHeader);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Name = "FormShoppingCart";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "La Familia | Warenkorb";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Panel panelFooter;
        private System.Windows.Forms.Button buttonOrder;
        private System.Windows.Forms.Label label1;
				private System.Windows.Forms.Button buttonDeleteCart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelTotalCosts;
        private System.Windows.Forms.Button button2;
				private System.Windows.Forms.Label labelShippingCosts;
                private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelArticles;
                private System.Windows.Forms.Label label4;
                private System.Windows.Forms.Label label6;
                private System.Windows.Forms.Label labelSum;
    }
}