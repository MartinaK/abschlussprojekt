﻿namespace LaFamilia.BS2014.Controls
{
    partial class UserControlOrderList
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewOrder = new System.Windows.Forms.ListView();
            this.columnHeaderOrderNr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPersonID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderOrderDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderLastUpdate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderEmployee = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.comboBoxStatus = new System.Windows.Forms.ComboBox();
            this.panelFilter = new System.Windows.Forms.Panel();
            this.linkLabelResetFilter = new System.Windows.Forms.LinkLabel();
            this.comboBoxName = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.printDocumentOrder = new System.Drawing.Printing.PrintDocument();
            this.panelFilter.SuspendLayout();
            this.SuspendLayout();
            // 
            // listViewOrder
            // 
            this.listViewOrder.AutoArrange = false;
            this.listViewOrder.BackColor = System.Drawing.Color.WhiteSmoke;
            this.listViewOrder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listViewOrder.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderOrderNr,
            this.columnHeaderPersonID,
            this.columnHeaderName,
            this.columnHeaderOrderDate,
            this.columnHeaderStatus,
            this.columnHeaderLastUpdate,
            this.columnHeaderEmployee});
            this.listViewOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewOrder.FullRowSelect = true;
            this.listViewOrder.GridLines = true;
            this.listViewOrder.Location = new System.Drawing.Point(7, 49);
            this.listViewOrder.MultiSelect = false;
            this.listViewOrder.Name = "listViewOrder";
            this.listViewOrder.Size = new System.Drawing.Size(722, 332);
            this.listViewOrder.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listViewOrder.TabIndex = 0;
            this.listViewOrder.UseCompatibleStateImageBehavior = false;
            this.listViewOrder.View = System.Windows.Forms.View.Details;
            this.listViewOrder.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewOrder_MouseDoubleClick);
            // 
            // columnHeaderOrderNr
            // 
            this.columnHeaderOrderNr.Text = "Bestellnr.";
            this.columnHeaderOrderNr.Width = 80;
            // 
            // columnHeaderPersonID
            // 
            this.columnHeaderPersonID.Text = "Kundennr.";
            this.columnHeaderPersonID.Width = 80;
            // 
            // columnHeaderName
            // 
            this.columnHeaderName.Text = "Kundenname";
            this.columnHeaderName.Width = 150;
            // 
            // columnHeaderOrderDate
            // 
            this.columnHeaderOrderDate.Text = "Bestelldatum";
            this.columnHeaderOrderDate.Width = 100;
            // 
            // columnHeaderStatus
            // 
            this.columnHeaderStatus.Text = "Status";
            this.columnHeaderStatus.Width = 100;
            // 
            // columnHeaderLastUpdate
            // 
            this.columnHeaderLastUpdate.Text = "Letzte Änderung";
            this.columnHeaderLastUpdate.Width = 105;
            // 
            // columnHeaderEmployee
            // 
            this.columnHeaderEmployee.Text = "Mitarbeiter";
            this.columnHeaderEmployee.Width = 80;
            // 
            // comboBoxStatus
            // 
            this.comboBoxStatus.FormattingEnabled = true;
            this.comboBoxStatus.Location = new System.Drawing.Point(126, 4);
            this.comboBoxStatus.Name = "comboBoxStatus";
            this.comboBoxStatus.Size = new System.Drawing.Size(98, 23);
            this.comboBoxStatus.TabIndex = 1;
            this.comboBoxStatus.Text = "Status";
            this.comboBoxStatus.SelectedIndexChanged += new System.EventHandler(this.comboBoxStatus_SelectedIndexChanged);
            // 
            // panelFilter
            // 
            this.panelFilter.BackColor = System.Drawing.Color.Silver;
            this.panelFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelFilter.Controls.Add(this.linkLabelResetFilter);
            this.panelFilter.Controls.Add(this.comboBoxName);
            this.panelFilter.Controls.Add(this.label1);
            this.panelFilter.Controls.Add(this.comboBoxStatus);
            this.panelFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelFilter.Location = new System.Drawing.Point(7, 18);
            this.panelFilter.Name = "panelFilter";
            this.panelFilter.Size = new System.Drawing.Size(722, 32);
            this.panelFilter.TabIndex = 2;
            // 
            // linkLabelResetFilter
            // 
            this.linkLabelResetFilter.AutoSize = true;
            this.linkLabelResetFilter.Location = new System.Drawing.Point(334, 8);
            this.linkLabelResetFilter.Name = "linkLabelResetFilter";
            this.linkLabelResetFilter.Size = new System.Drawing.Size(109, 15);
            this.linkLabelResetFilter.TabIndex = 5;
            this.linkLabelResetFilter.TabStop = true;
            this.linkLabelResetFilter.Text = "Filter zurücksetzen";
            this.linkLabelResetFilter.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelResetFilter_LinkClicked);
            // 
            // comboBoxName
            // 
            this.comboBoxName.FormattingEnabled = true;
            this.comboBoxName.Location = new System.Drawing.Point(230, 4);
            this.comboBoxName.Name = "comboBoxName";
            this.comboBoxName.Size = new System.Drawing.Size(98, 23);
            this.comboBoxName.TabIndex = 3;
            this.comboBoxName.Text = "Nachname";
            this.comboBoxName.SelectedIndexChanged += new System.EventHandler(this.comboBoxName_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Setze Filter nach:";

            // UserControlOrderList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelFilter);
            this.Controls.Add(this.listViewOrder);
            this.Name = "UserControlOrderList";
            this.Size = new System.Drawing.Size(751, 384);
            this.panelFilter.ResumeLayout(false);
            this.panelFilter.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listViewOrder;
        private System.Windows.Forms.ColumnHeader columnHeaderOrderNr;
        private System.Windows.Forms.ColumnHeader columnHeaderPersonID;
        private System.Windows.Forms.ColumnHeader columnHeaderName;
				private System.Windows.Forms.ColumnHeader columnHeaderOrderDate;
				private System.Windows.Forms.ColumnHeader columnHeaderStatus;
				private System.Windows.Forms.ColumnHeader columnHeaderLastUpdate;
				private System.Windows.Forms.ColumnHeader columnHeaderEmployee;
                private System.Windows.Forms.ComboBox comboBoxStatus;
                private System.Windows.Forms.Panel panelFilter;
                private System.Windows.Forms.Label label1;
                private System.Windows.Forms.ComboBox comboBoxName;
                private System.Windows.Forms.LinkLabel linkLabelResetFilter;
                private System.Drawing.Printing.PrintDocument printDocumentOrder;
    }
}
