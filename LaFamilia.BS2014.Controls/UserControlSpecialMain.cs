﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;
using LaFamilia.BS2014.Administration;

namespace LaFamilia.BS2014.Controls
{
    public partial class UserControlSpecialMain : UserControl
    {
        private Person person;
        private Special special;
        bool admin;

        //***********************************************************
        #region constructors

        public UserControlSpecialMain(Person person, bool admin)
        {
            InitializeComponent();

            this.person = person;
            this.admin = admin;

            this.pictureBoxActual.Image = null;
            this.pictureBoxNext.Image = Properties.Resources.plus;

            if (!this.admin)
                this.buttonNewSpecial.Visible = false;

            LoadData();

        }

        #endregion

        //*************************************************************
        #region private methods

        private void LoadData()
        {
            UserControlSpecial ucSpecialNext;

            this.flowLayoutPanelActualSpecials.Controls.Clear();
            this.flowLayoutPanelNextSpecials.Controls.Clear();


            foreach (Special special in Special.GetList().Where(s => s.DateFrom <= DateTime.Now && s.DateTo >= DateTime.Now))
            {
                UserControlSpecial ucSpecialActual = new UserControlSpecial(special, true, admin, this.person);
                ucSpecialActual.ShowSpecialShop += new EventHandler(ucSpecialActual_ShowSpecialShop);
                this.special = ucSpecialActual.Special;
                this.flowLayoutPanelActualSpecials.Controls.Add(ucSpecialActual);
            }

            foreach (Special special in Special.GetList().Where(s => s.DateFrom >= DateTime.Now && s.DateTo >= DateTime.Now))
            {
                if (admin)
                {
                    ucSpecialNext = new UserControlSpecial(special, true, admin, this.person);
                    this.flowLayoutPanelNextSpecials.Controls.Add(ucSpecialNext);
                    return;
                }
                ucSpecialNext = new UserControlSpecial(special, false, admin, this.person);
                this.flowLayoutPanelNextSpecials.Controls.Add(ucSpecialNext);
            }
        }


        void ucSpecialActual_ShowSpecialShop(object sender, EventArgs e)
        {
            this.Controls.Clear();
            UserControlSpecial ucspecial = sender as UserControlSpecial;

            UserControlShop ucSpecialShop = new UserControlShop(ucspecial.Special, this.person, false);
            this.Controls.Add(ucSpecialShop);
        }

        #endregion

        private void buttonNewSpecial_Click(object sender, EventArgs e)
        {
                FormSpecialDetails specialDetailsForm = new FormSpecialDetails();

                if (specialDetailsForm.ShowDialog() == DialogResult.OK)
                {
                    LoadData();
                }
                return;
        }

        private void pictureBoxActual_Click(object sender, EventArgs e)
        {
            this.flowLayoutPanelNextSpecials.Hide();
            this.flowLayoutPanelActualSpecials.Show();

            this.pictureBoxActual.Image = null;
            this.pictureBoxNext.Image = Properties.Resources.plus;

            this.panelHeadNextSpecials.Location = new Point(10, 400);

            LoadData();
        }

        private void pictureBoxNext_Click(object sender, EventArgs e)
        {
            this.flowLayoutPanelActualSpecials.Hide();

            this.pictureBoxActual.Image = Properties.Resources.plus;
            this.pictureBoxNext.Image = null;

            this.panelHeadNextSpecials.Location = new Point(10, 80);
            this.flowLayoutPanelNextSpecials.Location = new Point(10, 115);
            this.flowLayoutPanelNextSpecials.Height = 300;
            this.flowLayoutPanelNextSpecials.Show();
            LoadData();
        }
    }

}
