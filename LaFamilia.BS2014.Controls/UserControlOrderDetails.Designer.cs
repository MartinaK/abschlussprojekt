﻿namespace LaFamilia.BS2014.Controls
{
	partial class UserControlOrderDetails
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.labelOrderNr = new System.Windows.Forms.Label();
			this.textBoxOrderNr = new System.Windows.Forms.TextBox();
			this.labelIdentificationNr = new System.Windows.Forms.Label();
			this.labelName = new System.Windows.Forms.Label();
			this.labelStreet = new System.Windows.Forms.Label();
			this.labelPlz = new System.Windows.Forms.Label();
			this.labelLocation = new System.Windows.Forms.Label();
			this.labelLand = new System.Windows.Forms.Label();
			this.labelArtikel = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.labelOrderDate = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.userControlOrderedArticle1 = new LaFamilia.BS2014.Controls.UserControlOrderedArticle();
			this.label6 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// labelOrderNr
			// 
			this.labelOrderNr.AutoSize = true;
			this.labelOrderNr.Location = new System.Drawing.Point(3, 9);
			this.labelOrderNr.Name = "labelOrderNr";
			this.labelOrderNr.Size = new System.Drawing.Size(75, 13);
			this.labelOrderNr.TabIndex = 0;
			this.labelOrderNr.Text = "Bestellnummer";
			// 
			// textBoxOrderNr
			// 
			this.textBoxOrderNr.Enabled = false;
			this.textBoxOrderNr.Location = new System.Drawing.Point(6, 25);
			this.textBoxOrderNr.Name = "textBoxOrderNr";
			this.textBoxOrderNr.Size = new System.Drawing.Size(115, 20);
			this.textBoxOrderNr.TabIndex = 1;
			// 
			// labelIdentificationNr
			// 
			this.labelIdentificationNr.AutoSize = true;
			this.labelIdentificationNr.Location = new System.Drawing.Point(3, 3);
			this.labelIdentificationNr.Name = "labelIdentificationNr";
			this.labelIdentificationNr.Size = new System.Drawing.Size(59, 13);
			this.labelIdentificationNr.TabIndex = 2;
			this.labelIdentificationNr.Text = "Kundennr.:";
			// 
			// labelName
			// 
			this.labelName.AutoSize = true;
			this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelName.Location = new System.Drawing.Point(3, 25);
			this.labelName.Name = "labelName";
			this.labelName.Size = new System.Drawing.Size(137, 13);
			this.labelName.TabIndex = 4;
			this.labelName.Text = "Maximilian Mustermann";
			// 
			// labelStreet
			// 
			this.labelStreet.AutoSize = true;
			this.labelStreet.Location = new System.Drawing.Point(3, 44);
			this.labelStreet.Name = "labelStreet";
			this.labelStreet.Size = new System.Drawing.Size(83, 13);
			this.labelStreet.TabIndex = 6;
			this.labelStreet.Text = "Musterstraße 12";
			// 
			// labelPlz
			// 
			this.labelPlz.AutoSize = true;
			this.labelPlz.Location = new System.Drawing.Point(3, 67);
			this.labelPlz.Name = "labelPlz";
			this.labelPlz.Size = new System.Drawing.Size(37, 13);
			this.labelPlz.TabIndex = 7;
			this.labelPlz.Text = "51356";
			// 
			// labelLocation
			// 
			this.labelLocation.AutoSize = true;
			this.labelLocation.Location = new System.Drawing.Point(46, 67);
			this.labelLocation.Name = "labelLocation";
			this.labelLocation.Size = new System.Drawing.Size(74, 13);
			this.labelLocation.TabIndex = 8;
			this.labelLocation.Text = "Musterhausen";
			// 
			// labelLand
			// 
			this.labelLand.AutoSize = true;
			this.labelLand.Location = new System.Drawing.Point(3, 82);
			this.labelLand.Name = "labelLand";
			this.labelLand.Size = new System.Drawing.Size(59, 13);
			this.labelLand.TabIndex = 9;
			this.labelLand.Text = "Musterland";
			// 
			// labelArtikel
			// 
			this.labelArtikel.AutoSize = true;
			this.labelArtikel.Location = new System.Drawing.Point(13, 132);
			this.labelArtikel.Name = "labelArtikel";
			this.labelArtikel.Size = new System.Drawing.Size(79, 13);
			this.labelArtikel.TabIndex = 10;
			this.labelArtikel.Text = "Bestellte Artikel";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.labelLand);
			this.panel1.Controls.Add(this.labelIdentificationNr);
			this.panel1.Controls.Add(this.labelName);
			this.panel1.Controls.Add(this.labelStreet);
			this.panel1.Controls.Add(this.labelPlz);
			this.panel1.Controls.Add(this.labelLocation);
			this.panel1.Location = new System.Drawing.Point(228, 9);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(147, 103);
			this.panel1.TabIndex = 11;
			// 
			// labelOrderDate
			// 
			this.labelOrderDate.AutoSize = true;
			this.labelOrderDate.Location = new System.Drawing.Point(3, 53);
			this.labelOrderDate.Name = "labelOrderDate";
			this.labelOrderDate.Size = new System.Drawing.Size(67, 13);
			this.labelOrderDate.TabIndex = 12;
			this.labelOrderDate.Text = "Bestelldatum";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(6, 69);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(115, 20);
			this.textBox1.TabIndex = 13;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.tableLayoutPanel1);
			this.panel2.Controls.Add(this.userControlOrderedArticle1);
			this.panel2.Controls.Add(this.label1);
			this.panel2.Location = new System.Drawing.Point(6, 118);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(369, 235);
			this.panel2.TabIndex = 14;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 3);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(79, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Bestellte Artikel";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(4, 1);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(32, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Anz.";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(113, 1);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(84, 13);
			this.label3.TabIndex = 2;
			this.label3.Text = "Beschreibung";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(258, 1);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(24, 13);
			this.label4.TabIndex = 3;
			this.label4.Text = "Gr.";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(298, 1);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(35, 13);
			this.label5.TabIndex = 4;
			this.label5.Text = "Preis";
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel1.ColumnCount = 5;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.69725F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.30275F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 144F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 39F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
			this.tableLayoutPanel1.Controls.Add(this.label6, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.label3, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.label4, 2, 0);
			this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.label5, 4, 0);
			this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 19);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(342, 22);
			this.tableLayoutPanel1.TabIndex = 5;
			// 
			// userControlOrderedArticle1
			// 
			this.userControlOrderedArticle1.Location = new System.Drawing.Point(10, 45);
			this.userControlOrderedArticle1.Name = "userControlOrderedArticle1";
			this.userControlOrderedArticle1.Size = new System.Drawing.Size(342, 30);
			this.userControlOrderedArticle1.TabIndex = 5;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(44, 1);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(58, 13);
			this.label6.TabIndex = 5;
			this.label6.Text = "Artikelnr.";
			// 
			// UserControlOrderDetails
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.labelOrderDate);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.labelArtikel);
			this.Controls.Add(this.textBoxOrderNr);
			this.Controls.Add(this.labelOrderNr);
			this.Enabled = false;
			this.Name = "UserControlOrderDetails";
			this.Size = new System.Drawing.Size(385, 363);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label labelOrderNr;
		private System.Windows.Forms.TextBox textBoxOrderNr;
		private System.Windows.Forms.Label labelIdentificationNr;
		private System.Windows.Forms.Label labelName;
		private System.Windows.Forms.Label labelStreet;
		private System.Windows.Forms.Label labelPlz;
		private System.Windows.Forms.Label labelLocation;
		private System.Windows.Forms.Label labelLand;
		private System.Windows.Forms.Label labelArtikel;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label labelOrderDate;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label1;
		private UserControlOrderedArticle userControlOrderedArticle1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label6;
	}
}
