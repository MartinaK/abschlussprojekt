﻿namespace LaFamilia.BS2014.Controls
{
    partial class UserControlArticleThumb
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelName = new System.Windows.Forms.Label();
            this.labelPreis = new System.Windows.Forms.Label();
            this.pictureBoxImage = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelBrand = new System.Windows.Forms.Label();
            this.labelUVP = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.Location = new System.Drawing.Point(13, 17);
            this.labelName.MaximumSize = new System.Drawing.Size(137, 37);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(137, 34);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Artikelname";
            this.labelName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPreis
            // 
            this.labelPreis.Location = new System.Drawing.Point(16, 59);
            this.labelPreis.Name = "labelPreis";
            this.labelPreis.Size = new System.Drawing.Size(134, 21);
            this.labelPreis.TabIndex = 2;
            this.labelPreis.Text = "Preis €";
            this.labelPreis.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBoxImage
            // 
            this.pictureBoxImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxImage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxImage.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxImage.Name = "pictureBoxImage";
            this.pictureBoxImage.Size = new System.Drawing.Size(163, 112);
            this.pictureBoxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxImage.TabIndex = 0;
            this.pictureBoxImage.TabStop = false;
            this.pictureBoxImage.Click += new System.EventHandler(this.pictureBoxImage_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.labelBrand);
            this.panel1.Controls.Add(this.labelUVP);
            this.panel1.Controls.Add(this.labelName);
            this.panel1.Controls.Add(this.labelPreis);
            this.panel1.Location = new System.Drawing.Point(3, 115);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(163, 110);
            this.panel1.TabIndex = 3;
            // 
            // labelBrand
            // 
            this.labelBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBrand.Location = new System.Drawing.Point(13, -1);
            this.labelBrand.MaximumSize = new System.Drawing.Size(137, 37);
            this.labelBrand.Name = "labelBrand";
            this.labelBrand.Size = new System.Drawing.Size(137, 18);
            this.labelBrand.TabIndex = 4;
            this.labelBrand.Text = "Brand";
            this.labelBrand.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelUVP
            // 
            this.labelUVP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Strikeout, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUVP.ForeColor = System.Drawing.Color.Gray;
            this.labelUVP.Location = new System.Drawing.Point(16, 80);
            this.labelUVP.Name = "labelUVP";
            this.labelUVP.Size = new System.Drawing.Size(134, 28);
            this.labelUVP.TabIndex = 3;
            this.labelUVP.Text = "UVP:";
            this.labelUVP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UserControlArticleThumb
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBoxImage);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "UserControlArticleThumb";
            this.Size = new System.Drawing.Size(180, 228);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxImage;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelPreis;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelUVP;
        private System.Windows.Forms.Label labelBrand;
    }
}
