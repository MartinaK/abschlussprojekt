﻿namespace LaFamilia.BS2014.Controls
{
    partial class UserControlShop
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlShop));
			this.treeViewOutlet = new System.Windows.Forms.TreeView();
			this.flowLayoutPanelArticles = new System.Windows.Forms.FlowLayoutPanel();
			this.pictureBoxImage = new System.Windows.Forms.PictureBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panelFilter = new System.Windows.Forms.Panel();
			this.comboBoxBrand = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.comboBoxSize = new System.Windows.Forms.ComboBox();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).BeginInit();
			this.panel1.SuspendLayout();
			this.panelFilter.SuspendLayout();
			this.SuspendLayout();
			// 
			// treeViewOutlet
			// 
			this.treeViewOutlet.BackColor = System.Drawing.Color.WhiteSmoke;
			this.treeViewOutlet.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.treeViewOutlet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.treeViewOutlet.Location = new System.Drawing.Point(3, 78);
			this.treeViewOutlet.Name = "treeViewOutlet";
			this.treeViewOutlet.Size = new System.Drawing.Size(201, 321);
			this.treeViewOutlet.TabIndex = 0;
			this.treeViewOutlet.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeViewOutlet_AfterSelect);
			// 
			// flowLayoutPanelArticles
			// 
			this.flowLayoutPanelArticles.AutoScroll = true;
			this.flowLayoutPanelArticles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.flowLayoutPanelArticles.Location = new System.Drawing.Point(205, 34);
			this.flowLayoutPanelArticles.Name = "flowLayoutPanelArticles";
			this.flowLayoutPanelArticles.Size = new System.Drawing.Size(526, 365);
			this.flowLayoutPanelArticles.TabIndex = 1;
			// 
			// pictureBoxImage
			// 
			this.pictureBoxImage.BackColor = System.Drawing.Color.WhiteSmoke;
			this.pictureBoxImage.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxImage.Image")));
			this.pictureBoxImage.Location = new System.Drawing.Point(3, 4);
			this.pictureBoxImage.Name = "pictureBoxImage";
			this.pictureBoxImage.Size = new System.Drawing.Size(201, 74);
			this.pictureBoxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.pictureBoxImage.TabIndex = 0;
			this.pictureBoxImage.TabStop = false;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.panelFilter);
			this.panel1.Controls.Add(this.pictureBoxImage);
			this.panel1.Controls.Add(this.flowLayoutPanelArticles);
			this.panel1.Controls.Add(this.treeViewOutlet);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(772, 418);
			this.panel1.TabIndex = 3;
			// 
			// panelFilter
			// 
			this.panelFilter.BackColor = System.Drawing.Color.Silver;
			this.panelFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panelFilter.Controls.Add(this.comboBoxBrand);
			this.panelFilter.Controls.Add(this.label1);
			this.panelFilter.Controls.Add(this.comboBoxSize);
			this.panelFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.panelFilter.Location = new System.Drawing.Point(205, 3);
			this.panelFilter.Name = "panelFilter";
			this.panelFilter.Size = new System.Drawing.Size(526, 32);
			this.panelFilter.TabIndex = 3;
			// 
			// comboBoxBrand
			// 
			this.comboBoxBrand.Enabled = false;
			this.comboBoxBrand.FormattingEnabled = true;
			this.comboBoxBrand.Location = new System.Drawing.Point(230, 4);
			this.comboBoxBrand.Name = "comboBoxBrand";
			this.comboBoxBrand.Size = new System.Drawing.Size(98, 23);
			this.comboBoxBrand.TabIndex = 3;
			this.comboBoxBrand.Text = "Marke";
			this.comboBoxBrand.SelectedIndexChanged += new System.EventHandler(this.comboBoxBrand_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(4, 7);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(101, 15);
			this.label1.TabIndex = 2;
			this.label1.Text = "Setze Filter nach:";
			// 
			// comboBoxSize
			// 
			this.comboBoxSize.Enabled = false;
			this.comboBoxSize.FormattingEnabled = true;
			this.comboBoxSize.Location = new System.Drawing.Point(126, 4);
			this.comboBoxSize.Name = "comboBoxSize";
			this.comboBoxSize.Size = new System.Drawing.Size(98, 23);
			this.comboBoxSize.TabIndex = 1;
			this.comboBoxSize.Text = "Größe";
			this.comboBoxSize.SelectedIndexChanged += new System.EventHandler(this.comboBoxSize_SelectedIndexChanged);
			// 
			// UserControlShop
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Controls.Add(this.panel1);
			this.Name = "UserControlShop";
			this.Size = new System.Drawing.Size(772, 435);
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panelFilter.ResumeLayout(false);
			this.panelFilter.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeViewOutlet;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelArticles;
        private System.Windows.Forms.PictureBox pictureBoxImage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelFilter;
        private System.Windows.Forms.ComboBox comboBoxBrand;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxSize;
    }
}
