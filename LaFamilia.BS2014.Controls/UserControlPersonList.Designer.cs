﻿namespace LaFamilia.BS2014.Controls
{
    partial class UserControlPersonList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewCustomer = new System.Windows.Forms.ListView();
            this.columnHeaderCustomerID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPLZLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderCountry = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderTel = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panelFilter = new System.Windows.Forms.Panel();
            this.comboBoxFilterPersonType = new System.Windows.Forms.ComboBox();
            this.linkLabelResetFilter = new System.Windows.Forms.LinkLabel();
            this.buttonNewCustomer = new System.Windows.Forms.Button();
            this.comboBoxCountry = new System.Windows.Forms.ComboBox();
            this.comboBoxLastName = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panelFilter.SuspendLayout();
            this.SuspendLayout();
            // 
            // listViewCustomer
            // 
            this.listViewCustomer.BackColor = System.Drawing.Color.WhiteSmoke;
            this.listViewCustomer.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderCustomerID,
            this.columnHeaderName,
            this.columnHeaderPLZLocation,
            this.columnHeaderCountry,
            this.columnHeaderTel});
            this.listViewCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewCustomer.FullRowSelect = true;
            this.listViewCustomer.GridLines = true;
            this.listViewCustomer.Location = new System.Drawing.Point(7, 49);
            this.listViewCustomer.MultiSelect = false;
            this.listViewCustomer.Name = "listViewCustomer";
            this.listViewCustomer.Size = new System.Drawing.Size(722, 332);
            this.listViewCustomer.TabIndex = 1;
            this.listViewCustomer.UseCompatibleStateImageBehavior = false;
            this.listViewCustomer.View = System.Windows.Forms.View.Details;
            this.listViewCustomer.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewOrder_MouseDoubleClick);
            // 
            // columnHeaderCustomerID
            // 
            this.columnHeaderCustomerID.Text = "Kundennr.";
            this.columnHeaderCustomerID.Width = 100;
            // 
            // columnHeaderName
            // 
            this.columnHeaderName.Text = "Name";
            this.columnHeaderName.Width = 150;
            // 
            // columnHeaderPLZLocation
            // 
            this.columnHeaderPLZLocation.Text = "Ort";
            this.columnHeaderPLZLocation.Width = 155;
            // 
            // columnHeaderCountry
            // 
            this.columnHeaderCountry.Text = "Land";
            this.columnHeaderCountry.Width = 130;
            // 
            // columnHeaderTel
            // 
            this.columnHeaderTel.Text = "Telefon";
            this.columnHeaderTel.Width = 120;
            // 
            // panelFilter
            // 
            this.panelFilter.BackColor = System.Drawing.Color.Silver;
            this.panelFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelFilter.Controls.Add(this.comboBoxFilterPersonType);
            this.panelFilter.Controls.Add(this.linkLabelResetFilter);
            this.panelFilter.Controls.Add(this.buttonNewCustomer);
            this.panelFilter.Controls.Add(this.comboBoxCountry);
            this.panelFilter.Controls.Add(this.comboBoxLastName);
            this.panelFilter.Controls.Add(this.label1);
            this.panelFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelFilter.Location = new System.Drawing.Point(7, 18);
            this.panelFilter.Name = "panelFilter";
            this.panelFilter.Size = new System.Drawing.Size(722, 32);
            this.panelFilter.TabIndex = 3;
            // 
            // comboBoxFilterPersonType
            // 
            this.comboBoxFilterPersonType.FormattingEnabled = true;
            this.comboBoxFilterPersonType.Location = new System.Drawing.Point(111, 4);
            this.comboBoxFilterPersonType.Name = "comboBoxFilterPersonType";
            this.comboBoxFilterPersonType.Size = new System.Drawing.Size(91, 23);
            this.comboBoxFilterPersonType.TabIndex = 7;
            this.comboBoxFilterPersonType.Text = "Mitarbeiter";
            this.comboBoxFilterPersonType.SelectedIndexChanged += new System.EventHandler(this.comboBoxFilterPersonType_SelectedIndexChanged);
            // 
            // linkLabelResetFilter
            // 
            this.linkLabelResetFilter.AutoSize = true;
            this.linkLabelResetFilter.Location = new System.Drawing.Point(416, 7);
            this.linkLabelResetFilter.Name = "linkLabelResetFilter";
            this.linkLabelResetFilter.Size = new System.Drawing.Size(109, 15);
            this.linkLabelResetFilter.TabIndex = 5;
            this.linkLabelResetFilter.TabStop = true;
            this.linkLabelResetFilter.Text = "Filter zurücksetzen";
            this.linkLabelResetFilter.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelResetFilter_LinkClicked);
            // 
            // buttonNewCustomer
            // 
            this.buttonNewCustomer.BackColor = System.Drawing.Color.MediumVioletRed;
            this.buttonNewCustomer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonNewCustomer.ForeColor = System.Drawing.Color.White;
            this.buttonNewCustomer.Location = new System.Drawing.Point(574, 4);
            this.buttonNewCustomer.Name = "buttonNewCustomer";
            this.buttonNewCustomer.Size = new System.Drawing.Size(137, 23);
            this.buttonNewCustomer.TabIndex = 6;
            this.buttonNewCustomer.Text = "Neue Person";
            this.buttonNewCustomer.UseVisualStyleBackColor = false;
            this.buttonNewCustomer.Click += new System.EventHandler(this.buttonNewCustomer_Click);
            // 
            // comboBoxCountry
            // 
            this.comboBoxCountry.FormattingEnabled = true;
            this.comboBoxCountry.Location = new System.Drawing.Point(312, 4);
            this.comboBoxCountry.Name = "comboBoxCountry";
            this.comboBoxCountry.Size = new System.Drawing.Size(98, 23);
            this.comboBoxCountry.TabIndex = 5;
            this.comboBoxCountry.Text = "Land";
            this.comboBoxCountry.SelectedIndexChanged += new System.EventHandler(this.comboBoxCountry_SelectedIndexChanged);
            // 
            // comboBoxLastName
            // 
            this.comboBoxLastName.FormattingEnabled = true;
            this.comboBoxLastName.Location = new System.Drawing.Point(208, 4);
            this.comboBoxLastName.Name = "comboBoxLastName";
            this.comboBoxLastName.Size = new System.Drawing.Size(98, 23);
            this.comboBoxLastName.TabIndex = 3;
            this.comboBoxLastName.Text = "Nachname";
            this.comboBoxLastName.SelectedIndexChanged += new System.EventHandler(this.comboBoxLastName_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Setze Filter nach:";
            // 
            // UserControlPersonList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelFilter);
            this.Controls.Add(this.listViewCustomer);
            this.Name = "UserControlPersonList";
            this.Size = new System.Drawing.Size(751, 384);
            this.panelFilter.ResumeLayout(false);
            this.panelFilter.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listViewCustomer;
        private System.Windows.Forms.ColumnHeader columnHeaderCustomerID;
        private System.Windows.Forms.ColumnHeader columnHeaderName;
        private System.Windows.Forms.ColumnHeader columnHeaderPLZLocation;
        private System.Windows.Forms.ColumnHeader columnHeaderCountry;
        private System.Windows.Forms.ColumnHeader columnHeaderTel;
        private System.Windows.Forms.Panel panelFilter;
        private System.Windows.Forms.ComboBox comboBoxLastName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxCountry;
                private System.Windows.Forms.Button buttonNewCustomer;
                private System.Windows.Forms.LinkLabel linkLabelResetFilter;
                private System.Windows.Forms.ComboBox comboBoxFilterPersonType;
    }
}
