﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Controls;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Controls
{
    public partial class FormOrder : Form
    {
        private Order order;
        private Person employee;
        private UserControlOrderedArticle ucOrderedArticle;
        double shippingCosts = 4.95;

        //Fürs Drucken
        int currentIndex = 0;
        int currentPage = 0;
        Font stdFont = new Font("Arial", 11f);



        //*******************************************************************************
        #region constructors

        public FormOrder()
        {
            InitializeComponent();

        }

        public FormOrder(Order order, Person employee)
        {
            InitializeComponent();

            this.order = order;
            this.employee = employee;


            if (employee == null)
            {
                this.textBoxFirstName.Enabled = false;
                this.textBoxLastName.Enabled = false;
                this.textBoxStreet.Enabled = false;
                this.textBoxPostalcode.Enabled = false;
                this.textBoxLocation.Enabled = false;
                this.textBoxCountry.Enabled = false;
                this.buttonCreateInvoice.Visible = false;
                this.labelStatus.Visible = false;
                this.comboBoxStatus.Visible = false;
            }

            UserControlHeader ucHeader = new UserControlHeader();
            this.panelHeader.Controls.Add(ucHeader);

            UserControlFooter ucFooter = new UserControlFooter();
            this.panelFooter.Controls.Add(ucFooter);

            this.Text = "LaFamilia | BS2014 | Bestellnr.: " + this.order.OrderNr;
            this.labelOrderNr_Text.Text = this.order.OrderNr.ToString();
            this.labelOrderDate_Text.Text = this.order.OrderDate.Value.ToShortDateString();

            this.labelIdentificationNr_Text.Text = this.order.Person.IdentificationNumber;
            this.textBoxFirstName.Text = this.order.Person.Firstname;
            this.textBoxLastName.Text = this.order.Person.Lastname;
            this.textBoxStreet.Text = this.order.Person.Address.Street;
            this.textBoxPostalcode.Text = this.order.Person.Address.Postalcode;
            this.textBoxLocation.Text = this.order.Person.Address.Location;
            this.textBoxCountry.Text = this.order.Person.Address.Country;

            this.comboBoxStatus.DataSource = Enum.GetValues(typeof(OrderStatus));
            this.comboBoxStatus.Text = this.order.Status;

            if (this.comboBoxStatus.Text == OrderStatus.Rechnung.ToString() ||
                this.comboBoxStatus.Text == OrderStatus.Erledigt.ToString() ||
                this.comboBoxStatus.Text == OrderStatus.Storniert.ToString() ||
                this.comboBoxStatus.Text == OrderStatus.Zurückgewiesen.ToString())
            {
                this.buttonCreateInvoice.Visible = false;
            }

            foreach (ArticleOrdered article in order.ArticlesOrdered)
            {
                ucOrderedArticle = new UserControlOrderedArticle(article, true);
                this.flowLayoutPanelArticles.Controls.Add(ucOrderedArticle);
            }
        }
        #endregion

        //**************************************************************************
        #region events

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.order.Person.Firstname = this.textBoxFirstName.Text;
            this.order.Person.Lastname = this.textBoxLastName.Text;
            this.order.Person.Address.Street = this.textBoxStreet.Text;
            this.order.Person.Address.Postalcode = this.textBoxPostalcode.Text;
            this.order.Person.Address.Location = this.textBoxLocation.Text;
            this.order.Person.Address.Country = this.textBoxCountry.Text;
            this.order.Status = this.comboBoxStatus.SelectedItem.ToString();

            if (this.employee != null)
            {
                this.order.LastUpdate = DateTime.Now;
                this.order.NameShortcut = this.employee.NameShortcut;
            }

            this.order.Save();

            this.Close();

        }


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonCreateInvoice_Click(object sender, EventArgs e)
        {
            Invoice invoice = new Invoice();
            invoice.OrderId = this.order.OrderId;
            invoice.PersonId = this.order.Person.PersonId;
            invoice.InvoiceNumber = DateTime.Now.ToString("yyyyMMdd") + "/" + this.order.OrderNr;
            invoice.InvoiceDate = DateTime.Now;
            invoice.Status = InvoiceStatus.Offen.ToString();
            invoice.LastUpdate = DateTime.Now;
            invoice.NameShortcut = this.employee.NameShortcut;

            foreach (Invoice i in Invoice.GetList())
            {
                if (i.InvoiceNumber.Equals(invoice.InvoiceNumber))
                {
                    MessageBox.Show("Es existiert bereits eine Rechnung! Bitte ändern Sie den Status!");
                    return;
                }
            }

            invoice.Save();

            this.comboBoxStatus.SelectedItem = OrderStatus.Rechnung;
        }


        private void printDocumentOrder_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            SizeF size = SizeF.Empty;
            float xOffset = 25;

            float lastY = 25;

            //Header
            lastY = PrintHeader(e);
            //Footer
            PrintFooter(e);

            //Tabellenkopf
            lastY = PrintListHeader(lastY + 20, e);
            lastY += 5;

            //Tabellenzeilen hinzufügen und prüfen ob mehrzeilig
            for (int index = this.currentIndex; index < this.order.ArticlesOrdered.Count(); index++, this.currentIndex++)
            {
                size = e.Graphics.MeasureString(this.order.ArticlesOrdered[this.currentIndex].Price.ToString(), stdFont);

                if (lastY + size.Height > e.PageSettings.PrintableArea.Height - 400 && this.currentIndex < this.order.ArticlesOrdered.Count() - 1)
                {
                    this.currentPage++;
                    e.HasMorePages = true;
                    break;
                }

                lastY = PrintZeile(lastY, this.order.ArticlesOrdered[this.currentIndex], e);
            }

            //Tabellenende
            if (this.currentIndex == this.order.ArticlesOrdered.Count())
            {
                lastY += 15;
                e.Graphics.DrawLine(new Pen(Brushes.Black, 1f), new Point((int)e.PageSettings.PrintableArea.X + (int)xOffset, (int)lastY), new Point((int)e.PageSettings.PrintableArea.X + (int)e.PageSettings.PrintableArea.Width - ((int)xOffset * 2), (int)lastY));
                lastY += 3;
                e.Graphics.DrawLine(new Pen(Brushes.Black, 1f), new Point((int)e.PageSettings.PrintableArea.X + (int)xOffset, (int)lastY), new Point((int)e.PageSettings.PrintableArea.X + (int)e.PageSettings.PrintableArea.Width - ((int)xOffset * 2), (int)lastY));
                lastY += 15;
                e.Graphics.DrawString("Versandkosten: " + this.shippingCosts.ToString() + " €", stdFont, Brushes.Black, new PointF(xOffset + 500, lastY));
                lastY += 15;
                e.Graphics.DrawString("Summe: " + this.order.TotalCosts.ToString() + " €", stdFont, Brushes.Black, new PointF(xOffset + 551, lastY));
                lastY += 30;
                e.Graphics.DrawString("Ihre Bestellung wird voraussichtlich am " + this.order.DeliveryDate.Value.ToShortDateString() + " geliefert.\r\nBei Fragen wenden Sie sich bitte an unseren Kundendienst.\r\nVielen Dank, dass Sie bei uns bestellt haben! ", stdFont, Brushes.Black, new PointF(xOffset, lastY));
                this.currentPage = 0;
                this.currentIndex = 0;
            }

        }
        #endregion
        //***************************************************************************************************
        #region Print Methods

        private float PrintHeader(System.Drawing.Printing.PrintPageEventArgs e)
        {
            Font headerFont = new Font("Arial", 14f);

            float lastY = 15;
            float xOffset = 25;

            e.Graphics.DrawImage(Properties.Resources.head, 0, 0);

            lastY += ((int)(Properties.Resources.head.Height / 3));
            e.Graphics.DrawString("La Familia | Musterstraße 10 | 5020 Musterhausen | Tel.: 01234 / 15616 | Mail: office@la-familia.at", stdFont, Brushes.Black, new PointF(xOffset + 20, lastY));

            SizeF size = e.Graphics.MeasureString("La Familia", stdFont);
            lastY += size.Height;

            e.Graphics.DrawLine(new Pen(Brushes.Black, 2f), new Point((int)e.PageSettings.PrintableArea.X + (int)xOffset, (int)lastY), new Point((int)e.PageSettings.PrintableArea.X + (int)e.PageSettings.PrintableArea.Width - ((int)xOffset * 2), (int)lastY));

            string text = "An\r\n" + this.order.Person.Firstname.ToString() + " " + this.order.Person.Lastname.ToString() + "\r\n" + this.order.Person.Address.Street.ToString() + "\r\n" +
                this.order.Person.Address.Postalcode.ToString() + this.order.Person.Address.Location.ToString() + "\r\n" + this.order.Person.Address.Country.ToString();
            size = e.Graphics.MeasureString(text, stdFont);
            e.Graphics.DrawString(text, stdFont, Brushes.Black, new PointF(xOffset + 10, lastY + 50));

            text = "Kundennummer: " + this.order.Person.IdentificationNumber + "\r\nBestellnummer: " + this.order.OrderNr + "\r\nBestelldatum: " + this.order.OrderDate.Value.ToShortDateString();
            e.Graphics.DrawString(text, stdFont, Brushes.Black, new PointF(e.PageSettings.PrintableArea.X + e.PageSettings.PrintableArea.Width - size.Width - (xOffset * 7), lastY + size.Height - 10));

            lastY += size.Height + 80;
            text = "Bestellung Nr.: " + this.order.OrderNr;
            size = e.Graphics.MeasureString(text, headerFont);
            e.Graphics.DrawString(text, new Font(headerFont, FontStyle.Bold), Brushes.Black, new PointF(xOffset + 5, lastY));

            return lastY;
        }

        private float PrintListHeader(float pLastY, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Font headerFont = new Font("Arial", 12f);
            float lastY = pLastY;
            float xOffset = 25;

            lastY += 15;
            e.Graphics.DrawLine(new Pen(Brushes.Black, 2f), new Point((int)e.PageSettings.PrintableArea.X + (int)xOffset, (int)lastY), new Point((int)e.PageSettings.PrintableArea.X + (int)e.PageSettings.PrintableArea.Width - ((int)xOffset * 2), (int)lastY));
            lastY += 5;
            SizeF size = e.Graphics.MeasureString("Anz. Artikelnr. Beschreibung Gr. Preis", headerFont);
            e.Graphics.DrawString("Anz.", headerFont, Brushes.Black, new PointF(xOffset + 15, lastY));
            e.Graphics.DrawString("Artikelnr.", headerFont, Brushes.Black, new PointF(xOffset + 65, lastY));
            e.Graphics.DrawString("Beschreibung", headerFont, Brushes.Black, new PointF(xOffset + 180, lastY));
            e.Graphics.DrawString("Gr.", headerFont, Brushes.Black, new PointF(xOffset + 580, lastY));
            e.Graphics.DrawString("Preis", headerFont, Brushes.Black, new PointF(xOffset + 650, lastY));

            lastY += size.Height + 3;
            e.Graphics.DrawLine(new Pen(Brushes.Black, 1f), new Point((int)e.PageSettings.PrintableArea.X + (int)xOffset, (int)lastY), new Point((int)e.PageSettings.PrintableArea.X + (int)e.PageSettings.PrintableArea.Width - ((int)xOffset * 2), (int)lastY));
            lastY += 2;
            e.Graphics.DrawLine(new Pen(Brushes.Black, 1f), new Point((int)e.PageSettings.PrintableArea.X + (int)xOffset, (int)lastY), new Point((int)e.PageSettings.PrintableArea.X + (int)e.PageSettings.PrintableArea.Width - ((int)xOffset * 2), (int)lastY));
            return lastY;
        }

        private float PrintZeile(float pLastY, ArticleOrdered article, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float lastY = pLastY;
            float xOffset = 20;

            lastY += 5;
            SizeF size = e.Graphics.MeasureString("Artikelanzahl", stdFont);
            e.Graphics.DrawString(article.Quantity.ToString(), stdFont, Brushes.Black, new PointF(xOffset + 15, lastY));
            e.Graphics.DrawString(article.ArticleNumber, stdFont, Brushes.Black, new PointF(xOffset + 65, lastY));
            e.Graphics.DrawString(article.Title, stdFont, Brushes.Black, new PointF(xOffset + 180, lastY));
            e.Graphics.DrawString("Size", stdFont, Brushes.Black, new PointF(xOffset + 580, lastY));
            e.Graphics.DrawString(article.Price.ToString(), stdFont, Brushes.Black, new PointF(xOffset + 650, lastY));
            lastY += size.Height;

            return lastY;
        }

        private void PrintFooter(System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(Properties.Resources.footer, 0, 1042);

            int y = Properties.Resources.footer.Height;
            int y2 = (int)e.PageSettings.PrintableArea.Height;
        }

        #endregion

        private void pictureBoxPrint_Click(object sender, EventArgs e)
        {
            PrintPreviewDialog dialog = new PrintPreviewDialog();
            dialog.Document = this.printDocumentOrder;

            dialog.ShowDialog();
        }
    }

}
