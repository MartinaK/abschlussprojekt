﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Controls
{
	public partial class UserControlInvoiceList : UserControl
	{
		private InvoiceStatus currentFilter;
		private List<Invoice> showableInvoices;
        private Invoice invoice;
        double shippingCosts = 4.95;

        //Fürs Drucken
        int currentIndex = 0;
        int currentPage = 0;
        Font stdFont = new Font("Arial", 11f);


		//*******************************************************************************
		#region constructors

		public UserControlInvoiceList(List<Invoice> showableInvoices)
		{
			InitializeComponent();

            this.showableInvoices = showableInvoices;

            this.comboBoxStatus.DataSource = Enum.GetValues(typeof(InvoiceStatus));

            foreach (Invoice invoice in Invoice.GetList())
            {
                this.comboBoxLastName.Items.Add(invoice.Order.Person.Lastname);
            }

			LoadData(showableInvoices);
		}

		#endregion

		//*********************************************************************************
		#region private methods

		private void LoadData(List<Invoice> showableInvoices)
		{
			ListViewItem item = null;
			this.listViewInvoice.Items.Clear();

			foreach (Invoice invoice in showableInvoices)
			{
				item = new ListViewItem();
				item.Tag = invoice;
				item.Text = invoice.InvoiceNumber.ToString();
				item.SubItems.Add(invoice.InvoiceDate.Value.ToShortDateString());
				item.SubItems.Add(invoice.Order.Person.IdentificationNumber.ToString());
				item.SubItems.Add(invoice.Order.Person.Firstname + " " + invoice.Order.Person.Lastname);
				item.SubItems.Add(invoice.Status);
				item.SubItems.Add(invoice.LastUpdate.ToShortDateString());
				item.SubItems.Add(invoice.NameShortcut);
				this.listViewInvoice.Items.Add(item);
			}
		}

		//setzt den Filter über den Invoice Status
		private List<Invoice> SetFilter(InvoiceStatus filter)
		{
			this.showableInvoices = Invoice.GetList();

			this.currentFilter = filter;

			if (filter ==  InvoiceStatus.Status)
			{
				this.comboBoxStatus.Text = "Status";
				return showableInvoices;
			}

			showableInvoices = Invoice.GetList().Where(i => (i.StatusValue & filter) == i.StatusValue).ToList<Invoice>();
			return showableInvoices;
		}

		#endregion

		private void comboBoxStatus_SelectedIndexChanged(object sender, EventArgs e)
		{
			InvoiceStatus filter = (InvoiceStatus)this.comboBoxStatus.SelectedItem;
			LoadData(SetFilter(filter));
		}

        private void comboBoxLastName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string filterName = this.comboBoxLastName.SelectedItem.ToString();
            this.showableInvoices = this.showableInvoices.Where(i => i.Order.Person.Lastname.Contains(filterName)).ToList<Invoice>();
            LoadData(this.showableInvoices);
            this.showableInvoices = Invoice.GetList();
        }

        private void linkLabelResetFilter_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.comboBoxLastName.Text = "Nachname";
            this.comboBoxStatus.Text = "Status";
            LoadData(Invoice.GetList());
        }

        private void listViewInvoice_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewItem item = this.listViewInvoice.GetItemAt(e.X, e.Y);

            this.invoice = (Invoice)item.Tag;

            FormInvoice invoiceForm = new FormInvoice(this.invoice, true);
            invoiceForm.Show();
        }

        private void pictureBoxPrint_Click(object sender, EventArgs e)
        {
            PrintPreviewDialog dialog = new PrintPreviewDialog();
            dialog.Document = this.printDocumentInvoice;

            dialog.ShowDialog();
        }

        private void printDocumentInvoice_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            this.invoice = (Invoice)this.listViewInvoice.SelectedItems[0].Tag;

            SizeF size = SizeF.Empty;
            float xOffset = 25;

            float lastY = 25;

            //Header
            lastY = PrintHeader(e);
            //Footer
            PrintFooter(e);

            //Tabellenkopf
            lastY = PrintListHeader(lastY + 20, e);
            lastY += 5;

            //Tabellenzeilen hinzufügen und prüfen ob mehrzeilig
            for (int index = this.currentIndex; index < this.invoice.Order.ArticlesOrdered.Count(); index++, this.currentIndex++)
            {
                size = e.Graphics.MeasureString(this.invoice.Order.ArticlesOrdered[this.currentIndex].Price.ToString(), stdFont);

                if (lastY + size.Height > e.PageSettings.PrintableArea.Height - 400 && this.currentIndex < this.invoice.Order.ArticlesOrdered.Count() - 1)
                {
                    this.currentPage++;
                    e.HasMorePages = true;
                    break;
                }

                lastY = PrintZeile(lastY, this.invoice.Order.ArticlesOrdered[this.currentIndex], e);
            }

            //Tabellenende
            if (this.currentIndex == this.invoice.Order.ArticlesOrdered.Count())
            {
                lastY += 15;
                e.Graphics.DrawLine(new Pen(Brushes.Black, 1f), new Point((int)e.PageSettings.PrintableArea.X + (int)xOffset, (int)lastY), new Point((int)e.PageSettings.PrintableArea.X + (int)e.PageSettings.PrintableArea.Width - ((int)xOffset * 2), (int)lastY));
                lastY += 3;
                e.Graphics.DrawLine(new Pen(Brushes.Black, 1f), new Point((int)e.PageSettings.PrintableArea.X + (int)xOffset, (int)lastY), new Point((int)e.PageSettings.PrintableArea.X + (int)e.PageSettings.PrintableArea.Width - ((int)xOffset * 2), (int)lastY));
                lastY += 15;
                e.Graphics.DrawString("Versandkosten: " + this.shippingCosts.ToString() + " €", stdFont, Brushes.Black, new PointF(xOffset + 500, lastY));
                lastY += 15;
                e.Graphics.DrawString("Summe: " + this.invoice.TotalCosts.ToString() + " €", stdFont, Brushes.Black, new PointF(xOffset + 551, lastY));
                lastY += 30;
                e.Graphics.DrawString("Bitte überweisen Sie den Gesamtbetrag in Höhe von " + this.invoice.TotalCosts.ToString() +
                    " € binnen 14 Tage auf das unten angeführte Konto.\r\nVielen Dank für Ihren Einkauf!", stdFont, Brushes.Black, new PointF(xOffset, lastY));
                this.currentPage = 0;
                this.currentIndex = 0;
            }

        }

        //***************************************************************************************************
        #region Print Methods

        private float PrintHeader(System.Drawing.Printing.PrintPageEventArgs e)
        {
            Font headerFont = new Font("Arial", 14f);

            float lastY = 15;
            float xOffset = 25;

            e.Graphics.DrawImage(Properties.Resources.head,0,0);

            lastY += ((int)(Properties.Resources.head.Height / 3));
            e.Graphics.DrawString("La Familia | Musterstraße 10 | 5020 Musterhausen | Tel.: 01234 / 15616 | Mail: office@la-familia.at", stdFont, Brushes.Black, new PointF( xOffset + 20, lastY));

            SizeF size = e.Graphics.MeasureString("La Familia", stdFont);
            lastY += size.Height;

            e.Graphics.DrawLine(new Pen(Brushes.Black, 2f), new Point((int)e.PageSettings.PrintableArea.X + (int)xOffset, (int)lastY), new Point((int)e.PageSettings.PrintableArea.X + (int)e.PageSettings.PrintableArea.Width - ((int)xOffset * 2), (int)lastY));

            string text = "An\r\n" + this.invoice.Person.Firstname.ToString() + " " + this.invoice.Person.Lastname.ToString() + "\r\n" + this.invoice.Person.Address.Street.ToString() + "\r\n" +
                this.invoice.Person.Address.Postalcode.ToString() + this.invoice.Person.Address.Location.ToString() + "\r\n" + this.invoice.Person.Address.Country.ToString();
            size = e.Graphics.MeasureString(text, stdFont);
            e.Graphics.DrawString(text, stdFont, Brushes.Black, new PointF(xOffset + 10, lastY + 50));

            text = "Kundennummer: " + this.invoice.Person.IdentificationNumber + "\r\nRechnungsnummer: " + this.invoice.InvoiceNumber + "\r\nRechnungsdatum: " + this.invoice.InvoiceDate.Value.ToShortDateString();
            e.Graphics.DrawString(text, stdFont, Brushes.Black, new PointF(e.PageSettings.PrintableArea.X + e.PageSettings.PrintableArea.Width - size.Width - (xOffset * 7), lastY + size.Height - 10));

            lastY += size.Height + 80;
            text = "Rechnung zu Ihrer Bestellung Nr.: " + this.invoice.Order.OrderNr;
            size = e.Graphics.MeasureString(text, headerFont);
            e.Graphics.DrawString(text, new Font(headerFont, FontStyle.Bold), Brushes.Black, new PointF(xOffset + 5, lastY));

            return lastY;
        }

        private float PrintListHeader(float pLastY, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Font headerFont = new Font("Arial", 12f);
            float lastY = pLastY;
            float xOffset = 25;

            lastY += 15;
            e.Graphics.DrawLine(new Pen(Brushes.Black, 2f), new Point((int)e.PageSettings.PrintableArea.X + (int)xOffset, (int)lastY), new Point((int)e.PageSettings.PrintableArea.X + (int)e.PageSettings.PrintableArea.Width - ((int)xOffset * 2), (int)lastY));
            lastY += 5;
            SizeF size = e.Graphics.MeasureString("Anz. Artikelnr. Beschreibung Gr. Preis", headerFont);
            e.Graphics.DrawString("Anz.", headerFont, Brushes.Black, new PointF(xOffset + 15, lastY));
            e.Graphics.DrawString("Artikelnr.", headerFont, Brushes.Black, new PointF(xOffset + 65, lastY));
            e.Graphics.DrawString("Beschreibung", headerFont, Brushes.Black, new PointF(xOffset + 180, lastY));
            e.Graphics.DrawString("Gr.", headerFont, Brushes.Black, new PointF(xOffset + 580, lastY));
            e.Graphics.DrawString("Preis", headerFont, Brushes.Black, new PointF(xOffset + 650, lastY));

            lastY += size.Height + 3;
            e.Graphics.DrawLine(new Pen(Brushes.Black, 1f), new Point((int)e.PageSettings.PrintableArea.X + (int)xOffset, (int)lastY), new Point((int)e.PageSettings.PrintableArea.X + (int)e.PageSettings.PrintableArea.Width - ((int)xOffset * 2), (int)lastY));
            lastY += 2;
            e.Graphics.DrawLine(new Pen(Brushes.Black, 1f), new Point((int)e.PageSettings.PrintableArea.X + (int)xOffset, (int)lastY), new Point((int)e.PageSettings.PrintableArea.X + (int)e.PageSettings.PrintableArea.Width - ((int)xOffset * 2), (int)lastY));
            return lastY;
        }

        private float PrintZeile(float pLastY, Article article, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float lastY = pLastY;
            float xOffset = 20;

            lastY += 5;
            SizeF size = e.Graphics.MeasureString("Artikelanzahl", stdFont);
            e.Graphics.DrawString("1", stdFont, Brushes.Black, new PointF(xOffset + 15, lastY));
            e.Graphics.DrawString(article.ArticleNumber, stdFont, Brushes.Black, new PointF(xOffset + 65, lastY));
            e.Graphics.DrawString(article.Title, stdFont, Brushes.Black, new PointF(xOffset + 180, lastY));
            e.Graphics.DrawString("Size", stdFont, Brushes.Black, new PointF(xOffset + 580, lastY));
            e.Graphics.DrawString(article.Price.ToString(), stdFont, Brushes.Black, new PointF(xOffset + 650, lastY));
            lastY += size.Height;

            return lastY;
        }

        private void PrintFooter(System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(Properties.Resources.footer, 0, 1042);

            int y = Properties.Resources.footer.Height;
            int y2 = (int)e.PageSettings.PrintableArea.Height;
        }

        #endregion

    }
}
