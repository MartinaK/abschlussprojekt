﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Controls
{
	public partial class UserControlArticleVariant : UserControl
	{

		public event EventHandler ArticleVariantDeleted;
		public event EventHandler ArticleVariantCreated;

		private Variant variant = null;
		private Article article = null;

		//****************************************************************************************
		#region constructors

		public UserControlArticleVariant(Article article)
		{
			InitializeComponent();
			this.article = article;
			this.comboBoxAttribute.SelectedIndex = 0;
		}

		public UserControlArticleVariant(Variant variant)
		{
			InitializeComponent();

			this.variant = variant;
			this.comboBoxAttribute.SelectedIndex = 0;


			this.textBoxAttrival.Text = this.variant.AttriVal;
			this.comboBoxAttribute.Text = this.variant.Attribute;
			this.textBoxStack.Text = this.variant.Stack.ToString();

		}

		#endregion

		//*****************************************************************************************
		#region properties

		public Variant Variant
		{
			get
			{
				return this.variant;
			}
		}

		#endregion

		//*****************************************************************************************
		#region public methods

		#endregion

		//*****************************************************************************************
		#region events

		private void pictureBoxDelete_Click(object sender, EventArgs e)
		{
			if (this.variant != null)
			{
				if (MessageBox.Show("Wollen Sie wirklich löschen?", "Frage..", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					//löst event aus
					this.variant.Delete();
					if (this.ArticleVariantDeleted != null)
						this.ArticleVariantDeleted(this, EventArgs.Empty);
				}
			}
		}

		private void textBoxStack_Leave(object sender, EventArgs e)
		{
			if (!String.IsNullOrEmpty(this.textBoxAttrival.Text)
					&& !String.IsNullOrEmpty(this.comboBoxAttribute.Text)
					&& !String.IsNullOrEmpty(this.textBoxStack.Text))
			{
				if (this.variant == null)
				{
					this.variant = new Variant(this.article);

					if (ArticleVariantCreated != null)
						ArticleVariantCreated(this, EventArgs.Empty);
				}
				this.variant.Attribute = this.comboBoxAttribute.Text;
				this.variant.Stack = Convert.ToInt32(this.textBoxStack.Text);
				this.variant.AttriVal = this.textBoxAttrival.Text;
			}

		}
		#endregion
	}
}
