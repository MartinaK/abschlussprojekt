﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Controls
{
    public partial class UserControlShopArticle : UserControl
    {

        private Article article;
        private Variant variant;
        private Person person;
        private Order order;

        //*****************************************************************
        #region constructors

        public UserControlShopArticle(Article article, Person person)
        {
            InitializeComponent();

            this.article = article;
            this.person = person;

            this.labelArtikelnr.Text = this.article.ArticleNumber;
            this.labelTitle.Text = this.article.Title;
            this.labelBrand.Text = this.article.Brand;
            this.labelPrice.Text = "Preis: " + this.article.Price.ToString();
            this.labelUvp.Text = "UVP: " + this.article.Uvp.ToString();
            this.pictureBoxArticleImg.Image = this.article.ArticleImg;

            foreach (Variant v in this.article.Variants)
            {
                this.comboBoxSize.Tag = v;
                this.comboBoxSize.Items.Add(v.AttriVal);
            }

        }

        #endregion

        //***************************************************************************
        #region private methods

        private string GenerateOrderNr()
        {
            int[] id = new int[6];
            Random zahlen = new Random();

            //jede Bestellnr soll mit 3 beginnen
            id[0] = 3;

            for (int i = 1; i < id.Length; i++)
                id[i] = zahlen.Next(1, 10);

            //String.Join setzt die zahlen zu einer Zeichenkette zusammen, Trennzeichen ""
            return String.Join("", new List<int>(id).ConvertAll(i => i.ToString()).ToArray());
        }

        #endregion

        private void comboBoxSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.numericUpDownQuantity.Value = 1;
            string filter = this.comboBoxSize.SelectedItem.ToString();
            foreach (Variant v in this.article.Variants.Where(a => a.AttriVal == filter))
            {
                if (v.Stack >= 1)
                {
                    this.numericUpDownQuantity.Maximum = v.Stack;
                    this.labelStatus.Text = v.Stack + " Verfügbar";
                    this.labelSupplyDate.Text = "Voraussichtliches Lieferdatum: " + DateTime.Now.AddDays(3).ToShortDateString();
                    this.variant = v;
                    return;
                }
                this.numericUpDownQuantity.Enabled = false;
                this.pictureBoxStatus.Image = Properties.Resources.not_available;
                this.labelStatus.Text = "Ausverkauft";
            }
        }

        private void pictureBoxAddToCart_Click(object sender, EventArgs e)
        {
            foreach (Order order in Order.GetList().Where(o => o.PersonId == this.person.PersonId && o.Status == OrderStatus.Einkaufswagen.ToString()))
                this.order = order;

            if (this.order == null)
            {
                this.order = new Order();
                this.order.Status = OrderStatus.Einkaufswagen.ToString();
                this.order.PersonId = this.person.PersonId;
                this.order.TotalCosts = 4.95m;
                this.order.OrderDate = DateTime.Now;
                this.order.LastUpdate = DateTime.Now;
                this.order.DeliveryDate = DateTime.Now.AddDays(3);
                this.order.OrderNr = GenerateOrderNr();

                foreach (Order order in Order.GetList())
                {
                    if (order.OrderNr.Equals(this.order.OrderNr))
                        this.order.OrderNr = GenerateOrderNr();
                }

                this.order.Save();
            }

            this.variant.Stack = this.variant.Stack - (int)this.numericUpDownQuantity.Value;
            this.variant.Save();

            ArticleOrdered articleOrdered = this.order.ArticlesOrdered.SingleOrDefault(ao => ao.ArticleId == article.ArticleId);

            if (articleOrdered == null)
            {
                this.order.ArticleAdd(this.article);
                this.order.RefreshCollections();
                articleOrdered = this.order.ArticlesOrdered.SingleOrDefault(ao => ao.ArticleId == article.ArticleId);
                articleOrdered.Save();
            }
            
            articleOrdered.Quantity += (int)this.numericUpDownQuantity.Value;
            articleOrdered.Variant = this.variant;
            this.order.TotalCosts += articleOrdered.Quantity * articleOrdered.Price;
            this.order.Save();
            this.order.RefreshCollections();
            articleOrdered.Save();

            FormShoppingCart shoppingCartForm = new FormShoppingCart(this.order);
            shoppingCartForm.ShowDialog();
        }

    }
}
