﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;
using LaFamilia.BS2014.Administration;

namespace LaFamilia.BS2014.Controls
{
    public partial class UserControlPersonList : UserControl
    {
        private List<Person> showablePersons;


        public UserControlPersonList(List<Person> showablePersons)
        {
            InitializeComponent();

            this.showablePersons = showablePersons;

            this.comboBoxCountry.DataSource = Enum.GetValues(typeof(Country));
            this.comboBoxFilterPersonType.DataSource = Enum.GetValues(typeof(PersonType));

            foreach (Person person in showablePersons)
            {
                this.comboBoxLastName.Items.Add(person.Lastname);
            }

            LoadData(showablePersons);
        }


        //********************************************************************************************
        #region private methods

        private void LoadData(List<Person> showablePersons)
        {
            ListViewItem item = null;
            this.listViewCustomer.Items.Clear();

            foreach (Person person in showablePersons)
            {
                item = new ListViewItem();
                item.Tag = person;
                item.Text = person.IdentificationNumber.ToString();
                item.SubItems.Add(person.Firstname + " " + person.Lastname);
                item.SubItems.Add(person.Address.Postalcode + " " + person.Address.Location);
                item.SubItems.Add(person.Address.Country);
                item.SubItems.Add(person.Phone);
                this.listViewCustomer.Items.Add(item);
            }
        }

        #endregion

        private void listViewOrder_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewItem item = this.listViewCustomer.GetItemAt(e.X, e.Y);

            Person person = (Person)item.Tag;

            FormPerson personForm = new FormPerson(true, person);
            personForm.Show();
        }


        #region comboBox Filter
        private void comboBoxLastName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string filterName = this.comboBoxLastName.SelectedItem.ToString();
            this.showablePersons = this.showablePersons.Where(p => p.Lastname.Contains(filterName)).ToList<Person>();
            LoadData(this.showablePersons);
            this.showablePersons = Person.GetList();
        }


        private void comboBoxCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            string filterName = this.comboBoxCountry.SelectedItem.ToString();
            this.showablePersons = Person.GetList();

            if (filterName != "Alle")
                this.showablePersons = this.showablePersons.Where(p => p.Address.Country.Contains(filterName)).ToList<Person>();

            LoadData(this.showablePersons);
        }
        #endregion

        private void buttonNewCustomer_Click(object sender, EventArgs e)
        {
            FormPerson personForm = new FormPerson(true);
            personForm.Show();
        }

        private void linkLabelResetFilter_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.comboBoxLastName.Text = "Nachname";
            this.comboBoxCountry.Text = "Land";
            this.comboBoxFilterPersonType.Text = "Alle";
            LoadData(Person.GetList());
        }

        private void comboBoxFilterPersonType_SelectedIndexChanged(object sender, EventArgs e)
        {
            PersonType filter = (PersonType)this.comboBoxFilterPersonType.SelectedItem;
            if (filter == PersonType.Alle)
                this.showablePersons = Person.GetList();
            if (filter == PersonType.Kunden)
                this.showablePersons = Person.GetList().Where(p => (p.IsCustomer == true)).ToList();
            if (filter == PersonType.Mitarbeiter)
                this.showablePersons = Person.GetList().Where(p => (p.IsEmployee == true)).ToList();
            LoadData(this.showablePersons);
        }

    }
}
