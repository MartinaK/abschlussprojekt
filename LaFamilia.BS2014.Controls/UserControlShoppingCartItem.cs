﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Controls
{
    public partial class UserControlShoppingCartItem : UserControl
    {
        private ArticleOrdered articleOrdered;
        private Variant variant;


        //*********************************************************************
        #region constructors

        public UserControlShoppingCartItem()
        {
            InitializeComponent();
        }

        public UserControlShoppingCartItem(ArticleOrdered articleOrdered)
        {
            InitializeComponent();

            this.articleOrdered = articleOrdered;
            this.variant = this.articleOrdered.Variant;

            this.comboBoxSize.DataSource = articleOrdered.Variants;
            this.comboBoxSize.Text = this.variant.AttriVal;
            this.pictureBoxArticleImg.Image = this.articleOrdered.ArticleImg;
            this.labelArticleTitle.Text = this.articleOrdered.Title;
            this.labelPrice.Text = this.articleOrdered.Price * this.articleOrdered.Quantity + " €";
            this.numericUpDownQuantity.Value = this.articleOrdered.Quantity;
        }


        #endregion


        //*********************************************************************
        #region private methods

        private int RefreshStack()
        {
            int diff = (int)this.numericUpDownQuantity.Value - this.articleOrdered.Quantity;
            return variant.Stack -= diff;
        }

        #endregion

        private void comboBoxSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.variant != null)
            {
                //gibt dem voher ausgewählten Lagerstand wieder die Anzahl dazu
                this.variant.Stack += (int)this.numericUpDownQuantity.Value;
                this.variant.Save();

                this.variant = this.articleOrdered.Variants.SingleOrDefault(ao => ao.AttriVal == this.comboBoxSize.SelectedItem.ToString());

                //ändert den Lagerstand der neuen Größe
                this.variant.Stack = RefreshStack();
                this.variant.Save();
                this.articleOrdered.Variant = this.variant;
                this.articleOrdered.Save();
            }
        }


        private void numericUpDownQuantity_ValueChanged(object sender, EventArgs e)
        {
            if (this.variant != null)
            {
                this.articleOrdered.Quantity = (int)this.numericUpDownQuantity.Value;
                this.labelPrice.Text = this.articleOrdered.Price * this.articleOrdered.Quantity + " €";
                RefreshStack();
                articleOrdered.Save();
            }
        }
    }
}
