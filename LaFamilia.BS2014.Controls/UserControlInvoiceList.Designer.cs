﻿namespace LaFamilia.BS2014.Controls
{
	partial class UserControlInvoiceList
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.panelFilter = new System.Windows.Forms.Panel();
            this.pictureBoxPrint = new System.Windows.Forms.PictureBox();
            this.linkLabelResetFilter = new System.Windows.Forms.LinkLabel();
            this.comboBoxLastName = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxStatus = new System.Windows.Forms.ComboBox();
            this.listViewInvoice = new System.Windows.Forms.ListView();
            this.columnHeaderInvoiceNr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderInvoiceDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderCustomerID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderCustomerName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderLastUpdate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderEmployee = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.printDocumentInvoice = new System.Drawing.Printing.PrintDocument();
            this.panelFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // panelFilter
            // 
            this.panelFilter.BackColor = System.Drawing.Color.Silver;
            this.panelFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelFilter.Controls.Add(this.pictureBoxPrint);
            this.panelFilter.Controls.Add(this.linkLabelResetFilter);
            this.panelFilter.Controls.Add(this.comboBoxLastName);
            this.panelFilter.Controls.Add(this.label1);
            this.panelFilter.Controls.Add(this.comboBoxStatus);
            this.panelFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelFilter.Location = new System.Drawing.Point(7, 18);
            this.panelFilter.Name = "panelFilter";
            this.panelFilter.Size = new System.Drawing.Size(722, 32);
            this.panelFilter.TabIndex = 5;
            // 
            // pictureBoxPrint
            // 
            this.pictureBoxPrint.BackColor = System.Drawing.Color.White;
            this.pictureBoxPrint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxPrint.Image = global::LaFamilia.BS2014.Controls.Properties.Resources.Drucker;
            this.pictureBoxPrint.Location = new System.Drawing.Point(690, 4);
            this.pictureBoxPrint.Name = "pictureBoxPrint";
            this.pictureBoxPrint.Size = new System.Drawing.Size(25, 25);
            this.pictureBoxPrint.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxPrint.TabIndex = 37;
            this.pictureBoxPrint.TabStop = false;
            this.pictureBoxPrint.Click += new System.EventHandler(this.pictureBoxPrint_Click);
            // 
            // linkLabelResetFilter
            // 
            this.linkLabelResetFilter.AutoSize = true;
            this.linkLabelResetFilter.Location = new System.Drawing.Point(369, 8);
            this.linkLabelResetFilter.Name = "linkLabelResetFilter";
            this.linkLabelResetFilter.Size = new System.Drawing.Size(109, 15);
            this.linkLabelResetFilter.TabIndex = 4;
            this.linkLabelResetFilter.TabStop = true;
            this.linkLabelResetFilter.Text = "Filter zurücksetzen";
            this.linkLabelResetFilter.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelResetFilter_LinkClicked);
            // 
            // comboBoxLastName
            // 
            this.comboBoxLastName.FormattingEnabled = true;
            this.comboBoxLastName.Location = new System.Drawing.Point(238, 4);
            this.comboBoxLastName.Name = "comboBoxLastName";
            this.comboBoxLastName.Size = new System.Drawing.Size(125, 23);
            this.comboBoxLastName.TabIndex = 3;
            this.comboBoxLastName.Text = "Nachname";
            this.comboBoxLastName.SelectedIndexChanged += new System.EventHandler(this.comboBoxLastName_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Setze Filter nach:";
            // 
            // comboBoxStatus
            // 
            this.comboBoxStatus.FormattingEnabled = true;
            this.comboBoxStatus.Location = new System.Drawing.Point(126, 4);
            this.comboBoxStatus.Name = "comboBoxStatus";
            this.comboBoxStatus.Size = new System.Drawing.Size(106, 23);
            this.comboBoxStatus.TabIndex = 1;
            this.comboBoxStatus.Text = "Status";
            this.comboBoxStatus.SelectedIndexChanged += new System.EventHandler(this.comboBoxStatus_SelectedIndexChanged);
            // 
            // listViewInvoice
            // 
            this.listViewInvoice.BackColor = System.Drawing.Color.WhiteSmoke;
            this.listViewInvoice.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderInvoiceNr,
            this.columnHeaderInvoiceDate,
            this.columnHeaderCustomerID,
            this.columnHeaderCustomerName,
            this.columnHeaderStatus,
            this.columnHeaderLastUpdate,
            this.columnHeaderEmployee});
            this.listViewInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewInvoice.FullRowSelect = true;
            this.listViewInvoice.GridLines = true;
            this.listViewInvoice.Location = new System.Drawing.Point(6, 49);
            this.listViewInvoice.MultiSelect = false;
            this.listViewInvoice.Name = "listViewInvoice";
            this.listViewInvoice.Size = new System.Drawing.Size(723, 328);
            this.listViewInvoice.TabIndex = 4;
            this.listViewInvoice.UseCompatibleStateImageBehavior = false;
            this.listViewInvoice.View = System.Windows.Forms.View.Details;
            this.listViewInvoice.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewInvoice_MouseDoubleClick);
            // 
            // columnHeaderInvoiceNr
            // 
            this.columnHeaderInvoiceNr.Text = "Rechnungsnr.";
            this.columnHeaderInvoiceNr.Width = 100;
            // 
            // columnHeaderInvoiceDate
            // 
            this.columnHeaderInvoiceDate.Text = "Rechnungsdatum";
            this.columnHeaderInvoiceDate.Width = 124;
            // 
            // columnHeaderCustomerID
            // 
            this.columnHeaderCustomerID.Text = "Kundennr.";
            this.columnHeaderCustomerID.Width = 100;
            // 
            // columnHeaderCustomerName
            // 
            this.columnHeaderCustomerName.Text = "Kundenname";
            this.columnHeaderCustomerName.Width = 102;
            // 
            // columnHeaderStatus
            // 
            this.columnHeaderStatus.Text = "Status";
            this.columnHeaderStatus.Width = 100;
            // 
            // columnHeaderLastUpdate
            // 
            this.columnHeaderLastUpdate.Text = "Letzte Änderung";
            this.columnHeaderLastUpdate.Width = 105;
            // 
            // columnHeaderEmployee
            // 
            this.columnHeaderEmployee.Text = "Mitarbeiter";
            this.columnHeaderEmployee.Width = 75;
            // 
            // printDocumentInvoice
            // 
            this.printDocumentInvoice.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocumentInvoice_PrintPage);
            // 
            // UserControlInvoiceList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.panelFilter);
            this.Controls.Add(this.listViewInvoice);
            this.Name = "UserControlInvoiceList";
            this.Size = new System.Drawing.Size(735, 384);
            this.panelFilter.ResumeLayout(false);
            this.panelFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPrint)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panelFilter;
		private System.Windows.Forms.ComboBox comboBoxLastName;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox comboBoxStatus;
		private System.Windows.Forms.ListView listViewInvoice;
		private System.Windows.Forms.ColumnHeader columnHeaderInvoiceNr;
		private System.Windows.Forms.ColumnHeader columnHeaderInvoiceDate;
		private System.Windows.Forms.ColumnHeader columnHeaderCustomerID;
		private System.Windows.Forms.ColumnHeader columnHeaderCustomerName;
		private System.Windows.Forms.ColumnHeader columnHeaderStatus;
		private System.Windows.Forms.ColumnHeader columnHeaderLastUpdate;
        private System.Windows.Forms.ColumnHeader columnHeaderEmployee;
        private System.Windows.Forms.LinkLabel linkLabelResetFilter;
        private System.Windows.Forms.PictureBox pictureBoxPrint;
        private System.Drawing.Printing.PrintDocument printDocumentInvoice;
	}
}
