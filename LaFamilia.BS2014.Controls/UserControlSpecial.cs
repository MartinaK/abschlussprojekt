﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;
using LaFamilia.BS2014.Administration;

namespace LaFamilia.BS2014.Controls
{
    public partial class UserControlSpecial : UserControl
    {
        private Person person;
        bool clickable;
        bool admin;

        public event EventHandler ShowSpecialShop;

        //**************************************************************************************
        #region constructors

        public UserControlSpecial(Special special, bool clickable, bool admin, Person person)
        {
            InitializeComponent();

            this.special = special;
            this.person = person;
            this.clickable = clickable;
            this.admin = admin;

            this.pictureBoxSpecial.Image = this.special.PictureImg;
            this.label_name.Text = this.special.Title;
            this.label_description.Text = this.special.Description;
            this.label_dateFrom.Text = this.special.DateFrom.Value.ToShortDateString();
            this.label_dateTo.Text = " - " +  this.special.DateTo.Value.ToShortDateString();
        }

        #endregion

        //**************************************************************************************
        #region properties

        private Special special;

        public Special Special
        {
            get { return special; }
            set { special = value; }
        }

        #endregion

        private void UserControlSpecial_Click(object sender, EventArgs e)
        {
            if (admin && clickable)
            {
                FormSpecialDetails specialDetailsForm = new FormSpecialDetails(this.special);
                specialDetailsForm.Show();
                return;
            }
            if (clickable)
            {
                if (ShowSpecialShop != null)
                    ShowSpecialShop(this, EventArgs.Empty);
            }
        }

    }
}
