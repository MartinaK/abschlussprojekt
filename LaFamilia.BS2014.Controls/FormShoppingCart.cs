﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Controls
{
    public partial class FormShoppingCart : Form
    {

        private Order order;
        double shippingCosts = 4.95;

        //*****************************************************************************
        #region constructors

        public FormShoppingCart(Order order)
        {
            InitializeComponent();

            this.order = order;

            UserControlHeader ucHeader = new UserControlHeader();
            this.panelHeader.Controls.Add(ucHeader);

            UserControlFooter ucFooter = new UserControlFooter();
            this.panelFooter.Controls.Add(ucFooter);

            this.order.RefreshCollections();

            this.labelSum.Text = this.order.TotalCosts + " €";

            foreach (ArticleOrdered article in this.order.ArticlesOrdered)
            {
                UserControlShoppingCartItem ucShoppingCartItem = new UserControlShoppingCartItem(article);
                this.flowLayoutPanelArticles.Controls.Add(ucShoppingCartItem);
            }
        }

        #endregion

        private void buttonDeleteCart_Click(object sender, EventArgs e)
        {
            if (this.order != null)
            {
                if (MessageBox.Show("Wollen Sie wirklich löschen?", "Frage..", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    foreach (UserControlShoppingCartItem ucItem in this.flowLayoutPanelArticles.Controls)
                    {
                        this.flowLayoutPanelArticles.Controls.Clear();
                    }
                    foreach (ArticleOrdered ao in this.order.ArticlesOrdered)
                    {
                        //fügt dem Lager wieder die Anzahl hinzu
                        ao.Variant.Stack = ao.Variant.Stack + ao.Quantity;
                        ao.Variant.Save();
                        ao.Delete();
                    }
                    this.order.Delete();
                    this.labelShippingCosts.Text = String.Empty;
                    this.labelTotalCosts.Text = String.Empty;
                }
            }
        }

        private void buttonBackToShop_Click(object sender, EventArgs e)
        {
            this.order.RefreshCollections();
            this.Close();
        }

        private void buttonOrder_Click(object sender, EventArgs e)
        {
            FormOrderConfirm orderConfirmForm = new FormOrderConfirm(this.order);

            if (orderConfirmForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.Focus();
                this.order.RefreshCollections();
            }
        }

    }
}
