﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Controls;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Administration
{
    public partial class FormSpecialDetails : Form
    {
        private Special special;
        private List<Article> checkedArticles;

        //************************************************************************
        #region constructors

        public FormSpecialDetails()
        {
            InitializeComponent();

            UserControlHeader ucHeader = new UserControlHeader();
            this.panelHeader.Controls.Add(ucHeader);

            UserControlFooter ucFooter = new UserControlFooter();
            this.panelFooter.Controls.Add(ucFooter);

            foreach (Article article in Article.GetList().Where(a => a.Type == "A"))
            {
                if (!this.comboBoxAllBrands.Items.Contains(article.Brand))
                    this.comboBoxAllBrands.Items.Add(article.Brand);
            }

        }

        public FormSpecialDetails(Special special)
        {
            InitializeComponent();

            this.special = special;

            UserControlHeader ucHeader = new UserControlHeader();
            this.panelHeader.Controls.Add(ucHeader);

            UserControlFooter ucFooter = new UserControlFooter();
            this.panelFooter.Controls.Add(ucFooter);

            if (this.special.PictureImg != null)
                this.pictureBoxSpecialImg.Image = this.special.PictureImg;
            this.textBoxTitle.Text = this.special.Title;
            this.richTextBoxDescription.Text = this.special.Description;
            this.dateTimePickerDateFrom.Value = this.special.DateFrom.Value;
            this.dateTimePickerDateTo.Value = this.special.DateTo.Value;

            this.comboBoxAllBrands.Enabled = false;

            foreach (Article article in Article.GetList().Where(a => a.SpecialId == this.special.SpecialId))
            {
                if (this.special.Articles == null)
                    this.special.Articles = new List<Article>();

                this.special.Articles.Add(article);
            }

            if (this.checkedArticles == null)
                this.checkedArticles = new List<Article>();
            this.checkedArticles = this.special.Articles;


            LoadData(this.special.Articles);

        }

        #endregion


        //************************************************************************
        #region events

        private void pictureBoxSpecialImg_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.pictureBoxSpecialImg.Image = Image.FromFile(dialog.FileName);
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (this.special == null)
                this.special = new Special();

            this.special.PictureImg = null;
            if (this.pictureBoxSpecialImg != null)
                this.special.PictureImg = this.pictureBoxSpecialImg.Image;

            this.special.Title = this.textBoxTitle.Text;
            this.special.Description = this.richTextBoxDescription.Text;

            this.special.DateFrom = null;
            if (this.dateTimePickerDateFrom.Value != null)
                this.special.DateFrom = this.dateTimePickerDateFrom.Value;

            this.special.DateTo = null;
            if (this.dateTimePickerDateTo.Value != null)
                this.special.DateTo = this.dateTimePickerDateTo.Value;

            this.special.Save();

            foreach (Article article in this.checkedArticles)
            {
                article.SpecialId = this.special.SpecialId;
                article.Save();
            }

            this.Hide();
        }

        #endregion

        //************************************************************************
        #region private methods

        private void LoadData(List<Article> articles)
        {
            ListViewItem item = null;
            this.listViewArticles.Items.Clear();

            if (articles != null)
            {
                foreach (Article article in articles)
                {
                    item = new ListViewItem();
                    item.Tag = article;
                    item.Text = article.ArticleNumber.ToString();
                    item.SubItems.Add(article.Title);
                    item.SubItems.Add(article.Brand);
                    item.Checked = true;
                    this.listViewArticles.Items.Add(item);
                }
            }
        }

        #endregion

        private void listViewArticles_ItemChecked(object sender, ItemCheckedEventArgs e)
        {


            Article article = (Article)e.Item.Tag;

            if (e.Item.Checked)
            {
                e.Item.BackColor = Color.LightGray;

                if (this.checkedArticles == null)
                    this.checkedArticles = new List<Article>();

                this.checkedArticles.Add(article);
                return;
            }

            //if (!e.Item.Checked)
            //{
            //    e.Item.BackColor = Color.WhiteSmoke;
            //    e.Item.ForeColor = Color.Black;

            //    Article articleFound = null;

            //    if (this.checkedArticles != null)
            //    {
            //        foreach (Article art in this.checkedArticles)
            //        {
            //            //articleFound = this.checkedArticles.SingleOrDefault(a => a.ArticleId.Value == art.ArticleId.Value);
            //            if (this.checkedArticles.Contains(art))
            //                articleFound = art;
            //        }

            //        if (articleFound == null)
            //            return;

            //        this.checkedArticles.Remove(articleFound);
            //        articleFound.SpecialId = null;
            //        articleFound.Save();

            //    }
            //}
        }

        private void comboBoxAllBrands_SelectedIndexChanged(object sender, EventArgs e)
        {
            string filterName = this.comboBoxAllBrands.SelectedItem.ToString();
            List<Article> showableArticles = Article.GetList().Where(a => a.Brand.Equals(filterName)).ToList<Article>();
            LoadData(showableArticles);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
