﻿namespace LaFamilia.BS2014.Controls
{
    partial class UserControlSpecialMain
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelHeadActualSpecials = new System.Windows.Forms.Panel();
            this.pictureBoxActual = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panelHeadNextSpecials = new System.Windows.Forms.Panel();
            this.pictureBoxNext = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanelActualSpecials = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanelNextSpecials = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonNewSpecial = new System.Windows.Forms.Button();
            this.panelHeadActualSpecials.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxActual)).BeginInit();
            this.panelHeadNextSpecials.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNext)).BeginInit();
            this.SuspendLayout();
            // 
            // panelHeadActualSpecials
            // 
            this.panelHeadActualSpecials.BackColor = System.Drawing.Color.Silver;
            this.panelHeadActualSpecials.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelHeadActualSpecials.Controls.Add(this.pictureBoxActual);
            this.panelHeadActualSpecials.Controls.Add(this.label1);
            this.panelHeadActualSpecials.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelHeadActualSpecials.Location = new System.Drawing.Point(9, 35);
            this.panelHeadActualSpecials.Name = "panelHeadActualSpecials";
            this.panelHeadActualSpecials.Size = new System.Drawing.Size(722, 32);
            this.panelHeadActualSpecials.TabIndex = 4;
            // 
            // pictureBoxActual
            // 
            this.pictureBoxActual.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxActual.Location = new System.Drawing.Point(697, 7);
            this.pictureBoxActual.Name = "pictureBoxActual";
            this.pictureBoxActual.Size = new System.Drawing.Size(20, 17);
            this.pictureBoxActual.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxActual.TabIndex = 1;
            this.pictureBoxActual.TabStop = false;
            this.pictureBoxActual.Click += new System.EventHandler(this.pictureBoxActual_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Aktuelle Aktionen";
            // 
            // panelHeadNextSpecials
            // 
            this.panelHeadNextSpecials.BackColor = System.Drawing.Color.Silver;
            this.panelHeadNextSpecials.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelHeadNextSpecials.Controls.Add(this.pictureBoxNext);
            this.panelHeadNextSpecials.Controls.Add(this.label2);
            this.panelHeadNextSpecials.Cursor = System.Windows.Forms.Cursors.Default;
            this.panelHeadNextSpecials.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelHeadNextSpecials.Location = new System.Drawing.Point(9, 355);
            this.panelHeadNextSpecials.Name = "panelHeadNextSpecials";
            this.panelHeadNextSpecials.Size = new System.Drawing.Size(722, 32);
            this.panelHeadNextSpecials.TabIndex = 5;
            // 
            // pictureBoxNext
            // 
            this.pictureBoxNext.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxNext.Location = new System.Drawing.Point(697, 8);
            this.pictureBoxNext.Name = "pictureBoxNext";
            this.pictureBoxNext.Size = new System.Drawing.Size(20, 17);
            this.pictureBoxNext.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxNext.TabIndex = 2;
            this.pictureBoxNext.TabStop = false;
            this.pictureBoxNext.Click += new System.EventHandler(this.pictureBoxNext_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(174, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Demnächst startende Aktionen";
            // 
            // flowLayoutPanelActualSpecials
            // 
            this.flowLayoutPanelActualSpecials.AutoScroll = true;
            this.flowLayoutPanelActualSpecials.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanelActualSpecials.Location = new System.Drawing.Point(9, 66);
            this.flowLayoutPanelActualSpecials.Name = "flowLayoutPanelActualSpecials";
            this.flowLayoutPanelActualSpecials.Size = new System.Drawing.Size(722, 278);
            this.flowLayoutPanelActualSpecials.TabIndex = 7;
            // 
            // flowLayoutPanelNextSpecials
            // 
            this.flowLayoutPanelNextSpecials.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanelNextSpecials.Location = new System.Drawing.Point(9, 386);
            this.flowLayoutPanelNextSpecials.Name = "flowLayoutPanelNextSpecials";
            this.flowLayoutPanelNextSpecials.Size = new System.Drawing.Size(722, 12);
            this.flowLayoutPanelNextSpecials.TabIndex = 8;
            // 
            // buttonNewSpecial
            // 
            this.buttonNewSpecial.BackColor = System.Drawing.Color.MediumVioletRed;
            this.buttonNewSpecial.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonNewSpecial.ForeColor = System.Drawing.Color.White;
            this.buttonNewSpecial.Location = new System.Drawing.Point(626, 6);
            this.buttonNewSpecial.Name = "buttonNewSpecial";
            this.buttonNewSpecial.Size = new System.Drawing.Size(105, 23);
            this.buttonNewSpecial.TabIndex = 33;
            this.buttonNewSpecial.Text = "Neue Aktion";
            this.buttonNewSpecial.UseVisualStyleBackColor = false;
            this.buttonNewSpecial.Click += new System.EventHandler(this.buttonNewSpecial_Click);
            // 
            // UserControlSpecialMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonNewSpecial);
            this.Controls.Add(this.flowLayoutPanelNextSpecials);
            this.Controls.Add(this.flowLayoutPanelActualSpecials);
            this.Controls.Add(this.panelHeadNextSpecials);
            this.Controls.Add(this.panelHeadActualSpecials);
            this.Name = "UserControlSpecialMain";
            this.Size = new System.Drawing.Size(758, 456);
            this.panelHeadActualSpecials.ResumeLayout(false);
            this.panelHeadActualSpecials.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxActual)).EndInit();
            this.panelHeadNextSpecials.ResumeLayout(false);
            this.panelHeadNextSpecials.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNext)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelHeadActualSpecials;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelHeadNextSpecials;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBoxActual;
        private System.Windows.Forms.PictureBox pictureBoxNext;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelActualSpecials;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelNextSpecials;
        private System.Windows.Forms.Button buttonNewSpecial;
    }
}
