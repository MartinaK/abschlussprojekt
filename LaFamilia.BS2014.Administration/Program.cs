﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;


namespace LaFamilia.BS2014.Administration
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormLogin());

        }
    }
}
