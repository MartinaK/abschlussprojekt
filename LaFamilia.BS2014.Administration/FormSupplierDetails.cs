﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Controls;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Administration
{
    public partial class FormSupplierDetails : Form
    {
        private Supplier supplier;

        //********************************************************************
        #region constructors

        public FormSupplierDetails()
        {
            InitializeComponent();

            UserControlHeader ucHeader = new UserControlHeader();
            this.panelHead.Controls.Add(ucHeader);

            UserControlFooter ucFooter = new UserControlFooter();
            this.panelFooter.Controls.Add(ucFooter);
        }

        public FormSupplierDetails(Supplier supplier)
        {
            InitializeComponent();

            this.supplier = supplier;

            UserControlHeader ucHeader = new UserControlHeader();
            this.panelHead.Controls.Add(ucHeader);

            UserControlFooter ucFooter = new UserControlFooter();
            this.panelFooter.Controls.Add(ucFooter);

            this.textBoxCompany.Text = this.supplier.Company;
            this.textBoxContact.Text = this.supplier.Contact;
            this.textBoxMail.Text = this.supplier.Mail;
            this.textBoxPhone.Text = this.supplier.Phone;
            this.textBoxCountry.Text = this.supplier.Address.Country;
            this.textBoxPlace.Text = this.supplier.Address.Location;
            this.textBoxPlz.Text = this.supplier.Address.Postalcode;
            this.textBoxStreet.Text = this.supplier.Address.Street;
            this.textBoxSupplyTime.Text = this.supplier.SupplyTime.ToString() + " Tage";

            if (this.supplier.Address != null)
                this.address = this.supplier.Address;
        }
        #endregion

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (this.supplier == null)
                this.supplier = new Supplier();

            this.supplier.Company = this.textBoxCompany.Text;
            this.supplier.Contact = this.textBoxContact.Text;
            this.supplier.Mail = this.textBoxMail.Text;
            this.supplier.Phone = this.textBoxPhone.Text;

            if (this.address == null)
                this.address = new Address();

            this.address.SupplierId = this.supplier.SupplierId;
            this.address.Street = this.textBoxStreet.Text;
            this.address.Postalcode = this.textBoxPlz.Text;
            this.address.Location = this.textBoxPlace.Text;
            this.address.Country = this.textBoxCountry.Text;

            this.address.Save();
            this.supplier.Address = this.address;
           
            this.supplier.Save();
            this.Hide();
						this.DialogResult = DialogResult.OK;
        }

        //********************************************************************
        #region constructors

        private Address address;

        public Address Address
        {
            get { return address; }
            set { address = value; }
        }

        #endregion

				private void buttonCancel_Click(object sender, EventArgs e)
				{
					this.Close();
					this.DialogResult = DialogResult.Cancel;
				}

    }
}
