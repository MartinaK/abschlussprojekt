﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Administration
{
    public partial class FormLogin : Form
    {

        private Person person = null;

        //******************************************************************
        #region constructor

        public FormLogin()
        {
            InitializeComponent();
        }

        #endregion

        //******************************************************************
        #region private methods

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            List<Person> persons = Person.GetList();

            foreach (Person p in persons.Where(p => p.IsEmployee == true))
            {
                if (VerifyMd5Hash(GetMd5Hash(this.textBoxPassword.Text), p.Password) && this.textBoxLoginName.Text == p.Username)
                    this.person = p;
            }

            if (this.person != null)
            {
                FormMainAdmin adminForm = new FormMainAdmin(this.person);
                adminForm.Show();
                this.Hide();
                return;
            }

            this.labelNotification.Text = "Passwort oder Loginname ist falsch!";
            this.textBoxPassword.Clear();
            this.textBoxPassword.Focus();

        }

        #endregion

        //******************************************************************
        #region properties

        public Person Person { get; set; }

        #endregion

        //******************************************************************
        #region public methods


        ///<summary>
        ///Wandelt einen Text in einen Hash Wert um und gibt diesen zurück
        ///</summary>
        /// <param name="value">eingebener text</param>
        /// <returns>string</returns>
        private string GetMd5Hash(string value)
        {
            //erstellt eine Instanz vom md5 Algorithmus
            var md5 = System.Security.Cryptography.MD5.Create();

            //konvertiert den string in ein byte Array
            byte[] data = Encoding.Default.GetBytes(value);
            //erzeugt den Hash
            byte[] md5HashBytes = md5.ComputeHash(data);

            //erzeugt einen StringBuilder um die bytes zu speichern und erzeugt daraus einen String
            StringBuilder builder = new StringBuilder(md5HashBytes.Length * 2);

            //formatiert ein jedes einzelne Byte als hexadezimal string
            foreach (var md5Byte in md5HashBytes)
                builder.Append(md5Byte.ToString("X2"));
            return builder.ToString();
        }

        ///<summary>
        ///Vergleicht einen Hash mit einem Input
        ///</summary>
        /// <param name="input">eingebener string</param>
        /// <param name="hash">zu vergleichender hashwert</param>
        /// <returns>bool</returns>
        private bool VerifyMd5Hash(string input, string hash)
        {
            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (comparer.Compare(input, hash) != 0)
            {
                return false;
            }

            return true;

        }

        private void textBoxPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                List<Person> persons = Person.GetList();

                foreach (Person p in persons.Where(p => p.IsEmployee == true))
                {
                    if (VerifyMd5Hash(GetMd5Hash(this.textBoxPassword.Text), p.Password) && this.textBoxLoginName.Text == p.Username)
                        this.person = p;
                }

                if (this.person != null)
                {
                    FormMainAdmin adminForm = new FormMainAdmin(this.person);
                    adminForm.Show();
                    this.Hide();
                    return;
                }

                this.labelNotification.Text = "Passwort oder Loginname ist falsch!";
                this.textBoxPassword.Clear();
                this.textBoxPassword.Focus();
            }

            if (this.textBoxPassword.Text.Length >= 1)
                this.labelNotification.Text = "";
        }



        //public static string Md5HashToText(byte[] hash)
        //{
        //    return System.BitConverter.ToString(hash);
        //}

        #endregion
    }
}
