﻿namespace LaFamilia.BS2014.Administration
{
    partial class FormMainAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMainAdmin));
            this.panel8 = new System.Windows.Forms.Panel();
            this.pictureBoxDelete = new System.Windows.Forms.PictureBox();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.pictureBoxSearch = new System.Windows.Forms.PictureBox();
            this.buttonOrder = new System.Windows.Forms.Button();
            this.buttonCustomer = new System.Windows.Forms.Button();
            this.buttonSpecial = new System.Windows.Forms.Button();
            this.buttonInvoice = new System.Windows.Forms.Button();
            this.buttonOutlet = new System.Windows.Forms.Button();
            this.buttonStack = new System.Windows.Forms.Button();
            this.panelMainContainer = new System.Windows.Forms.Panel();
            this.pictureBoxZoomIn = new System.Windows.Forms.PictureBox();
            this.pictureBoxZoomOut = new System.Windows.Forms.PictureBox();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.labelName = new System.Windows.Forms.Label();
            this.labelWelcome = new System.Windows.Forms.Label();
            this.panelFooter = new System.Windows.Forms.Panel();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxZoomIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxZoomOut)).BeginInit();
            this.panelHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Silver;
            this.panel8.Controls.Add(this.pictureBoxDelete);
            this.panel8.Controls.Add(this.textBoxSearch);
            this.panel8.Controls.Add(this.pictureBoxSearch);
            this.panel8.ForeColor = System.Drawing.Color.Black;
            this.panel8.Location = new System.Drawing.Point(297, 95);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(299, 42);
            this.panel8.TabIndex = 17;
            // 
            // pictureBoxDelete
            // 
            this.pictureBoxDelete.BackColor = System.Drawing.Color.White;
            this.pictureBoxDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxDelete.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxDelete.Image")));
            this.pictureBoxDelete.Location = new System.Drawing.Point(237, 12);
            this.pictureBoxDelete.Name = "pictureBoxDelete";
            this.pictureBoxDelete.Size = new System.Drawing.Size(15, 18);
            this.pictureBoxDelete.TabIndex = 30;
            this.pictureBoxDelete.TabStop = false;
            this.pictureBoxDelete.Click += new System.EventHandler(this.pictureBoxDelete_Click);
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Location = new System.Drawing.Point(10, 8);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(247, 21);
            this.textBoxSearch.TabIndex = 13;
            this.textBoxSearch.TextChanged += new System.EventHandler(this.textBoxSearch_TextChanged);
            this.textBoxSearch.Enter += new System.EventHandler(this.textBoxSearch_Enter);
            // 
            // pictureBoxSearch
            // 
            this.pictureBoxSearch.BackgroundImage = global::LaFamilia.BS2014.Administration.Properties.Resources.SearchIcon;
            this.pictureBoxSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxSearch.Location = new System.Drawing.Point(258, 8);
            this.pictureBoxSearch.Name = "pictureBoxSearch";
            this.pictureBoxSearch.Size = new System.Drawing.Size(29, 23);
            this.pictureBoxSearch.TabIndex = 14;
            this.pictureBoxSearch.TabStop = false;
            this.pictureBoxSearch.Click += new System.EventHandler(this.pictureBoxSearch_Click);
            // 
            // buttonOrder
            // 
            this.buttonOrder.BackColor = System.Drawing.Color.MediumAquamarine;
            this.buttonOrder.FlatAppearance.BorderColor = System.Drawing.Color.Magenta;
            this.buttonOrder.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOrder.ForeColor = System.Drawing.Color.White;
            this.buttonOrder.Location = new System.Drawing.Point(45, 158);
            this.buttonOrder.Name = "buttonOrder";
            this.buttonOrder.Size = new System.Drawing.Size(128, 31);
            this.buttonOrder.TabIndex = 18;
            this.buttonOrder.Text = "Bestellungen";
            this.buttonOrder.UseVisualStyleBackColor = false;
            this.buttonOrder.Click += new System.EventHandler(this.buttonOrder_Click);
            // 
            // buttonCustomer
            // 
            this.buttonCustomer.BackColor = System.Drawing.Color.MediumAquamarine;
            this.buttonCustomer.FlatAppearance.BorderColor = System.Drawing.Color.Magenta;
            this.buttonCustomer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCustomer.ForeColor = System.Drawing.Color.White;
            this.buttonCustomer.Location = new System.Drawing.Point(181, 158);
            this.buttonCustomer.Name = "buttonCustomer";
            this.buttonCustomer.Size = new System.Drawing.Size(128, 31);
            this.buttonCustomer.TabIndex = 19;
            this.buttonCustomer.Text = "Personen";
            this.buttonCustomer.UseVisualStyleBackColor = false;
            this.buttonCustomer.Click += new System.EventHandler(this.buttonCustomer_Click);
            // 
            // buttonSpecial
            // 
            this.buttonSpecial.BackColor = System.Drawing.Color.MediumAquamarine;
            this.buttonSpecial.FlatAppearance.BorderColor = System.Drawing.Color.Magenta;
            this.buttonSpecial.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSpecial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSpecial.ForeColor = System.Drawing.Color.White;
            this.buttonSpecial.Location = new System.Drawing.Point(451, 158);
            this.buttonSpecial.Name = "buttonSpecial";
            this.buttonSpecial.Size = new System.Drawing.Size(128, 31);
            this.buttonSpecial.TabIndex = 20;
            this.buttonSpecial.Text = "Aktionen";
            this.buttonSpecial.UseVisualStyleBackColor = false;
            this.buttonSpecial.Click += new System.EventHandler(this.buttonSpecial_Click);
            // 
            // buttonInvoice
            // 
            this.buttonInvoice.BackColor = System.Drawing.Color.MediumAquamarine;
            this.buttonInvoice.FlatAppearance.BorderColor = System.Drawing.Color.Magenta;
            this.buttonInvoice.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonInvoice.ForeColor = System.Drawing.Color.White;
            this.buttonInvoice.Location = new System.Drawing.Point(316, 158);
            this.buttonInvoice.Name = "buttonInvoice";
            this.buttonInvoice.Size = new System.Drawing.Size(128, 31);
            this.buttonInvoice.TabIndex = 21;
            this.buttonInvoice.Text = "Rechnungen";
            this.buttonInvoice.UseVisualStyleBackColor = false;
            this.buttonInvoice.Click += new System.EventHandler(this.buttonInvoice_Click);
            // 
            // buttonOutlet
            // 
            this.buttonOutlet.BackColor = System.Drawing.Color.MediumAquamarine;
            this.buttonOutlet.FlatAppearance.BorderColor = System.Drawing.Color.Magenta;
            this.buttonOutlet.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonOutlet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOutlet.ForeColor = System.Drawing.Color.White;
            this.buttonOutlet.Location = new System.Drawing.Point(587, 158);
            this.buttonOutlet.Name = "buttonOutlet";
            this.buttonOutlet.Size = new System.Drawing.Size(128, 31);
            this.buttonOutlet.TabIndex = 22;
            this.buttonOutlet.Text = "Outlet";
            this.buttonOutlet.UseVisualStyleBackColor = false;
            this.buttonOutlet.Click += new System.EventHandler(this.buttonOutlet_Click);
            // 
            // buttonStack
            // 
            this.buttonStack.BackColor = System.Drawing.Color.MediumAquamarine;
            this.buttonStack.FlatAppearance.BorderColor = System.Drawing.Color.Magenta;
            this.buttonStack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonStack.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStack.ForeColor = System.Drawing.Color.White;
            this.buttonStack.Location = new System.Drawing.Point(722, 158);
            this.buttonStack.Name = "buttonStack";
            this.buttonStack.Size = new System.Drawing.Size(128, 31);
            this.buttonStack.TabIndex = 23;
            this.buttonStack.Text = "Lager";
            this.buttonStack.UseVisualStyleBackColor = false;
            this.buttonStack.Click += new System.EventHandler(this.buttonStack_Click);
            // 
            // panelMainContainer
            // 
            this.panelMainContainer.Location = new System.Drawing.Point(21, 202);
            this.panelMainContainer.Name = "panelMainContainer";
            this.panelMainContainer.Size = new System.Drawing.Size(861, 462);
            this.panelMainContainer.TabIndex = 24;
            // 
            // pictureBoxZoomIn
            // 
            this.pictureBoxZoomIn.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBoxZoomIn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxZoomIn.BackgroundImage")));
            this.pictureBoxZoomIn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBoxZoomIn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxZoomIn.Location = new System.Drawing.Point(797, 99);
            this.pictureBoxZoomIn.Name = "pictureBoxZoomIn";
            this.pictureBoxZoomIn.Size = new System.Drawing.Size(44, 25);
            this.pictureBoxZoomIn.TabIndex = 29;
            this.pictureBoxZoomIn.TabStop = false;
            this.pictureBoxZoomIn.Click += new System.EventHandler(this.pictureBoxZoomIn_Click_1);
            // 
            // pictureBoxZoomOut
            // 
            this.pictureBoxZoomOut.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBoxZoomOut.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxZoomOut.BackgroundImage")));
            this.pictureBoxZoomOut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBoxZoomOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxZoomOut.Location = new System.Drawing.Point(840, 99);
            this.pictureBoxZoomOut.Name = "pictureBoxZoomOut";
            this.pictureBoxZoomOut.Size = new System.Drawing.Size(44, 25);
            this.pictureBoxZoomOut.TabIndex = 28;
            this.pictureBoxZoomOut.TabStop = false;
            this.pictureBoxZoomOut.Click += new System.EventHandler(this.pictureBoxZoomOut_Click_1);
            // 
            // panelHeader
            // 
            this.panelHeader.Controls.Add(this.labelName);
            this.panelHeader.Controls.Add(this.labelWelcome);
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(902, 95);
            this.panelHeader.TabIndex = 30;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.BackColor = System.Drawing.Color.YellowGreen;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.Location = new System.Drawing.Point(682, 59);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(118, 15);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Max Mustermann";
            // 
            // labelWelcome
            // 
            this.labelWelcome.AutoSize = true;
            this.labelWelcome.BackColor = System.Drawing.Color.YellowGreen;
            this.labelWelcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWelcome.Location = new System.Drawing.Point(682, 38);
            this.labelWelcome.Name = "labelWelcome";
            this.labelWelcome.Size = new System.Drawing.Size(124, 15);
            this.labelWelcome.TabIndex = 0;
            this.labelWelcome.Text = "Herzlich Willkommen";
            // 
            // panelFooter
            // 
            this.panelFooter.Location = new System.Drawing.Point(0, 670);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(902, 29);
            this.panelFooter.TabIndex = 31;
            // 
            // FormMainAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(902, 698);
            this.Controls.Add(this.panelFooter);
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.pictureBoxZoomIn);
            this.Controls.Add(this.pictureBoxZoomOut);
            this.Controls.Add(this.buttonSpecial);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.buttonStack);
            this.Controls.Add(this.buttonOutlet);
            this.Controls.Add(this.buttonInvoice);
            this.Controls.Add(this.buttonCustomer);
            this.Controls.Add(this.buttonOrder);
            this.Controls.Add(this.panelMainContainer);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormMainAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LaFamilia | BS2014 | Hauptmenü";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMainAdmin1_FormClosing);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxZoomIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxZoomOut)).EndInit();
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox textBoxSearch;
        private System.Windows.Forms.PictureBox pictureBoxSearch;
        private System.Windows.Forms.Button buttonOrder;
        private System.Windows.Forms.Button buttonCustomer;
        private System.Windows.Forms.Button buttonSpecial;
        private System.Windows.Forms.Button buttonInvoice;
        private System.Windows.Forms.Button buttonOutlet;
        private System.Windows.Forms.Button buttonStack;
        private System.Windows.Forms.Panel panelMainContainer;
        private System.Windows.Forms.PictureBox pictureBoxZoomIn;
        private System.Windows.Forms.PictureBox pictureBoxZoomOut;
        private System.Windows.Forms.PictureBox pictureBoxDelete;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Panel panelFooter;
        private System.Windows.Forms.Label labelWelcome;
				private System.Windows.Forms.Label labelName;
    }
}