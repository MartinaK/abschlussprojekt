﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Controls;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Administration
{
    public partial class FormOrder : Form
    {
        private Order order;

        public FormOrder()
        {
            InitializeComponent();
        }

        public FormOrder(Order order)
        {
            InitializeComponent();

            this.order = order;

            UserControlOrderDetails od = new UserControlOrderDetails(this.order);
            this.panelOrderDetails.Controls.Add(od);
        }
    }
}
