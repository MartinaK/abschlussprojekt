﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;
using LaFamilia.BS2014.Controls;

namespace LaFamilia.BS2014.Administration
{
    public partial class FormMainAdmin : Form
    {
        private Person employee;
        private UserControl[] basisControl = new UserControl[6];
        List<Order> showableOrders = Order.GetList();
        List<Person> showablePersons = Person.GetList();
        List<Invoice> showableInvoices = Invoice.GetList();

        int active_page_nr;

        //*****************************************************************************
        #region constructors

        public FormMainAdmin(Person person)
        {
            InitializeComponent();

            UserControlHeader ucHeader = new UserControlHeader();
            this.panelHeader.Controls.Add(ucHeader);

            UserControlFooter ucFooter = new UserControlFooter();
            this.panelFooter.Controls.Add(ucFooter);


            ResetButtons();
            this.buttonOrder.BackColor = Color.LightSeaGreen;

            if (person != null)
            {
                this.employee = person;
                this.labelName.Text = this.employee.Firstname + " " + this.employee.Lastname;
                this.employee.Lastlogin = DateTime.Now;
                this.employee.Save();
            }

            this.pictureBoxDelete.Enabled = false;
            this.pictureBoxDelete.Hide();

            this.basisControl[0] = new UserControlOrderList(this.showableOrders, this.employee);
            this.panelMainContainer.Controls.Add(this.basisControl[0]);
            this.active_page_nr = 0;
        }

        #endregion

        //*****************************************************************************
        #region private methods

        private List<Order> SetFilterOrderWithSearchText(string text)
        {
            this.showableOrders = Order.GetList();

            if (!String.IsNullOrEmpty(text))
            {
                this.showableOrders = this.showableOrders.Where(o => o.OrderNr.Contains(text)).ToList<Order>();

                if (this.showableOrders.Count == 0)
                    this.showableOrders = Order.GetList().Where(o => o.Person.Lastname.Contains(text)).ToList<Order>();
            }

            return this.showableOrders;
        }

        private List<Person> SetFilterPersonWithSearchText(string text)
        {
            this.showablePersons = Person.GetList();

            if (!String.IsNullOrEmpty(this.textBoxSearch.Text))
            {
                this.showablePersons = this.showablePersons.Where(p => p.IdentificationNumber.Contains(text)).ToList<Person>();

                if (this.showablePersons.Count == 0)
                    this.showablePersons = Person.GetList().Where(p => p.Lastname.Contains(text)).ToList<Person>();
            }

            return this.showablePersons;
        }

        private List<Invoice> SetFilterInvoiceWithSearchText(string text)
        {
            this.showableInvoices = Invoice.GetList();

            if (!String.IsNullOrEmpty(this.textBoxSearch.Text))
            {
                this.showableInvoices = this.showableInvoices.Where(r => r.Order.Person.IdentificationNumber.Contains(text)).ToList<Invoice>();

                while (showableInvoices.Count == 0)
                {
                    this.showableInvoices = Invoice.GetList().Where(r => r.InvoiceNumber.Contains(text)).ToList<Invoice>();
                    this.showableInvoices = Invoice.GetList().Where(r => r.Order.Person.Lastname.Contains(text)).ToList<Invoice>();
                }
            }

            return this.showableInvoices;
        }

        private void ExecuteSearch()
        {
            this.panelMainContainer.Controls.Clear();

            switch (active_page_nr)
            {
                case 0:
                    this.basisControl[active_page_nr] = new UserControlOrderList(this.showableOrders, this.employee);
                    break;
                case 1:
                    this.basisControl[active_page_nr] = new UserControlPersonList(this.showablePersons);
                    break;
                case 2:
                    this.basisControl[active_page_nr] = new UserControlInvoiceList(this.showableInvoices);
                    break;
            }

            this.panelMainContainer.Controls.Add(this.basisControl[active_page_nr]);
        }

        private void ResetButtons()
        {
            this.buttonCustomer.BackColor = Color.FromArgb(98, 201, 195);
            this.buttonInvoice.BackColor = Color.FromArgb(98, 201, 195);
            this.buttonOrder.BackColor = Color.FromArgb(98, 201, 195);
            this.buttonOutlet.BackColor = Color.FromArgb(98, 201, 195);
            this.buttonSpecial.BackColor = Color.FromArgb(98, 201, 195);
            this.buttonStack.BackColor = Color.FromArgb(98, 201, 195);

            this.panelMainContainer.Controls.Clear();
        }

        private void pictureBoxZoomIn_Click_1(object sender, EventArgs e)
        {
            this.Font = new Font("MS Sans Serif", this.Font.Size + 1);
            this.labelName.Font = new Font("MS Sans Serif", this.Font.Size + 1, FontStyle.Bold);
            this.labelWelcome.Font = new Font("MS Sans Serif", this.Font.Size + 1);
            this.buttonCustomer.Font = new Font("MS Sans Serif", this.Font.Size + 1, FontStyle.Bold);
            this.buttonInvoice.Font = new Font("MS Sans Serif", this.Font.Size + 1, FontStyle.Bold);
            this.buttonOrder.Font = new Font("MS Sans Serif", this.Font.Size + 1, FontStyle.Bold);
            this.buttonOutlet.Font = new Font("MS Sans Serif", this.Font.Size + 1, FontStyle.Bold);
            this.buttonSpecial.Font = new Font("MS Sans Serif", this.Font.Size + 1, FontStyle.Bold);
            this.buttonStack.Font = new Font("MS Sans Serif", this.Font.Size + 1, FontStyle.Bold);
        }

        private void pictureBoxZoomOut_Click_1(object sender, EventArgs e)
        {
            this.Font = new Font("MS Sans Serif", this.Font.Size - 1);
            this.labelName.Font = new Font("MS Sans Serif", this.Font.Size - 1, FontStyle.Bold);
            this.labelWelcome.Font = new Font("MS Sans Serif", this.Font.Size - 1);
            this.buttonCustomer.Font = new Font("MS Sans Serif", this.Font.Size - 1, FontStyle.Bold);
            this.buttonInvoice.Font = new Font("MS Sans Serif", this.Font.Size - 1, FontStyle.Bold);
            this.buttonOrder.Font = new Font("MS Sans Serif", this.Font.Size - 1, FontStyle.Bold);
            this.buttonOutlet.Font = new Font("MS Sans Serif", this.Font.Size - 1, FontStyle.Bold);
            this.buttonSpecial.Font = new Font("MS Sans Serif", this.Font.Size - 1, FontStyle.Bold);
            this.buttonStack.Font = new Font("MS Sans Serif", this.Font.Size - 1, FontStyle.Bold);
        }


        #endregion

        private void FormMainAdmin1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        #region buttons

        private void buttonOrder_Click(object sender, EventArgs e)
        {
            ResetButtons();
            this.buttonOrder.BackColor = Color.LightSeaGreen;

            this.basisControl[0] = new UserControlOrderList(this.showableOrders, this.employee);

            this.panelMainContainer.Controls.Add(basisControl[0]);
            this.active_page_nr = 0;
        }

        private void buttonCustomer_Click(object sender, EventArgs e)
        {
            ResetButtons();
            this.buttonCustomer.BackColor = Color.LightSeaGreen;

            this.basisControl[1] = new UserControlPersonList(this.showablePersons);

            this.panelMainContainer.Controls.Add(this.basisControl[1]);
            this.active_page_nr = 1;
        }

        private void buttonInvoice_Click(object sender, EventArgs e)
        {
            ResetButtons();
            this.buttonInvoice.BackColor = Color.LightSeaGreen;

            this.basisControl[2] = new UserControlInvoiceList(this.showableInvoices);

            this.panelMainContainer.Controls.Add(this.basisControl[2]);
            this.active_page_nr = 2;
        }

        private void buttonSpecial_Click(object sender, EventArgs e)
        {
            ResetButtons();
            this.buttonSpecial.BackColor = Color.LightSeaGreen;

            this.basisControl[3] = new UserControlSpecialMain(this.employee, true);

            this.panelMainContainer.Controls.Add(this.basisControl[3]);
            this.active_page_nr = 3;
        }

        private void buttonOutlet_Click(object sender, EventArgs e)
        {
            ResetButtons();
            this.buttonOutlet.BackColor = Color.LightSeaGreen;

            this.basisControl[4] = new UserControlShop(this.employee, true);

            this.panelMainContainer.Controls.Add(this.basisControl[4]);
            this.active_page_nr = 4;
        }

        private void buttonStack_Click(object sender, EventArgs e)
        {
            ResetButtons();
            this.buttonStack.BackColor = Color.LightSeaGreen;

            this.basisControl[5] = new UserControlStack();

            this.panelMainContainer.Controls.Add(this.basisControl[5]);
            this.active_page_nr = 5;
        }

        #endregion

        private void textBoxSearch_Enter(object sender, EventArgs e)
        {
            SetFilterOrderWithSearchText(this.textBoxSearch.Text);
            SetFilterPersonWithSearchText(this.textBoxSearch.Text);
            SetFilterInvoiceWithSearchText(this.textBoxSearch.Text);
            ExecuteSearch();
        }

        private void textBoxSearch_TextChanged(object sender, EventArgs e)
        {
            this.pictureBoxDelete.Enabled = true;
            this.pictureBoxDelete.Show();

            if (active_page_nr == 0)
                SetFilterOrderWithSearchText(this.textBoxSearch.Text);
            ExecuteSearch();
            if (active_page_nr == 1)
                SetFilterPersonWithSearchText(this.textBoxSearch.Text);
            ExecuteSearch();
            if (active_page_nr == 2)
                SetFilterInvoiceWithSearchText(this.textBoxSearch.Text);

            ExecuteSearch();

        }


        private void pictureBoxSearch_Click(object sender, EventArgs e)
        {
            if (this.textBoxSearch.Text.Length >= 1)
            {
                SetFilterOrderWithSearchText(this.textBoxSearch.Text);
                SetFilterPersonWithSearchText(this.textBoxSearch.Text);
                SetFilterInvoiceWithSearchText(this.textBoxSearch.Text);
                ExecuteSearch();
            }

            ExecuteSearch();
        }

        private void pictureBoxDelete_Click(object sender, EventArgs e)
        {
            this.textBoxSearch.Text = String.Empty;
            this.pictureBoxDelete.Enabled = false;
            this.pictureBoxDelete.Hide();
            ExecuteSearch();
        }

    }
}
