﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Administration
{
	public partial class FormNewCategory : Form
	{

		//************************************************************************
		#region constructors

		public FormNewCategory()
		{
			InitializeComponent();
		}

		#endregion

		//************************************************************************
		#region properties

		private Category category;

		public Category Category
		{
			get { return category; }
			set { category = value; }
		}
		

		#endregion

		private void buttonSave_Click(object sender, EventArgs e)
		{
			if (this.category == null)
				this.category = new Category();

			this.category.Title = this.textBoxCatTitle.Text;
			this.DialogResult = DialogResult.OK;
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
		}



	}
}
