﻿namespace LaFamilia.BS2014.Administration
{
    partial class FormSupplierDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
					this.panelHead = new System.Windows.Forms.Panel();
					this.panelFooter = new System.Windows.Forms.Panel();
					this.label1 = new System.Windows.Forms.Label();
					this.textBoxCompany = new System.Windows.Forms.TextBox();
					this.textBoxContact = new System.Windows.Forms.TextBox();
					this.label2 = new System.Windows.Forms.Label();
					this.textBoxPhone = new System.Windows.Forms.TextBox();
					this.label3 = new System.Windows.Forms.Label();
					this.textBoxMail = new System.Windows.Forms.TextBox();
					this.label4 = new System.Windows.Forms.Label();
					this.textBoxStreet = new System.Windows.Forms.TextBox();
					this.label5 = new System.Windows.Forms.Label();
					this.textBoxPlz = new System.Windows.Forms.TextBox();
					this.label6 = new System.Windows.Forms.Label();
					this.textBoxPlace = new System.Windows.Forms.TextBox();
					this.label7 = new System.Windows.Forms.Label();
					this.textBoxCountry = new System.Windows.Forms.TextBox();
					this.label8 = new System.Windows.Forms.Label();
					this.buttonOk = new System.Windows.Forms.Button();
					this.buttonCancel = new System.Windows.Forms.Button();
					this.textBoxSupplyTime = new System.Windows.Forms.TextBox();
					this.label9 = new System.Windows.Forms.Label();
					this.panel1 = new System.Windows.Forms.Panel();
					this.panel1.SuspendLayout();
					this.SuspendLayout();
					// 
					// panelHead
					// 
					this.panelHead.Dock = System.Windows.Forms.DockStyle.Top;
					this.panelHead.Location = new System.Drawing.Point(0, 0);
					this.panelHead.Name = "panelHead";
					this.panelHead.Size = new System.Drawing.Size(495, 110);
					this.panelHead.TabIndex = 54;
					// 
					// panelFooter
					// 
					this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
					this.panelFooter.Location = new System.Drawing.Point(0, 444);
					this.panelFooter.Name = "panelFooter";
					this.panelFooter.Size = new System.Drawing.Size(495, 35);
					this.panelFooter.TabIndex = 55;
					// 
					// label1
					// 
					this.label1.AutoSize = true;
					this.label1.Location = new System.Drawing.Point(12, 123);
					this.label1.Name = "label1";
					this.label1.Size = new System.Drawing.Size(39, 15);
					this.label1.TabIndex = 56;
					this.label1.Text = "Firma";
					// 
					// textBoxCompany
					// 
					this.textBoxCompany.Location = new System.Drawing.Point(12, 141);
					this.textBoxCompany.Name = "textBoxCompany";
					this.textBoxCompany.Size = new System.Drawing.Size(159, 21);
					this.textBoxCompany.TabIndex = 0;
					// 
					// textBoxContact
					// 
					this.textBoxContact.Location = new System.Drawing.Point(12, 199);
					this.textBoxContact.Name = "textBoxContact";
					this.textBoxContact.Size = new System.Drawing.Size(138, 21);
					this.textBoxContact.TabIndex = 1;
					// 
					// label2
					// 
					this.label2.AutoSize = true;
					this.label2.Location = new System.Drawing.Point(12, 181);
					this.label2.Name = "label2";
					this.label2.Size = new System.Drawing.Size(97, 15);
					this.label2.TabIndex = 58;
					this.label2.Text = "Ansprechpartner";
					// 
					// textBoxPhone
					// 
					this.textBoxPhone.Location = new System.Drawing.Point(160, 199);
					this.textBoxPhone.Name = "textBoxPhone";
					this.textBoxPhone.Size = new System.Drawing.Size(131, 21);
					this.textBoxPhone.TabIndex = 2;
					// 
					// label3
					// 
					this.label3.AutoSize = true;
					this.label3.Location = new System.Drawing.Point(160, 181);
					this.label3.Name = "label3";
					this.label3.Size = new System.Drawing.Size(48, 15);
					this.label3.TabIndex = 60;
					this.label3.Text = "Telefon";
					// 
					// textBoxMail
					// 
					this.textBoxMail.Location = new System.Drawing.Point(301, 199);
					this.textBoxMail.Name = "textBoxMail";
					this.textBoxMail.Size = new System.Drawing.Size(168, 21);
					this.textBoxMail.TabIndex = 3;
					// 
					// label4
					// 
					this.label4.AutoSize = true;
					this.label4.Location = new System.Drawing.Point(297, 181);
					this.label4.Name = "label4";
					this.label4.Size = new System.Drawing.Size(31, 15);
					this.label4.TabIndex = 62;
					this.label4.Text = "Mail";
					// 
					// textBoxStreet
					// 
					this.textBoxStreet.Location = new System.Drawing.Point(11, 264);
					this.textBoxStreet.Name = "textBoxStreet";
					this.textBoxStreet.Size = new System.Drawing.Size(280, 21);
					this.textBoxStreet.TabIndex = 4;
					// 
					// label5
					// 
					this.label5.AutoSize = true;
					this.label5.Location = new System.Drawing.Point(12, 245);
					this.label5.Name = "label5";
					this.label5.Size = new System.Drawing.Size(43, 15);
					this.label5.TabIndex = 64;
					this.label5.Text = "Straße";
					// 
					// textBoxPlz
					// 
					this.textBoxPlz.Location = new System.Drawing.Point(11, 309);
					this.textBoxPlz.Name = "textBoxPlz";
					this.textBoxPlz.Size = new System.Drawing.Size(84, 21);
					this.textBoxPlz.TabIndex = 5;
					// 
					// label6
					// 
					this.label6.AutoSize = true;
					this.label6.Location = new System.Drawing.Point(11, 290);
					this.label6.Name = "label6";
					this.label6.Size = new System.Drawing.Size(24, 15);
					this.label6.TabIndex = 66;
					this.label6.Text = "Plz";
					// 
					// textBoxPlace
					// 
					this.textBoxPlace.Location = new System.Drawing.Point(113, 309);
					this.textBoxPlace.Name = "textBoxPlace";
					this.textBoxPlace.Size = new System.Drawing.Size(178, 21);
					this.textBoxPlace.TabIndex = 6;
					// 
					// label7
					// 
					this.label7.AutoSize = true;
					this.label7.Location = new System.Drawing.Point(113, 290);
					this.label7.Name = "label7";
					this.label7.Size = new System.Drawing.Size(23, 15);
					this.label7.TabIndex = 68;
					this.label7.Text = "Ort";
					// 
					// textBoxCountry
					// 
					this.textBoxCountry.Location = new System.Drawing.Point(12, 354);
					this.textBoxCountry.Name = "textBoxCountry";
					this.textBoxCountry.Size = new System.Drawing.Size(159, 21);
					this.textBoxCountry.TabIndex = 7;
					// 
					// label8
					// 
					this.label8.AutoSize = true;
					this.label8.Location = new System.Drawing.Point(12, 335);
					this.label8.Name = "label8";
					this.label8.Size = new System.Drawing.Size(35, 15);
					this.label8.TabIndex = 70;
					this.label8.Text = "Land";
					// 
					// buttonOk
					// 
					this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
					this.buttonOk.Location = new System.Drawing.Point(303, 398);
					this.buttonOk.Name = "buttonOk";
					this.buttonOk.Size = new System.Drawing.Size(87, 27);
					this.buttonOk.TabIndex = 8;
					this.buttonOk.Text = "Ok";
					this.buttonOk.UseVisualStyleBackColor = true;
					this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
					// 
					// buttonCancel
					// 
					this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
					this.buttonCancel.Location = new System.Drawing.Point(396, 398);
					this.buttonCancel.Name = "buttonCancel";
					this.buttonCancel.Size = new System.Drawing.Size(87, 27);
					this.buttonCancel.TabIndex = 9;
					this.buttonCancel.Text = "Abbrechen";
					this.buttonCancel.UseVisualStyleBackColor = true;
					this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
					// 
					// textBoxSupplyTime
					// 
					this.textBoxSupplyTime.Location = new System.Drawing.Point(20, 33);
					this.textBoxSupplyTime.Name = "textBoxSupplyTime";
					this.textBoxSupplyTime.Size = new System.Drawing.Size(133, 21);
					this.textBoxSupplyTime.TabIndex = 0;
					// 
					// label9
					// 
					this.label9.AutoSize = true;
					this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
					this.label9.ForeColor = System.Drawing.Color.LightSeaGreen;
					this.label9.Location = new System.Drawing.Point(16, 15);
					this.label9.Name = "label9";
					this.label9.Size = new System.Drawing.Size(67, 15);
					this.label9.TabIndex = 73;
					this.label9.Text = "Lieferzeit";
					// 
					// panel1
					// 
					this.panel1.BackColor = System.Drawing.SystemColors.Control;
					this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
					this.panel1.Controls.Add(this.textBoxSupplyTime);
					this.panel1.Controls.Add(this.label9);
					this.panel1.Location = new System.Drawing.Point(304, 256);
					this.panel1.Name = "panel1";
					this.panel1.Size = new System.Drawing.Size(165, 74);
					this.panel1.TabIndex = 75;
					// 
					// FormSupplierDetails
					// 
					this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
					this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
					this.ClientSize = new System.Drawing.Size(495, 479);
					this.Controls.Add(this.panel1);
					this.Controls.Add(this.buttonCancel);
					this.Controls.Add(this.buttonOk);
					this.Controls.Add(this.textBoxCountry);
					this.Controls.Add(this.label8);
					this.Controls.Add(this.textBoxPlace);
					this.Controls.Add(this.label7);
					this.Controls.Add(this.textBoxPlz);
					this.Controls.Add(this.label6);
					this.Controls.Add(this.textBoxStreet);
					this.Controls.Add(this.label5);
					this.Controls.Add(this.textBoxMail);
					this.Controls.Add(this.label4);
					this.Controls.Add(this.textBoxPhone);
					this.Controls.Add(this.label3);
					this.Controls.Add(this.textBoxContact);
					this.Controls.Add(this.label2);
					this.Controls.Add(this.textBoxCompany);
					this.Controls.Add(this.label1);
					this.Controls.Add(this.panelFooter);
					this.Controls.Add(this.panelHead);
					this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
					this.Name = "FormSupplierDetails";
					this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
					this.Text = "FormSupplierDetails";
					this.panel1.ResumeLayout(false);
					this.panel1.PerformLayout();
					this.ResumeLayout(false);
					this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelHead;
        private System.Windows.Forms.Panel panelFooter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxCompany;
        private System.Windows.Forms.TextBox textBoxContact;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxPhone;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxMail;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxStreet;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxPlz;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxPlace;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxCountry;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TextBox textBoxSupplyTime;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel1;
    }
}