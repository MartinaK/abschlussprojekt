﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;
using LaFamilia.BS2014.Controls;

namespace LaFamilia.BS2014.Administration
{
	public partial class UserControlStack : UserControl
	{
		FormSupplierDetails supplierDetailsForm;
		private List<Category> catTree;
        private Variant variant;

		//************************************************************************************
		#region constructors

		public UserControlStack()
		{
			InitializeComponent();

			this.catTree = Category.GetList(true);

			BuildCategoryTree(this.treeViewCatFilter.Nodes, catTree);

			LoadArticleData(Article.GetList().Where(a => a.StatusValue == ArticleStatus.Ausverkauft).ToList<Article>());
			LoadSupplierData();

            this.comboBoxStatus.DataSource = Enum.GetValues(typeof(ArticleStatus));
		}

		#endregion

		//************************************************************************************
		#region private methods

		private void BuildCategoryTree(TreeNodeCollection nodes, List<Category> categories)
		{
			TreeNode node = null;
			TreeNode addCatNode = null;

			addCatNode = nodes.Add("Neue Kategorie anlegen...");
			addCatNode.Tag = "NEWCAT";

			foreach (Category cat in categories)
			{
				node = new TreeNode(cat.Title);
				node.Tag = cat;
				nodes.Add(node);
				BuildCategoryTree(node.Nodes, cat.Categories);
			}
		}

		private void LoadArticleData(List<Article> showableArticles)
		{
			ListViewItem item = null;
			this.listViewSoldOut.Items.Clear();

			foreach (Article article in showableArticles)
			{
                foreach (Variant variant in article.Variants)
                {
                    item = new ListViewItem();
                    item.Tag = article;
                    item.Text = article.ArticleNumber;
                    item.SubItems.Add(article.Title);
                    item.SubItems.Add(article.Brand);
                    item.SubItems.Add(variant.AttriVal.ToString());
                    item.SubItems.Add(article.Supplier.Company);
                    item.SubItems.Add(variant.Stack.ToString());
                    this.listViewSoldOut.Items.Add(item);
                }
            }
            this.listViewSoldOut.Sorting = SortOrder.Ascending;
		}

		private void LoadSupplierData()
		{
			ListViewItem item = null;
			this.listViewSuppliers.Items.Clear();

			foreach (Supplier supplier in Supplier.GetList())
			{
				item = new ListViewItem();
				item.Tag = supplier;
				item.Text = supplier.Company;
				item.SubItems.Add(supplier.Contact);
				item.SubItems.Add(supplier.Phone);
				item.SubItems.Add(supplier.Mail);
				this.listViewSuppliers.Items.Add(item);
			}
		}
		#endregion



		//************************************************************************************
		#region events

		private void buttonNewArticle_Click(object sender, EventArgs e)
		{
            if (this.treeViewCatFilter.SelectedNode == null ||this.treeViewCatFilter.SelectedNode.Text == "Neue Kategorie anlegen")
            {
                this.labelNotification.Text = "Bitte wählen Sie eine Kategorie aus!";
                return;
            }

            Category selectedCat =  (Category)this.treeViewCatFilter.SelectedNode.Tag;

			FormArticleDetails newItemForm = new FormArticleDetails(selectedCat);

			if (newItemForm.ShowDialog() == DialogResult.OK)
			{
			}
				
		}

		private void listViewSuppliers_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			ListViewItem item = this.listViewSuppliers.GetItemAt(e.X, e.Y);

			Supplier supplier = (Supplier)item.Tag;

			supplierDetailsForm = new FormSupplierDetails(supplier);
			supplierDetailsForm.ShowDialog();
			this.Hide();

			if (supplierDetailsForm.DialogResult == DialogResult.OK)
				this.Show();
			if (supplierDetailsForm.DialogResult == DialogResult.Cancel)
				this.Show();
		}

		private void buttonNewSupplier_Click(object sender, EventArgs e)
		{
			supplierDetailsForm = new FormSupplierDetails();
			supplierDetailsForm.ShowDialog();
			this.Hide();

			if (supplierDetailsForm.DialogResult == DialogResult.OK)
				this.Show();
			if (supplierDetailsForm.DialogResult == DialogResult.Cancel)
				this.Show();
		}
		#endregion

		private void treeViewCatFilter_AfterSelect(object sender, TreeViewEventArgs e)
		{
            this.labelNotification.Text = String.Empty;
			if (e.Node.Tag is String)
			{
				if (e.Node.Tag as String == "NEWCAT")
				{
					FormNewCategory newCatForm = new FormNewCategory();

					Category category = null;

					if(e.Node.Parent != null)
						category = (Category)e.Node.Parent.Tag;

					if (newCatForm.ShowDialog() == DialogResult.OK)
					{
						newCatForm.Category.ParentId = (category == null ? (decimal?)null : category.CategoryId.Value);
						newCatForm.Category.Save();
						newCatForm.Hide();

						TreeNode node = new TreeNode();
						node.Text = newCatForm.Category.Title;
						node.Tag = newCatForm.Category;

						if (e.Node.Parent == null)
						{
							this.treeViewCatFilter.Nodes.Add(node);
							return;
						}
						e.Node.Parent.Nodes.Add(node);
					}
				}
			}
		}

		private void buttonReorder_Click(object sender, EventArgs e)
		{
            if (this.listViewSoldOut.SelectedItems.Count > 0)
            {
                ListViewItem item = this.listViewSoldOut.SelectedItems[0];
                Article article = (Article)item.Tag;

                string filter = this.listViewSoldOut.SelectedItems[0].SubItems[3].Text;
                foreach (Variant v in article.Variants.Where(v => v.AttriVal == filter))
                {
                    this.variant = v;
                }

                FormReorderArticle reorderArticleForm = new FormReorderArticle(article, this.variant);

                if (reorderArticleForm.ShowDialog() == DialogResult.OK)
                {
                    LoadArticleData(Article.GetList());
                }
                return;
            }
            this.labelNotification.Text = "Bitte wählen Sie zuerst einen Artikel aus!";
		}

        private void comboBoxStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
			ArticleStatus filter = (ArticleStatus)this.comboBoxStatus.SelectedItem;
			LoadArticleData(SetFilter(filter));
		}
        
        //setzt den Filter über den ArticleStatus
        private List<Article> SetFilter(ArticleStatus filter)
        {
             List<Article> showableArticles = Article.GetList();

            if (filter == ArticleStatus.Alle)
            {
                this.comboBoxStatus.Text = "Alle";
                return showableArticles;
            }

            showableArticles = Article.GetList().Where(a => (a.StatusValue & filter) == a.StatusValue).ToList<Article>();
            return showableArticles;
        }
	}
}
