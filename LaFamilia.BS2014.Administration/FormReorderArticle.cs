﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;
using LaFamilia.BS2014.Controls;

namespace LaFamilia.BS2014.Administration
{
    public partial class FormReorderArticle : Form
    {
        private Article article;
        private Variant variant;

        public FormReorderArticle(Article article, Variant variant)
        {
            InitializeComponent();

            this.article = article;
            this.variant = variant;

            UserControlHeader ucHeader = new UserControlHeader();
            this.panelHeader.Controls.Add(ucHeader);

            UserControlFooter ucFooter = new UserControlFooter();
            this.panelFooter.Controls.Add(ucFooter);

            this.textBoxArticleNr.Text = this.article.ArticleNumber;
            this.textBoxArticleTitle.Text = this.article.Title;
            this.textBoxContact.Text = this.article.Supplier.Contact;
            this.textBoxSupplier.Text = this.article.Supplier.Company;
            this.textBoxPhone.Text = this.article.Supplier.Phone;
            this.textBoxSize.Text = this.variant.AttriVal;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            //erhöht das Lieferdatum um die Lieferzeit des Lieferanten
            this.article.SupplyDate = this.article.SupplyDate.Value.AddDays(this.article.Supplier.SupplyTime);
            this.variant.Stack += (int)this.numericUpDownQuantity.Value;
            this.article.Status = ArticleStatus.Nachbestellt.ToString();
            this.article.Save();
            this.variant.Save();
            this.DialogResult = DialogResult.OK;
        }

    }
}