﻿namespace LaFamilia.BS2014.Administration
{
    partial class UserControlStack
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonNewArticle = new System.Windows.Forms.Button();
            this.treeViewCatFilter = new System.Windows.Forms.TreeView();
            this.flowLayoutPanelActualSpecials = new System.Windows.Forms.FlowLayoutPanel();
            this.listViewSoldOut = new System.Windows.Forms.ListView();
            this.columnHeaderArtNr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderTitle = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderBrand = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderSupplier = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panelHeadActualSpecials = new System.Windows.Forms.Panel();
            this.comboBoxStatus = new System.Windows.Forms.ComboBox();
            this.pictureBoxActual = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonReorder = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.listViewSuppliers = new System.Windows.Forms.ListView();
            this.columnHeaderCompany = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderContact = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPhone = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderMail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonNewSupplier = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.labelNotification = new System.Windows.Forms.Label();
            this.flowLayoutPanelActualSpecials.SuspendLayout();
            this.panelHeadActualSpecials.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxActual)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonNewArticle
            // 
            this.buttonNewArticle.BackColor = System.Drawing.Color.MediumVioletRed;
            this.buttonNewArticle.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonNewArticle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewArticle.ForeColor = System.Drawing.Color.White;
            this.buttonNewArticle.Location = new System.Drawing.Point(460, 9);
            this.buttonNewArticle.Name = "buttonNewArticle";
            this.buttonNewArticle.Size = new System.Drawing.Size(131, 23);
            this.buttonNewArticle.TabIndex = 0;
            this.buttonNewArticle.Text = "Neuer Artikel";
            this.buttonNewArticle.UseVisualStyleBackColor = false;
            this.buttonNewArticle.Click += new System.EventHandler(this.buttonNewArticle_Click);
            // 
            // treeViewCatFilter
            // 
            this.treeViewCatFilter.Location = new System.Drawing.Point(8, 38);
            this.treeViewCatFilter.Name = "treeViewCatFilter";
            this.treeViewCatFilter.Size = new System.Drawing.Size(167, 332);
            this.treeViewCatFilter.TabIndex = 1;
            this.treeViewCatFilter.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeViewCatFilter_AfterSelect);
            // 
            // flowLayoutPanelActualSpecials
            // 
            this.flowLayoutPanelActualSpecials.Controls.Add(this.listViewSoldOut);
            this.flowLayoutPanelActualSpecials.Location = new System.Drawing.Point(178, 73);
            this.flowLayoutPanelActualSpecials.Name = "flowLayoutPanelActualSpecials";
            this.flowLayoutPanelActualSpecials.Size = new System.Drawing.Size(559, 158);
            this.flowLayoutPanelActualSpecials.TabIndex = 9;
            // 
            // listViewSoldOut
            // 
            this.listViewSoldOut.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderArtNr,
            this.columnHeaderTitle,
            this.columnHeaderBrand,
            this.columnHeaderSize,
            this.columnHeaderSupplier,
            this.columnHeaderCount});
            this.listViewSoldOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewSoldOut.FullRowSelect = true;
            this.listViewSoldOut.GridLines = true;
            this.listViewSoldOut.Location = new System.Drawing.Point(3, 3);
            this.listViewSoldOut.Name = "listViewSoldOut";
            this.listViewSoldOut.Size = new System.Drawing.Size(547, 146);
            this.listViewSoldOut.TabIndex = 1;
            this.listViewSoldOut.UseCompatibleStateImageBehavior = false;
            this.listViewSoldOut.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderArtNr
            // 
            this.columnHeaderArtNr.Text = "Artikelnr.";
            this.columnHeaderArtNr.Width = 61;
            // 
            // columnHeaderTitle
            // 
            this.columnHeaderTitle.Text = "Artikelname";
            this.columnHeaderTitle.Width = 175;
            // 
            // columnHeaderBrand
            // 
            this.columnHeaderBrand.Text = "Marke";
            this.columnHeaderBrand.Width = 88;
            // 
            // columnHeaderSize
            // 
            this.columnHeaderSize.Text = "Größe";
            // 
            // columnHeaderSupplier
            // 
            this.columnHeaderSupplier.Text = "Lieferant";
            this.columnHeaderSupplier.Width = 103;
            // 
            // columnHeaderCount
            // 
            this.columnHeaderCount.Text = "Anzahl";
            // 
            // panelHeadActualSpecials
            // 
            this.panelHeadActualSpecials.BackColor = System.Drawing.Color.Silver;
            this.panelHeadActualSpecials.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelHeadActualSpecials.Controls.Add(this.comboBoxStatus);
            this.panelHeadActualSpecials.Controls.Add(this.pictureBoxActual);
            this.panelHeadActualSpecials.Controls.Add(this.label1);
            this.panelHeadActualSpecials.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelHeadActualSpecials.Location = new System.Drawing.Point(181, 38);
            this.panelHeadActualSpecials.Name = "panelHeadActualSpecials";
            this.panelHeadActualSpecials.Size = new System.Drawing.Size(547, 38);
            this.panelHeadActualSpecials.TabIndex = 8;
            // 
            // comboBoxStatus
            // 
            this.comboBoxStatus.FormattingEnabled = true;
            this.comboBoxStatus.Location = new System.Drawing.Point(434, 6);
            this.comboBoxStatus.Name = "comboBoxStatus";
            this.comboBoxStatus.Size = new System.Drawing.Size(106, 23);
            this.comboBoxStatus.TabIndex = 2;
            this.comboBoxStatus.Text = "Ausverkauft";
            this.comboBoxStatus.SelectedIndexChanged += new System.EventHandler(this.comboBoxStatus_SelectedIndexChanged);
            // 
            // pictureBoxActual
            // 
            this.pictureBoxActual.Location = new System.Drawing.Point(682, 7);
            this.pictureBoxActual.Name = "pictureBoxActual";
            this.pictureBoxActual.Size = new System.Drawing.Size(36, 20);
            this.pictureBoxActual.TabIndex = 1;
            this.pictureBoxActual.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Alle Artikel";
            // 
            // buttonReorder
            // 
            this.buttonReorder.BackColor = System.Drawing.Color.MediumVioletRed;
            this.buttonReorder.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonReorder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonReorder.ForeColor = System.Drawing.Color.White;
            this.buttonReorder.Location = new System.Drawing.Point(597, 9);
            this.buttonReorder.Name = "buttonReorder";
            this.buttonReorder.Size = new System.Drawing.Size(131, 23);
            this.buttonReorder.TabIndex = 10;
            this.buttonReorder.Text = "Artikel nachbestellen";
            this.buttonReorder.UseVisualStyleBackColor = false;
            this.buttonReorder.Click += new System.EventHandler(this.buttonReorder_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.listViewSuppliers);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(178, 282);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(570, 88);
            this.flowLayoutPanel1.TabIndex = 11;
            // 
            // listViewSuppliers
            // 
            this.listViewSuppliers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderCompany,
            this.columnHeaderContact,
            this.columnHeaderPhone,
            this.columnHeaderMail});
            this.listViewSuppliers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewSuppliers.FullRowSelect = true;
            this.listViewSuppliers.Location = new System.Drawing.Point(3, 3);
            this.listViewSuppliers.Name = "listViewSuppliers";
            this.listViewSuppliers.Size = new System.Drawing.Size(547, 85);
            this.listViewSuppliers.TabIndex = 0;
            this.listViewSuppliers.UseCompatibleStateImageBehavior = false;
            this.listViewSuppliers.View = System.Windows.Forms.View.Details;
            this.listViewSuppliers.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewSuppliers_MouseDoubleClick);
            // 
            // columnHeaderCompany
            // 
            this.columnHeaderCompany.Text = "Firma";
            this.columnHeaderCompany.Width = 110;
            // 
            // columnHeaderContact
            // 
            this.columnHeaderContact.Text = "Ansprechperson";
            this.columnHeaderContact.Width = 150;
            // 
            // columnHeaderPhone
            // 
            this.columnHeaderPhone.Text = "Telefon";
            this.columnHeaderPhone.Width = 150;
            // 
            // columnHeaderMail
            // 
            this.columnHeaderMail.Text = "Mail";
            this.columnHeaderMail.Width = 150;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.buttonNewSupplier);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(181, 248);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(547, 37);
            this.panel1.TabIndex = 10;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(682, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(36, 20);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // buttonNewSupplier
            // 
            this.buttonNewSupplier.BackColor = System.Drawing.Color.MediumVioletRed;
            this.buttonNewSupplier.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonNewSupplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewSupplier.ForeColor = System.Drawing.Color.White;
            this.buttonNewSupplier.Location = new System.Drawing.Point(409, 6);
            this.buttonNewSupplier.Name = "buttonNewSupplier";
            this.buttonNewSupplier.Size = new System.Drawing.Size(131, 22);
            this.buttonNewSupplier.TabIndex = 12;
            this.buttonNewSupplier.Text = "Neuer Lieferant";
            this.buttonNewSupplier.UseVisualStyleBackColor = false;
            this.buttonNewSupplier.Click += new System.EventHandler(this.buttonNewSupplier_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Lieferanten";
            // 
            // labelNotification
            // 
            this.labelNotification.AutoSize = true;
            this.labelNotification.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.labelNotification.Location = new System.Drawing.Point(185, 15);
            this.labelNotification.Name = "labelNotification";
            this.labelNotification.Size = new System.Drawing.Size(0, 13);
            this.labelNotification.TabIndex = 13;
            // 
            // UserControlStack
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelNotification);
            this.Controls.Add(this.treeViewCatFilter);
            this.Controls.Add(this.panelHeadActualSpecials);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.buttonReorder);
            this.Controls.Add(this.flowLayoutPanelActualSpecials);
            this.Controls.Add(this.buttonNewArticle);
            this.Name = "UserControlStack";
            this.Size = new System.Drawing.Size(735, 393);
            this.flowLayoutPanelActualSpecials.ResumeLayout(false);
            this.panelHeadActualSpecials.ResumeLayout(false);
            this.panelHeadActualSpecials.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxActual)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonNewArticle;
        private System.Windows.Forms.TreeView treeViewCatFilter;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelActualSpecials;
        private System.Windows.Forms.Panel panelHeadActualSpecials;
        private System.Windows.Forms.PictureBox pictureBoxActual;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonReorder;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView listViewSuppliers;
        private System.Windows.Forms.ListView listViewSoldOut;
        private System.Windows.Forms.Button buttonNewSupplier;
        private System.Windows.Forms.ComboBox comboBoxStatus;
        private System.Windows.Forms.ColumnHeader columnHeaderArtNr;
        private System.Windows.Forms.ColumnHeader columnHeaderTitle;
        private System.Windows.Forms.ColumnHeader columnHeaderBrand;
        private System.Windows.Forms.ColumnHeader columnHeaderSupplier;
        private System.Windows.Forms.ColumnHeader columnHeaderCount;
        private System.Windows.Forms.ColumnHeader columnHeaderCompany;
        private System.Windows.Forms.ColumnHeader columnHeaderContact;
        private System.Windows.Forms.ColumnHeader columnHeaderPhone;
        private System.Windows.Forms.ColumnHeader columnHeaderMail;
        private System.Windows.Forms.ColumnHeader columnHeaderSize;
        private System.Windows.Forms.Label labelNotification;

    }
}
