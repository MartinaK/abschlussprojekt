﻿using LaFamilia.BS2014.Administration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using LaFamilia.BS2014.Data;
using System.Windows.Forms;
using LaFamilia.BS2014.Controls;


namespace FormPersonTest
{
    
    
    /// <summary>
    ///This is a test class for FormPersonTest and is intended
    ///to contain all FormPersonTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FormPersonTest
    {
        private Person person = null;
        private FormPerson target = null;

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        /// Überprüft ob Admin eingeloggt ist
        /// </summary>
				//[TestMethod()]
				//public void Admin_Returns_True()
				//{
				//  bool admin = true;
				//  this.target = new FormPerson(admin, person);
				//  Assert.IsTrue(admin);
				//}

				//[TestMethod()]
				//public void Fritz()
				//{
				//  bool admin = true;
				//  MessageBox.Show("Im nachfolgenden Dialog muss rechts oben \"hansi\" stehen.");
				//  this.target = new FormPerson(admin, person);
				//  target.ShowDialog();
				//  //Assert.IsTrue(admin);
				//}





    }
}
