﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Controls;
using LaFamilia.BS2014.Data;

namespace LaFamilia.BS2014.Customer
{
    public partial class FormOrders : Form
    {
        private Person person;

        //****************************************************************************
        #region constructors

        public FormOrders(Person person)
        {
            InitializeComponent();

            this.person = person;

            UserControlHeader ucHeader = new UserControlHeader();
            this.panelHeader.Controls.Add(ucHeader);

            UserControlFooter ucFooter = new UserControlFooter();
            this.panelFooter.Controls.Add(ucFooter);

            List<Order> showableOrders = Order.GetList().Where(o => o.PersonId == this.person.PersonId).ToList<Order>();

            UserControlOrderList ucOrderList = new UserControlOrderList(showableOrders);
            this.panelOrderList.Controls.Add(ucOrderList);

        }

        #endregion

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
