﻿namespace LaFamilia.BS2014.Customer
{
    partial class FormMainCustomer
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSpecials = new System.Windows.Forms.Button();
            this.buttonOutlet = new System.Windows.Forms.Button();
            this.panelMainContent = new System.Windows.Forms.Panel();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.labelName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelFooter = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelCartDisplay = new System.Windows.Forms.Label();
            this.pictureBoxShoppingCart = new System.Windows.Forms.PictureBox();
            this.linkLabelCustomerData = new System.Windows.Forms.LinkLabel();
            this.linkLabelOrders = new System.Windows.Forms.LinkLabel();
            this.panelHeader.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShoppingCart)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonSpecials
            // 
            this.buttonSpecials.BackColor = System.Drawing.Color.YellowGreen;
            this.buttonSpecials.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSpecials.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSpecials.ForeColor = System.Drawing.Color.White;
            this.buttonSpecials.Location = new System.Drawing.Point(14, 141);
            this.buttonSpecials.Name = "buttonSpecials";
            this.buttonSpecials.Size = new System.Drawing.Size(143, 30);
            this.buttonSpecials.TabIndex = 25;
            this.buttonSpecials.Text = "Aktionen";
            this.buttonSpecials.UseVisualStyleBackColor = false;
            this.buttonSpecials.Click += new System.EventHandler(this.buttonSpecials_Click);
            // 
            // buttonOutlet
            // 
            this.buttonOutlet.BackColor = System.Drawing.Color.LightSeaGreen;
            this.buttonOutlet.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonOutlet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOutlet.ForeColor = System.Drawing.Color.White;
            this.buttonOutlet.Location = new System.Drawing.Point(161, 141);
            this.buttonOutlet.Name = "buttonOutlet";
            this.buttonOutlet.Size = new System.Drawing.Size(143, 30);
            this.buttonOutlet.TabIndex = 26;
            this.buttonOutlet.Text = "Outlet";
            this.buttonOutlet.UseVisualStyleBackColor = false;
            this.buttonOutlet.Click += new System.EventHandler(this.buttonOutlet_Click);
            // 
            // panelMainContent
            // 
            this.panelMainContent.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelMainContent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMainContent.Location = new System.Drawing.Point(14, 170);
            this.panelMainContent.Name = "panelMainContent";
            this.panelMainContent.Size = new System.Drawing.Size(876, 497);
            this.panelMainContent.TabIndex = 27;
            // 
            // panelHeader
            // 
            this.panelHeader.Controls.Add(this.labelName);
            this.panelHeader.Controls.Add(this.label1);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(902, 93);
            this.panelHeader.TabIndex = 28;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.BackColor = System.Drawing.Color.YellowGreen;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.Location = new System.Drawing.Point(716, 57);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(118, 15);
            this.labelName.TabIndex = 3;
            this.labelName.Text = "Max Mustermann";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.YellowGreen;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(716, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Herzlich Willkommen,";
            // 
            // panelFooter
            // 
            this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFooter.Location = new System.Drawing.Point(0, 673);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(902, 25);
            this.panelFooter.TabIndex = 32;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Controls.Add(this.labelCartDisplay);
            this.panel1.Controls.Add(this.pictureBoxShoppingCart);
            this.panel1.Controls.Add(this.linkLabelCustomerData);
            this.panel1.Controls.Add(this.linkLabelOrders);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel1.Location = new System.Drawing.Point(0, 93);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(902, 42);
            this.panel1.TabIndex = 34;
            // 
            // labelCartDisplay
            // 
            this.labelCartDisplay.Location = new System.Drawing.Point(805, 3);
            this.labelCartDisplay.Name = "labelCartDisplay";
            this.labelCartDisplay.Size = new System.Drawing.Size(94, 36);
            this.labelCartDisplay.TabIndex = 26;
            this.labelCartDisplay.Text = "0 Artikel im Warenkorb";
            this.labelCartDisplay.Click += new System.EventHandler(this.labelCartDisplay_Click);
            // 
            // pictureBoxShoppingCart
            // 
            this.pictureBoxShoppingCart.Image = global::LaFamilia.BS2014.Customer.Properties.Resources.warenkorb;
            this.pictureBoxShoppingCart.Location = new System.Drawing.Point(759, 3);
            this.pictureBoxShoppingCart.Name = "pictureBoxShoppingCart";
            this.pictureBoxShoppingCart.Size = new System.Drawing.Size(40, 33);
            this.pictureBoxShoppingCart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxShoppingCart.TabIndex = 25;
            this.pictureBoxShoppingCart.TabStop = false;
            this.pictureBoxShoppingCart.Click += new System.EventHandler(this.pictureBoxShoppingCart_Click);
            // 
            // linkLabelCustomerData
            // 
            this.linkLabelCustomerData.AutoSize = true;
            this.linkLabelCustomerData.Cursor = System.Windows.Forms.Cursors.Default;
            this.linkLabelCustomerData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelCustomerData.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabelCustomerData.LinkColor = System.Drawing.Color.Black;
            this.linkLabelCustomerData.Location = new System.Drawing.Point(127, 12);
            this.linkLabelCustomerData.Name = "linkLabelCustomerData";
            this.linkLabelCustomerData.Size = new System.Drawing.Size(87, 16);
            this.linkLabelCustomerData.TabIndex = 24;
            this.linkLabelCustomerData.TabStop = true;
            this.linkLabelCustomerData.Text = "Kundendaten";
            this.linkLabelCustomerData.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelCustomerData_LinkClicked);
            this.linkLabelCustomerData.MouseHover += new System.EventHandler(this.linkLabelOrders_MouseHover);
            // 
            // linkLabelOrders
            // 
            this.linkLabelOrders.AutoSize = true;
            this.linkLabelOrders.Cursor = System.Windows.Forms.Cursors.Default;
            this.linkLabelOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelOrders.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabelOrders.LinkColor = System.Drawing.Color.Black;
            this.linkLabelOrders.Location = new System.Drawing.Point(22, 12);
            this.linkLabelOrders.Name = "linkLabelOrders";
            this.linkLabelOrders.Size = new System.Drawing.Size(86, 16);
            this.linkLabelOrders.TabIndex = 23;
            this.linkLabelOrders.TabStop = true;
            this.linkLabelOrders.Text = "Bestellungen";
            this.linkLabelOrders.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelOrders_LinkClicked);
            this.linkLabelOrders.MouseHover += new System.EventHandler(this.linkLabelOrders_MouseHover);
            // 
            // FormMainCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(902, 698);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelFooter);
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.panelMainContent);
            this.Controls.Add(this.buttonOutlet);
            this.Controls.Add(this.buttonSpecials);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormMainCustomer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "La Familia | Dein Shopping Club für die ganze Familie";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMainCustomer_FormClosing);
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShoppingCart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonSpecials;
                private System.Windows.Forms.Button buttonOutlet;
                private System.Windows.Forms.Panel panelMainContent;
                private System.Windows.Forms.Panel panelHeader;
                private System.Windows.Forms.Panel panelFooter;
                private System.Windows.Forms.Label labelName;
                private System.Windows.Forms.Label label1;
                private System.Windows.Forms.Panel panel1;
                private System.Windows.Forms.LinkLabel linkLabelCustomerData;
                private System.Windows.Forms.LinkLabel linkLabelOrders;
                private System.Windows.Forms.PictureBox pictureBoxShoppingCart;
                private System.Windows.Forms.Label labelCartDisplay;
    }
}

