﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LaFamilia.BS2014.Data;
using LaFamilia.BS2014.Controls;

namespace LaFamilia.BS2014.Customer
{
    public partial class FormMainCustomer : Form
    {
        private Person person;
        private UserControl[] basisControl = new UserControl[2];

        private int active_page_nr;

        //*****************************************************************
        #region constructors

        public FormMainCustomer(Person person)
        {
            InitializeComponent();

            this.person = person;

            UserControlHeader ucHeader = new UserControlHeader();
            this.panelHeader.Controls.Add(ucHeader);

            UserControlFooter ucFooter = new UserControlFooter();
            this.panelFooter.Controls.Add(ucFooter);

            this.basisControl[0] = new UserControlSpecialMain(this.person, false);
            this.panelMainContent.Controls.Add(this.basisControl[0]);
            this.active_page_nr = 0;

            if (person != null)
            {
                this.labelName.Text = this.person.Firstname + " " + this.person.Lastname;
                this.person.Lastlogin = DateTime.Now;
                this.person.Save();
            }

            foreach (Order order in Order.GetList().Where(o => o.PersonId == this.person.PersonId && o.Status == OrderStatus.Einkaufswagen.ToString()))
            {
                this.labelCartDisplay.Text = order.ArticlesOrdered.Count() + " Artikel im Warenkorb";
            }
        }

        #endregion

        //******************************************************************
        #region private methods

        private void ShowShoppingCart()
        {
            foreach (Order order in Order.GetList().Where(o => o.PersonId == this.person.PersonId && o.Status == OrderStatus.Einkaufswagen.ToString()))
            {
                FormShoppingCart shoppingCartForm = new FormShoppingCart(order);
                shoppingCartForm.Show();
            }
        }

        #endregion

        //******************************************************************
        #region events

        private void buttonSpecials_Click(object sender, EventArgs e)
        {
            this.buttonSpecials.Font = new Font("MS Sans Serif", 10f, FontStyle.Bold);
            this.buttonOutlet.Font = new Font("MS Sans Serif", 9f, FontStyle.Bold);
            UserControlSpecialMain ucSpecialMain = new UserControlSpecialMain(this.person, false);
            this.panelMainContent.Controls.Clear();
            this.panelMainContent.Controls.Add(ucSpecialMain);
            this.active_page_nr = 0;
        }

        private void buttonOutlet_Click(object sender, EventArgs e)
        {
            this.buttonOutlet.Font = new Font("MS Sans Serif", 10f, FontStyle.Bold);
            this.buttonSpecials.Font = new Font("MS Sans Serif", 9f, FontStyle.Bold);
            UserControlShop ucShop = new UserControlShop(this.person, false);
            this.panelMainContent.Controls.Clear();
            this.panelMainContent.Controls.Add(ucShop);
            this.active_page_nr = 1;
        }

        private void linkLabelCustomerData_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FormPerson personForm = new FormPerson(false, this.person);
            personForm.Show();
        }

        private void linkLabelOrders_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FormOrders ordersForm = new FormOrders(this.person);
            ordersForm.Show();
        }
        #endregion

        private void labelCartDisplay_Click(object sender, EventArgs e)
        {
            ShowShoppingCart();
        }

        private void pictureBoxShoppingCart_Click(object sender, EventArgs e)
        {
            ShowShoppingCart();
        }

        private void linkLabelOrders_MouseHover(object sender, EventArgs e)
        {
            this.linkLabelOrders.ForeColor = Color.MediumVioletRed;
        }

        private void FormMainCustomer_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }


    }
}
