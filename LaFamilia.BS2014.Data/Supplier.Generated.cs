using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;


namespace LaFamilia.BS2014.Data
{

	partial class Supplier
	{
		//***************************************************************************
		#region constants

		private const string TABLE = "bs2014.supplier";
		private const string COLUMNS = "supplier_id, company, contact, phone, mail, supply_time";
		private const string DEFAULTSELECT = "select " + COLUMNS + " from " + TABLE;

		#endregion

		//***************************************************************************
		#region private members


		#endregion

		//***************************************************************************
		#region static methods

		public static List<Supplier> GetList()
		{
            DBConnectionSingleton connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
			return connection.ExecuteQuery<Supplier>(DEFAULTSELECT);
		}
		#endregion

		//***************************************************************************
		#region constructors

		public Supplier() : base()
		{
            this.connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
		}

		public Supplier(DBConnectionSingleton connection, Dictionary<string, object> rowData)	: base(connection, rowData)
		{
		}

		public Supplier(DBItem parent, Dictionary<string, object> rowData): base(parent, rowData)
		{
		}

		public Supplier(DBItem parent) : base(parent)
		{
		}

		#endregion

		//***************************************************************************
		#region properties

		public decimal? SupplierId
		{
			get { return GetValue<decimal?>("supplier_id"); }
		}

		public string Company
		{
			get { return GetValue<string>("company"); }
			set { SetValue("company", value); }
		}

		public string Contact
		{
			get { return GetValue<string>("contact"); }
			set { SetValue("contact", value); }
		}

		public string Phone
		{
			get { return GetValue<string>("phone"); }
			set { SetValue("phone", value); }
		}

		public string Mail
		{
			get { return GetValue<string>("mail"); }
			set { SetValue("mail", value); }
		}

        public int SupplyTime
        {
            get { return GetValue<int>("supply_time"); }
            set { SetValue("supply_time", value); }
        }

		#endregion

		//***************************************************************************
		#region public methods

		public int Save()
		{
			if (this.SupplierId.HasValue)
			{
				return this.connection.ExecuteCommand("update " + TABLE + " set company = :company, contact = :contact, phone = :phone, mail = :mail, supply_time = :supply_time where supplier_id = :supplier_id",
                    new KeyValuePair<string, object>("company", this.Company), 
					new KeyValuePair<string, object>("contact", this.Contact), 
					new KeyValuePair<string, object>("phone", this.Phone), 
					new KeyValuePair<string, object>("mail", this.Mail),
                    new KeyValuePair<string, object>("supply_time", this.SupplyTime),
					new KeyValuePair<string, object>("supplier_id", this.SupplierId));
			}
			else
			{
				SetValue("supplier_id", this.connection.GetId(TABLE));
				return this.connection.ExecuteCommand("insert into " + TABLE + " (" + COLUMNS + " ) values (:supplier_id, :company, :contact, :phone, :mail, :supply_time)",
					new KeyValuePair<string, object>("supplier_id", this.SupplierId),
					new KeyValuePair<string, object>("company", this.Company),
					new KeyValuePair<string, object>("contact", this.Contact),
					new KeyValuePair<string, object>("phone", this.Phone),
					new KeyValuePair<string, object>("mail", this.Mail),
                    new KeyValuePair<string, object>("supply_time", this.SupplyTime));
			}
		}
		public int Delete()
		{
			return this.connection.ExecuteCommand("delete from " + TABLE + " where supplier_id = :supplier_id",
					new KeyValuePair<string, object>("supplier_id", this.SupplierId));
		}

		#endregion

	} // class:Supplier
} // namespace 
