using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;


namespace LaFamilia.BS2014.Data
{
    public enum Country
    {
        Alle,
        Österreich,
        Deutschland,
        Schweiz
    }
	public partial class Address : DBItem
	{
		//***************************************************************************
		#region additional constants


		#endregion

		//***************************************************************************
		#region additional private members

		#endregion

		//***************************************************************************
		#region additional static methods

        public static Address Get(Person person)
        {
            return person.Connection.ExecuteQuery<Address>(person, DEFAULTSELECT + " where PERSON_id = :pid",
                new KeyValuePair<string, object>("pid", person.PersonId.Value))[0];
        }

        public static Address Get(Supplier supplier)
        {
            return supplier.Connection.ExecuteQuery<Address>(supplier, DEFAULTSELECT + " where supplier_id = :pid",
                new KeyValuePair<string, object>("pid", supplier.SupplierId.Value))[0];
        }

		#endregion

		//***************************************************************************
		#region additional constructors


		#endregion

		//***************************************************************************
		#region additional properties
        
		#endregion

	}
}
