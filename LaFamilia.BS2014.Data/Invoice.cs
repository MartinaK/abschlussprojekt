using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;


namespace LaFamilia.BS2014.Data
{
    public enum InvoiceStatus
    {
        Status,
        Offen,
        Bezahlt,
        Mahnung_1,
        Mahnung_2
    }

    public partial class Invoice : DBItem
    {
        //***************************************************************************
        #region additional constants


        #endregion

        //***************************************************************************
        #region additional private members


        #endregion

        //***************************************************************************
        #region additional static methods

        public static Invoice Get(Person person)
        {
            return person.Connection.ExecuteQuery<Invoice>(person, DEFAULTSELECT + " where PERSON_id = :pid",
                new KeyValuePair<string, object>("pid", person.PersonId.Value))[0];
        }


        #endregion

        //***************************************************************************
        #region additional constructors


        #endregion

        //***************************************************************************
        #region additional properties

        public InvoiceStatus StatusValue
        {
            get
            {
                return (InvoiceStatus)(Enum.Parse(typeof(InvoiceStatus), GetValue<string>("status")));
            }
            set
            {
                SetValue("status", value.ToString());
                this.LastUpdate = DateTime.Now;
            }
        }

        public Order Order
        {
            get
            {
                if (this.parent != null && this.parent is Order)
                    return this.parent as Order;

                return LaFamilia.BS2014.Data.Order.Get(this);
            }
        }

        public Person Person
        {
            get
            {
                if (this.parent != null && this.parent is Person)
                    return this.parent as Person;
                return LaFamilia.BS2014.Data.Person.Get(this);
            }
        }

        #endregion

    }
}
