using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;


namespace LaFamilia.BS2014.Data
{

	public partial class Category : DBItem
	{
		//***************************************************************************
		#region additional constants


		#endregion

		//***************************************************************************
		#region additional private members


		#endregion

		//***************************************************************************
		#region additional static methods

		public static List<Category> GetList(bool tree)
		{
			DBConnectionSingleton connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
			List<Category> temp =  connection.ExecuteQuery<Category>(DEFAULTSELECT);

			List<Category> retList = temp.Where(t => !t.ParentId.HasValue).ToList<Category>();
			foreach (Category cat in retList)
			{
				BuildTree(temp, cat);
			}
			return retList;
		}

		private static void BuildTree(List<Category> allCategories, Category parent)
		{
			parent.Categories = allCategories.Where(c => c.ParentId.HasValue && c.ParentId.Value == parent.CategoryId.Value).ToList<Category>();
			foreach (Category cat in parent.Categories)
				BuildTree(allCategories, cat);
		}
		#endregion

		//***************************************************************************
		#region additional constructors


		#endregion

		//***************************************************************************
		#region additional public methods

		public void ArticleAdd(Article article)
		{
			this.Connection.ExecuteCommand("insert into bs2014.article_category (article_id, category_id) values (:aid, :cid)",
				new KeyValuePair<string, object>("aid", article.ArticleId),
				new KeyValuePair<string, object>("cid", this.CategoryId));
		}

		public void ArticleRemove(Article article)
		{
			this.Connection.ExecuteCommand("delete from bs2014.article_category where article_id = :aid and category_id = :cid",
				new KeyValuePair<string, object>("aid", article.ArticleId),
				new KeyValuePair<string, object>("cid", this.CategoryId));
		}
		#endregion

		//***************************************************************************
		#region additional properties

		public List<Category> Categories { get; set; }

		private List<Article> articles = null;

		public Article[] Articles
		{
			get 
			{
				if (this.articles == null)
					this.articles = Article.GetList(this);

				return articles.ToArray(); 
			}
		}
		

		#endregion

	}
}
