using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;


namespace LaFamilia.BS2014.Data
{

	partial class Address
	{
		//***************************************************************************
		#region constants

		private const string TABLE = "bs2014.address";
		private const string COLUMNS = "address_id, person_id, supplier_id, street, postalcode, place, country";
		private const string DEFAULTSELECT = "select " + COLUMNS + " from " + TABLE;

		#endregion

		//***************************************************************************
		#region private members


		#endregion

		//***************************************************************************
		#region static methods

		public static List<Address> GetList()
		{
			DBConnectionSingleton connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
			return connection.ExecuteQuery<Address>(DEFAULTSELECT);
		}
		#endregion

		//***************************************************************************
		#region constructors

		public Address()
			: base()
		{
			this.connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
		}

		public Address(DBConnectionSingleton connection, Dictionary<string, object> rowData)
			: base(connection, rowData)
		{
		}

		public Address(DBItem parent, Dictionary<string, object> rowData)
			: base(parent, rowData)
		{
		}

		public Address(DBItem parent)
			: base(parent)
		{
		}

		#endregion

		//***************************************************************************
		#region properties

		public decimal? AddressId
		{
			get { return GetValue<decimal?>("address_id"); }
		}

		public decimal? PersonId
		{
			get { return GetValue<decimal?>("person_id"); }
			set { SetValue("person_id", value); }
		}

		public decimal? SupplierId
		{
			get { return GetValue<decimal?>("supplier_id"); }
			set { SetValue("supplier_id", value); }
		}


		public string Street
		{
			get { return GetValue<string>("street"); }
			set { SetValue("street", value); }
		}

		public string Postalcode
		{
			get { return GetValue<string>("postalcode"); }
			set { SetValue("postalcode", value); }
		}

		public string Location
		{
			get { return GetValue<string>("place"); }
			set { SetValue("place", value); }
		}

		public string Country
		{
			get { return GetValue<string>("country"); }
			set { SetValue("country", value); }
		}


		#endregion

		//***************************************************************************
		#region public methods

		public int Save()
		{
			if (this.AddressId.HasValue)
			{
				return this.connection.ExecuteCommand("update " + TABLE + " set person_id = :person_id, supplier_id = :supplier_id, street = :street, postalcode = :postalcode, place = :place, country = :country where address_id = :address_id",
	new KeyValuePair<string, object>("person_id", this.PersonId),
						new KeyValuePair<string, object>("supplier_id", this.SupplierId),
						new KeyValuePair<string, object>("street", this.Street),
	new KeyValuePair<string, object>("postalcode", this.Postalcode),
	new KeyValuePair<string, object>("place", this.Location),
	new KeyValuePair<string, object>("country", this.Country),
	new KeyValuePair<string, object>("address_id", this.AddressId));
			}
			else
			{
				SetValue("address_id", this.connection.GetId(TABLE));
				return this.connection.ExecuteCommand("insert into " + TABLE + " (" + COLUMNS + " ) values (:address_id, :person_id, :supplier_id, :street, :postalcode, :place, :country)",
					new KeyValuePair<string, object>("address_id", this.AddressId),
										new KeyValuePair<string, object>("person_id", this.PersonId),
										new KeyValuePair<string, object>("supplier_id", this.SupplierId),
					new KeyValuePair<string, object>("street", this.Street),
					new KeyValuePair<string, object>("postalcode", this.Postalcode),
					new KeyValuePair<string, object>("place", this.Location),
					new KeyValuePair<string, object>("country", this.Country));
			}
		}
		public int Delete()
		{
			return this.connection.ExecuteCommand("delete from " + TABLE + " where address_id = :address_id",
					new KeyValuePair<string, object>("address_id", this.AddressId));
		}

		#endregion

	} // class:Address
} // namespace 
