using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;


namespace LaFamilia.BS2014.Data
{

	partial class Category
	{
		//***************************************************************************
		#region constants

		private const string TABLE = "bs2014.category";
		private const string COLUMNS = "category_id, title, parent_id";
		private const string DEFAULTSELECT = "select " + COLUMNS + " from " + TABLE;

		#endregion

		//***************************************************************************
		#region private members


		#endregion

		//***************************************************************************
		#region static methods

		public static List<Category> GetList()
		{
			DBConnectionSingleton connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
			return connection.ExecuteQuery<Category>(DEFAULTSELECT);
		}
		#endregion

		//***************************************************************************
		#region constructors

		public Category() : base()
		{
            this.connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
		}

		public Category(DBConnectionSingleton connection, Dictionary<string, object> rowData)	: base(connection, rowData)
		{
		}

		public Category(DBItem parent, Dictionary<string, object> rowData): base(parent, rowData)
		{
		}

		public Category(DBItem parent) : base(parent)
		{
		}

		#endregion

		//***************************************************************************
		#region properties

		public decimal? CategoryId
		{
			get { return GetValue<decimal?>("category_id"); }
		}

		public string Title
		{
			get { return GetValue<string>("title"); }
			set { SetValue("title", value); }
		}

		public decimal? ParentId
		{
			get { return GetValue<decimal?>("parent_id"); }
			set { SetValue("parent_id", value); }
		}


		#endregion

		//***************************************************************************
		#region public methods

		public int Save()
		{
			if (this.CategoryId.HasValue)
			{
				return this.connection.ExecuteCommand("update " + TABLE + " set title = :title, parent_id = :parent_id where category_id = :category_id",
					new KeyValuePair<string, object>("title", this.Title), 
					new KeyValuePair<string, object>("parent_id", this.ParentId),
					new KeyValuePair<string, object>("category_id", this.CategoryId));
			}
			else
			{
				SetValue("category_id", this.connection.GetId(TABLE));
				return this.connection.ExecuteCommand("insert into " + TABLE + " (" + COLUMNS + " ) values (:category_id, :title, :parent_id)",
					new KeyValuePair<string, object>("category_id", this.CategoryId),
					new KeyValuePair<string, object>("title", this.Title),
					new KeyValuePair<string, object>("parent_id", this.ParentId));
			}
		}
		public int Delete()
		{
			return this.connection.ExecuteCommand("delete from " + TABLE + " where category_id = :category_id",
					new KeyValuePair<string, object>("category_id", this.CategoryId));
		}

		#endregion

	} // class:Category
} // namespace 
