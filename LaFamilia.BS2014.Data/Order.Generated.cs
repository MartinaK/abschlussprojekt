using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using LaFamilia.Data;


namespace LaFamilia.BS2014.Data
{

	partial class Order
	{
		//***************************************************************************
		#region constants

		private const string TABLE = "bs2014.order";
		private const string COLUMNS = "order_id, person_id, order_date, total_costs, delivery_date, status, order_nr, last_update, name_shortcut";
		private const string DEFAULTSELECT = "select " + COLUMNS + " from " + TABLE;

		#endregion

		//***************************************************************************
		#region private members


		#endregion

		//***************************************************************************
		#region static methods

		public static List<Order> GetList()
		{
			DBConnectionSingleton connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
			return connection.ExecuteQuery<Order>(DEFAULTSELECT);
		}
		#endregion

		//***************************************************************************
		#region constructors

		public Order() : base()
		{
			this.connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
		}

		public Order(DBConnectionSingleton connection, Dictionary<string, object> rowData)	: base(connection, rowData)
		{
		}

		public Order(DBItem parent, Dictionary<string, object> rowData)
			: base(parent, rowData)
		{
		}

		public Order(DBItem parent) : base(parent)
		{
		}

		#endregion

		//***************************************************************************
		#region properties

		public decimal? OrderId
		{
			get { return GetValue<decimal?>("order_id"); }
		}

		public decimal? PersonId
		{
			get { return GetValue<decimal?>("person_id"); }
			set { SetValue("person_id", value); }
		}

		public DateTime? OrderDate
		{
			get { return GetValue<DateTime?>("order_date"); }
			set { SetValue("order_date", value); }
		}

		public decimal TotalCosts
		{
			get { return GetValue<decimal>("total_costs"); }
			set { SetValue("total_costs", value); }
		}

		public DateTime? DeliveryDate
		{
			get { return GetValue<DateTime?>("delivery_date"); }
			set { SetValue("delivery_date", value); }
		}

		public string Status
		{
			get { return GetValue<string>("status"); }
			set { SetValue("status", value); }
		}

		public string OrderNr
		{
			get { return GetValue<string>("order_nr"); }
			set { SetValue("order_nr", value); }
		}

		public DateTime? LastUpdate
		{
			get { return GetValue<DateTime?>("last_update"); }
			set { SetValue("last_update", value); }
		}

		public string NameShortcut
		{
			get { return GetValue<string>("name_shortcut"); }
			set { SetValue("name_shortcut", value); }
		}

		#endregion

		//***************************************************************************
		#region public methods

		public int Save()
		{
			if (this.OrderId.HasValue)
			{
				return this.connection.ExecuteCommand("update " + TABLE + " set person_id = :person_id, order_date = :order_date, total_costs = :total_costs, delivery_date = :delivery_date, status = :status, last_update = :last_update, name_shortcut = :name_shortcut, order_nr = :order_nr where order_id = :order_id",
					new KeyValuePair<string, object>("person_id", this.PersonId), 
					new KeyValuePair<string, object>("order_date", this.OrderDate), 
					new KeyValuePair<string, object>("total_costs", this.TotalCosts), 
					new KeyValuePair<string, object>("delivery_date", this.DeliveryDate), 
					new KeyValuePair<string, object>("status", this.Status), 
					new KeyValuePair<string, object>("order_nr", this.OrderNr),
					new KeyValuePair<string, object>("last_update", this.LastUpdate),
					new KeyValuePair<string, object>("name_shortcut", this.NameShortcut), 
					new KeyValuePair<string, object>("order_id", this.OrderId));
			}
			else
			{
				SetValue("order_id", this.connection.GetId(TABLE));
				return this.connection.ExecuteCommand("insert into " + TABLE + " (" + COLUMNS + " ) values (:order_id, :person_id, :order_date, :total_costs, :delivery_date, :status, :order_nr, :last_update, :name_shortcut)",
					new KeyValuePair<string, object>("order_id", this.OrderId),
					new KeyValuePair<string, object>("person_id", this.PersonId),
					new KeyValuePair<string, object>("order_date", this.OrderDate),
					new KeyValuePair<string, object>("total_costs", this.TotalCosts),
					new KeyValuePair<string, object>("delivery_date", this.DeliveryDate),
					new KeyValuePair<string, object>("status", this.Status),
					new KeyValuePair<string, object>("order_nr", this.OrderNr),
					new KeyValuePair<string, object>("last_update", this.LastUpdate),
					new KeyValuePair<string, object>("name_shortcut", this.NameShortcut));
			}
		}
		public int Delete()
		{
			return this.connection.ExecuteCommand("delete from " + TABLE + " where order_id = :order_id",
					new KeyValuePair<string, object>("order_id", this.OrderId));
		}

		#endregion

	} // class:Order
} // namespace 
