﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;

namespace LaFamilia.BS2014.Data
{
	public class Variant : DBItem
	{
		//***************************************************************************
		#region constants

		protected const string TABLE = "bs2014.variant";
		protected const string COLUMNS = "variant_id, article_id, attribute, attrival, stack";
		protected const string DEFAULTSELECT = "select " + COLUMNS + " from " + TABLE;

		#endregion

		//***************************************************************************
		#region private members


		#endregion

		//***************************************************************************
		#region static methods

		public static List<Variant> GetList(Article article)
		{
			return article.Connection.ExecuteQuery<Variant>(article, DEFAULTSELECT + " where article_id = :aId",
					new KeyValuePair<string, object>("aId", article.ArticleId));
		}

		public static Variant Get(ArticleOrdered articleOrdered)
		{
			if (articleOrdered.VariantId.HasValue)
				return articleOrdered.Connection.ExecuteQuery<Variant>(articleOrdered, DEFAULTSELECT + " where variant_id = :vid",
					new KeyValuePair<string, object>("vid", articleOrdered.VariantId))[0];
			return null;
		}

		#endregion

		//***************************************************************************
		#region constructors

		public Variant()
			: base()
		{
			this.connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
		}

		public Variant(DBConnectionSingleton connection, Dictionary<string, object> rowData)
			: base(connection, rowData)
		{
		}

		public Variant(DBItem parent, Dictionary<string, object> rowData)
			: base(parent, rowData)
		{
		}

		public Variant(DBItem parent)
			: base(parent)
		{
		}

		#endregion

		//***************************************************************************
		#region properties

		public decimal? VariantId
		{
			get { return GetValue<decimal?>("variant_id"); }
		}

		public decimal? ArticleId
		{
			get { return GetValue<decimal?>("article_id"); }
		}

		public string Attribute
		{
			get { return GetValue<string>("attribute"); }
			set { SetValue("attribute", value); }
		}

		public int Stack
		{
			get { return GetValue<int>("stack"); }
			set { SetValue("stack", value); }
		}

		public string AttriVal
		{
			get { return GetValue<string>("attrival"); }
			set { SetValue("attrival", value); }
		}

		#endregion

		//***************************************************************************
		#region public methods

		public int Save()
		{
			if (this.parent != null && this.parent is Article)
				SetValue("article_id", (this.parent as Article).ArticleId);

			if (this.VariantId.HasValue)
			{
				return this.connection.ExecuteCommand("update " + TABLE + " set attribute = :attribute, stack = :stack, attrival = :attrival where variant_id = :variant_id",
						new KeyValuePair<string, object>("attribute", this.Attribute),
						new KeyValuePair<string, object>("stack", this.Stack),
						new KeyValuePair<string, object>("attrival", this.AttriVal),
						new KeyValuePair<string, object>("variant_id", this.VariantId));
			}
			else
			{
				SetValue("variant_id", this.connection.GetId(TABLE));
				return this.connection.ExecuteCommand("insert into " + TABLE + " (" + COLUMNS + " ) values (:variant_id, :article_id, :attribute, :attrival, :stack)",
						new KeyValuePair<string, object>("attribute", this.Attribute),
						new KeyValuePair<string, object>("stack", this.Stack),
						new KeyValuePair<string, object>("attrival", this.AttriVal),
						new KeyValuePair<string, object>("variant_id", this.VariantId),
						new KeyValuePair<string, object>("article_id", this.ArticleId));
			}
		}
		public int Delete()
		{
			return this.connection.ExecuteCommand("delete from " + TABLE + " where variant_id = :variant_id",
							new KeyValuePair<string, object>("variant_id", this.VariantId));
		}


		public void ArticleAdd(Article article)
		{
			this.Connection.ExecuteCommand("insert into bs2014.variant (article_id, variant_id) values (:aid, :vid)",
					new KeyValuePair<string, object>("aid", article.ArticleId),
					new KeyValuePair<string, object>("vid", this.VariantId));
		}

		public override string ToString()
		{
			return this.AttriVal;
		}
		#endregion
	}
}
