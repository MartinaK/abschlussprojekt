using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;

namespace LaFamilia.BS2014.Data
{

	partial class Invoice
	{
		//***************************************************************************
		#region constants

		private const string TABLE = "bs2014.invoice";
		private const string COLUMNS = "invoice_id, person_id, order_id, invoice_number, invoice_date, total_costs, status, name_shortcut, last_update";
		private const string DEFAULTSELECT = "select " + COLUMNS + " from " + TABLE;

		#endregion

		//***************************************************************************
		#region private members


		#endregion

		//***************************************************************************
		#region static methods

		public static List<Invoice> GetList()
		{
            DBConnectionSingleton connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
			return connection.ExecuteQuery<Invoice>(DEFAULTSELECT);
		}
		#endregion

		//***************************************************************************
		#region constructors

		public Invoice() : base()
		{
            this.connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
		}

		public Invoice(DBConnectionSingleton connection, Dictionary<string, object> rowData)	: base(connection, rowData)
		{
		}

		public Invoice(DBItem parent, Dictionary<string, object> rowData): base(parent, rowData)
		{
		}

		public Invoice(DBItem parent) : base(parent)
		{
		}

		#endregion

		//***************************************************************************
		#region properties

		public decimal? InvoiceId
		{
			get { return GetValue<decimal?>("invoice_id"); }
		}

		public decimal? PersonId
		{
			get { return GetValue<decimal?>("person_id"); }
			set { SetValue("person_id", value); }
		}

		public decimal? OrderId
		{
			get { return GetValue<decimal?>("order_id"); }
			set { SetValue("order_id", value); }
		}

		public string InvoiceNumber
		{
			get { return GetValue<string>("invoice_number"); }
			set { SetValue("invoice_number", value); }
		}

		public DateTime? InvoiceDate
		{
			get { return GetValue<DateTime?>("invoice_date"); }
			set { SetValue("invoice_date", value); }
		}

		public decimal? TotalCosts
		{
			get { return GetValue<decimal?>("total_costs"); }
			set { SetValue("total_costs", value); }
		}

		public string Status
		{
			get { return GetValue<string>("status"); }
			set { SetValue("status", value); }
		}

		public string NameShortcut 
		{
			get { return GetValue<string>("name_shortcut"); }
			set { SetValue("name_shortcut", value); }
		}

		public DateTime LastUpdate
		{
			get { return GetValue<DateTime>("last_update"); }
			set { SetValue("last_update", value); }
		}


		#endregion

		//***************************************************************************
		#region public methods

		public int Save()
		{
			if (this.InvoiceId.HasValue)
			{
				return this.connection.ExecuteCommand("update " + TABLE + " set person_id = :person_id, order_id = :order_id, invoice_number = :invoice_number, invoice_date = :invoice_date, total_costs = :total_costs, status = :status, name_shortcut = :name_shortcut, last_update = :last_update where invoice_id = :invoice_id",
					new KeyValuePair<string, object>("person_id", this.PersonId), 
					new KeyValuePair<string, object>("order_id", this.OrderId), 
					new KeyValuePair<string, object>("invoice_number", this.InvoiceNumber), 
					new KeyValuePair<string, object>("invoice_date", this.InvoiceDate), 
					new KeyValuePair<string, object>("total_costs", this.TotalCosts), 
					new KeyValuePair<string, object>("status", this.Status),
 					new KeyValuePair<string, object>("name_shortcut", this.NameShortcut),
					new KeyValuePair<string, object>("last_update", this.LastUpdate),
					new KeyValuePair<string, object>("invoice_id", this.InvoiceId));
			}
			else
			{
				SetValue("invoice_id", this.connection.GetId(TABLE));
				return this.connection.ExecuteCommand("insert into " + TABLE + " (" + COLUMNS + " ) values (:invoice_id, :person_id, :order_id, :invoice_number, :invoice_date, :total_costs, :status, :name_shortcut, :last_update)",
					new KeyValuePair<string, object>("invoice_id", this.InvoiceId),
					new KeyValuePair<string, object>("person_id", this.PersonId),
					new KeyValuePair<string, object>("order_id", this.OrderId),
					new KeyValuePair<string, object>("invoice_number", this.InvoiceNumber),
					new KeyValuePair<string, object>("invoice_date", this.InvoiceDate),
					new KeyValuePair<string, object>("total_costs", this.TotalCosts),
					new KeyValuePair<string, object>("status", this.Status),
					new KeyValuePair<string, object>("name_shortcut", this.NameShortcut),
					new KeyValuePair<string, object>("last_update", this.LastUpdate));
			}
		}
		public int Delete()
		{
			return this.connection.ExecuteCommand("delete from " + TABLE + " where invoice_id = :invoice_id",
					new KeyValuePair<string, object>("invoice_id", this.InvoiceId));
		}

		#endregion

	} // class:Invoice
} // namespace 
