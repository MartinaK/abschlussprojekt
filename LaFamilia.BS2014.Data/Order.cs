using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;


namespace LaFamilia.BS2014.Data
{

    public enum OrderStatus
    {
        Status,
        Einkaufswagen,
        Offen,
        Bearbeitung,
        Versand,
        Rechnung,
        Erledigt,
        Zur�ckgewiesen,
        Storniert
    }

	public partial class Order : DBItem
	{
		//***************************************************************************
		#region additional constants


		#endregion

		//***************************************************************************
		#region additional private members

		#endregion

		//***************************************************************************
		#region additional static methods

		public static Order Get(Invoice invoice)
		{
			return invoice.Connection.ExecuteQuery<Order>(invoice, DEFAULTSELECT + " where Order_id = :id",
					new KeyValuePair<string, object>("id", invoice.OrderId.Value))[0];
		}

		#endregion

		//***************************************************************************
		#region additional constructors


		#endregion

		//***************************************************************************
		#region additional properties

        public OrderStatus StatusValue
        {
            get
            {
                return (OrderStatus)(Enum.Parse(typeof(OrderStatus), GetValue<string>("status")));
            }
            set
            {
                SetValue("status", value.ToString());
                this.LastUpdate = DateTime.Now;
            }
        }

        public Person Person
        {
            get
            {
                if (this.parent != null && this.parent is Person)
                    return this.parent as Person;
                else
                    return LaFamilia.BS2014.Data.Person.Get(this);
            }
        }


        private List<ArticleOrdered> articlesOrdered = null;

        public ArticleOrdered[] ArticlesOrdered
        {
            get
            {
                if (this.articlesOrdered == null)
                    this.articlesOrdered = ArticleOrdered.GetList(this);

                return articlesOrdered.ToArray();
            }
        }

		#endregion

				//***************************************************************************
				#region additional static methods

				public void ArticleAdd(Article article)
				{
					this.Connection.ExecuteCommand("insert into bs2014.article_ordered (article_id, order_id) values (:aid, :oid)",
						new KeyValuePair<string, object>("aid", article.ArticleId),
						new KeyValuePair<string, object>("oid", this.OrderId));
				}

				public void RefreshCollections()
				{
					this.articlesOrdered = ArticleOrdered.GetList(this);
				}
				#endregion
	}
}
