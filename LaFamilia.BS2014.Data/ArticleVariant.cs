﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;

namespace LaFamilia.BS2014.Data
{
    public class ArticleVariant : DBItem
    {

        //***************************************************************************
        #region constants

        private const string TABLE = "bs2014.article_variant";
        private const string COLUMNS = "article_variant_id, article_id";
        private const string DEFAULTSELECT = "select " + COLUMNS + " from " + TABLE;

        #endregion

        //***************************************************************************
        #region constructors

        public ArticleVariant()
            : base()
        {
            this.connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
        }

        public ArticleVariant(DBConnectionSingleton connection, Dictionary<string, object> rowData)
            : base(connection, rowData)
        {
        }

        public ArticleVariant(DBItem parent, Dictionary<string, object> rowData)
            : base(parent, rowData)
        {
        }

        public ArticleVariant(DBItem parent)
            : base(parent)
        {
        }

        #endregion

        //***************************************************************************
        #region static methods

        public static List<ArticleVariant> GetList(Article article)
        {
            return article.Connection.ExecuteQuery<ArticleVariant>(article, DEFAULTSELECT + " where article_id = :aid",
                new KeyValuePair<string, object>("aid", article.ArticleId.Value)).ToList();
        }

        #endregion

        //***************************************************************************
        #region properties

        public decimal? ArticleVariantId
        {
            get { return GetValue<decimal?>("article_variant_id"); }
        }

        public decimal? ArticleId
        {
            get { return GetValue<decimal?>("article_id");}
            set { SetValue("article_id", value); }
        }



        #endregion


        //***************************************************************************
        #region public methods

        public int Save()
        {
            if (this.ArticleVariantId.HasValue)
            {
                return this.connection.ExecuteCommand("update " + TABLE + " set article_id = :article_id where article_variant_id = :article_variant_id",
                    new KeyValuePair<string, object>("article_id", this.ArticleId),
                    new KeyValuePair<string, object>("article_variant_id", this.ArticleVariantId));
            }
            else
            {
                SetValue("article_variant_id", this.connection.GetId(TABLE));
                return this.connection.ExecuteCommand("insert into " + TABLE + " (" + COLUMNS + " ) values (:article_variant_id, :size, :stack, :article_id)",
                    new KeyValuePair<string, object>("article_variant_id", this.ArticleVariantId),
                    new KeyValuePair<string, object>("article_id", this.ArticleId));
            }
        }
        public int Delete()
        {
            return this.connection.ExecuteCommand("delete from " + TABLE + " where article_variant_id = :article_variant_id",
                    new KeyValuePair<string, object>("article_variant_id", this.ArticleVariantId));
        }

        #endregion


        
    }
}
