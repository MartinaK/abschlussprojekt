using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;


namespace LaFamilia.BS2014.Data
{

    public partial class Supplier : DBItem
    {
        //***************************************************************************
        #region additional constants


        #endregion

        //***************************************************************************
        #region additional private members


        #endregion

        //***************************************************************************
        #region additional static methods

        public static Supplier Get(Article article)
        {
            return article.Connection.ExecuteQuery<Supplier>(article, DEFAULTSELECT + " where supplier_id = :id",
                    new KeyValuePair<string, object>("id", article.SupplierId.Value))[0];
        }

        #endregion

        //***************************************************************************
        #region additional constructors


        #endregion

        //***************************************************************************
        #region additional properties

        private Address address = null;

        public Address Address
        {
            get
            {
                if (this.address == null)
                    this.address = Address.Get(this);
                return address;
            }
            set
            {
                address = value;
            }
        }

        #endregion

    }
}
