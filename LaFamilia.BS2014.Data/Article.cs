using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;


namespace LaFamilia.BS2014.Data
{
	public enum ArticleStatus
	{
		Alle,
		Ausverkauft,
		Nachbestellt,
		Verf�gbar
	}

	public partial class Article : DBItem
	{
		//***************************************************************************
		#region additional constants


		#endregion

		//***************************************************************************
		#region additional private members


		#endregion

		//***************************************************************************
		#region additional static methods

		public static List<Article> GetList(Category category)
		{
			string sql = "SELECT a." + COLUMNS.Replace(", ", ", a.") + " FROM " + TABLE + " as a join bs2014.article_category as ac on a.article_id = ac.article_id and ac.category_id = :catId";
			return category.Connection.ExecuteQuery<Article>(category, sql,
					new KeyValuePair<string, object>("catId", category.CategoryId));
		}


		#endregion

		//***************************************************************************
		#region additional constructors


		#endregion

		//***************************************************************************
		#region additional properties

		public ArticleStatus StatusValue
		{
			get
			{
				return (ArticleStatus)(Enum.Parse(typeof(ArticleStatus), GetValue<string>("status")));
			}
			set
			{
				SetValue("status", value.ToString());
			}
		}

		public Image ArticleImg
		{
			get
			{
				if (this.Picture == null)
					return null;

				MemoryStream memStream = new MemoryStream(this.Picture);
				return Image.FromStream(memStream);

			}
			set
			{
				if (value == null)
				{
					this.Picture = null;
					return;
				}

				MemoryStream memStream = new MemoryStream();
				value.Save(memStream, ImageFormat.Png);
				this.Picture = memStream.GetBuffer();

			}
		}

		public Supplier Supplier
		{
			get
			{
				if (this.parent != null && this.parent is Supplier)
					return this.parent as Supplier;
				else
					return LaFamilia.BS2014.Data.Supplier.Get(this);
			}
		}

		private List<Variant> variants = null;

		public Variant[] Variants
		{
			get
			{
				if (this.variants == null)
					this.variants = Variant.GetList(this);
				return variants.ToArray();
			}
		}
		#endregion

		//***************************************************************************
		#region public methods

		public void RefreshCollections()
		{
			this.variants = Variant.GetList(this);
		}

		#endregion

	}
}
