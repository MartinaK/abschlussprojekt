using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;


namespace LaFamilia.BS2014.Data
{

	partial class Special
	{
		//***************************************************************************
		#region constants

		private const string TABLE = "bs2014.special";
		private const string COLUMNS = "special_id, title, description, date_from, date_to, sold_out, picture";
		private const string DEFAULTSELECT = "select " + COLUMNS + " from " + TABLE;

		#endregion

		//***************************************************************************
		#region private members


		#endregion

		//***************************************************************************
		#region static methods

		public static List<Special> GetList()
		{
            DBConnectionSingleton connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
			return connection.ExecuteQuery<Special>(DEFAULTSELECT);
		}
		#endregion

		//***************************************************************************
		#region constructors

		public Special() : base()
		{
            this.connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
		}

		public Special(DBConnectionSingleton connection, Dictionary<string, object> rowData)	: base(connection, rowData)
		{
		}

		public Special(DBItem parent, Dictionary<string, object> rowData): base(parent, rowData)
		{
		}

		public Special(DBItem parent) : base(parent)
		{
		}

		#endregion

		//***************************************************************************
		#region properties

		public decimal? SpecialId
		{
			get { return GetValue<decimal?>("special_id"); }
		}

		public string Title
		{
			get { return GetValue<string>("title"); }
			set { SetValue("title", value); }
		}

		public string Description
		{
			get { return GetValue<string>("description"); }
			set { SetValue("description", value); }
		}

		public DateTime? DateFrom
		{
			get { return GetValue<DateTime?>("date_from"); }
			set { SetValue("date_from", value); }
		}

		public DateTime? DateTo
		{
			get { return GetValue<DateTime?>("date_to"); }
			set { SetValue("date_to", value); }
		}

		public bool? SoldOut
		{
			get { return GetValue<bool?>("sold_out"); }
			set { SetValue("sold_out", value); }
		}

        public byte[] Picture
        {
            get { return GetValue<byte[]>("picture"); }
            set { SetValue("picture", value); }
        }

		#endregion

		//***************************************************************************
		#region public methods

		public int Save()
		{
			if (this.SpecialId.HasValue)
			{
				return this.connection.ExecuteCommand("update " + TABLE + " set title = :title, description = :description, date_from = :date_from, date_to = :date_to, sold_out = :sold_out, picture = :picture where special_id = :special_id",
					new KeyValuePair<string, object>("title", this.Title), 
					new KeyValuePair<string, object>("description", this.Description), 
					new KeyValuePair<string, object>("date_from", this.DateFrom), 
					new KeyValuePair<string, object>("date_to", this.DateTo), 
					new KeyValuePair<string, object>("sold_out", this.SoldOut),
                    new KeyValuePair<string, object>("picture", this.Picture),
					new KeyValuePair<string, object>("special_id", this.SpecialId));
			}
			else
			{
				SetValue("special_id", this.connection.GetId(TABLE));
				return this.connection.ExecuteCommand("insert into " + TABLE + " (" + COLUMNS + " ) values (:special_id, :title, :description, :date_from, :date_to, :sold_out, :picture)",
					new KeyValuePair<string, object>("special_id", this.SpecialId),
					new KeyValuePair<string, object>("title", this.Title),
					new KeyValuePair<string, object>("description", this.Description),
					new KeyValuePair<string, object>("date_from", this.DateFrom),
					new KeyValuePair<string, object>("date_to", this.DateTo),
					new KeyValuePair<string, object>("sold_out", this.SoldOut),
                    new KeyValuePair<string, object>("picture", this.Picture));
			}
		}
		public int Delete()
		{
			return this.connection.ExecuteCommand("delete from " + TABLE + " where special_id = :special_id",
					new KeyValuePair<string, object>("special_id", this.SpecialId));
		}

		#endregion

	} // class:Special
} // namespace 
