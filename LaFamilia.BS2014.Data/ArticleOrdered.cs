﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;

namespace LaFamilia.BS2014.Data
{
	public class ArticleOrdered : Article
	{

//***************************************************************************
		#region constructors

		public ArticleOrdered() : base()
		{
			this.connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
		}

		public ArticleOrdered(DBConnectionSingleton connection, Dictionary<string, object> rowData)	: base(connection, rowData)
		{
		}

		public ArticleOrdered(DBItem parent, Dictionary<string, object> rowData): base(parent, rowData)
		{
		}

		public ArticleOrdered(DBItem parent)
			: base(parent)
		{
		}

		#endregion

		//***************************************************************************
		#region static methods

		public static List<ArticleOrdered> GetList(Order order)
		{
			string sql = "SELECT a." + COLUMNS.Replace(", ", ", a.") + ", oa.quantity, oa.variant_id FROM " + TABLE + " as a join bs2014.article_ordered as oa on a.article_id = oa.article_id and oa.order_id = :orderId";
			return order.Connection.ExecuteQuery<ArticleOrdered>(order, sql,
					new KeyValuePair<string, object>("orderId", order.OrderId));
		}

		#endregion

		//***************************************************************************
		#region properties

		public int Quantity
		{
			get { return GetValue<int>("quantity"); }
			set { SetValue("quantity", value); }
		}

		public Variant Variant
		{
			get { return Variant.Get(this); }
			set { SetValue("variant_id", value.VariantId); }
		}

		public decimal? VariantId
		{
			get
			{
				return GetValue<decimal?>("variant_id");
			}
		}

		#endregion

		//***************************************************************************
		#region public methods

		new public int Save()
		{
			if (this.parent != null && this.parent is Order)
			{
				Order order = this.parent as Order;

				long anz = this.Connection.ExecuteQueryValue<long>("SELECT count(*) FROM bs2014.article_ordered where article_id = :article_id and order_id = :order_id",
						new KeyValuePair<string, object>("article_id", this.ArticleId),
						new KeyValuePair<string, object>("order_id", order.OrderId));


				if (anz == 1)
				{
					return this.connection.ExecuteCommand("update bs2014.article_ordered set quantity = :quantity, variant_id = :variant_id where article_id = :article_id and order_id = :order_id",
						new KeyValuePair<string, object>("article_id", this.ArticleId),
						new KeyValuePair<string, object>("order_id", order.OrderId),
						new KeyValuePair<string, object>("quantity", this.Quantity),
                        new KeyValuePair<string, object>("variant_id", this.VariantId));
				}
				else if (anz == 0)
				{

					return this.connection.ExecuteCommand("insert into bs2014.article_ordered (article_id, order_id, quantity, variant_id) values (:article_id, :order_id, :quantity, :variant_id)",
						new KeyValuePair<string, object>("article_id", this.ArticleId),
						new KeyValuePair<string, object>("order_id", order.OrderId),
						new KeyValuePair<string, object>("quantity", this.Quantity),
                        new KeyValuePair<string, object>("variant_id", this.VariantId));
				}
				
			}
			return 0;
		}
		public int Delete()
		{
			if (this.parent != null && this.parent is Order)
			{
				Order order = this.parent as Order;
				return this.connection.ExecuteCommand("delete from bs2014.article_ordered where article_id = :article_id and order_id = :order_id",
							new KeyValuePair<string, object>("article_id", this.ArticleId),
							new KeyValuePair<string, object>("order_id", order.OrderId));
			}
			return 0;
		}


		#endregion
	}
}


