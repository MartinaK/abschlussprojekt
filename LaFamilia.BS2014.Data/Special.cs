using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;


namespace LaFamilia.BS2014.Data
{

	public partial class Special : DBItem
	{
		//***************************************************************************
		#region additional constants


		#endregion

		//***************************************************************************
		#region additional private members


		#endregion

		//***************************************************************************
		#region additional static methods

		#endregion

		//***************************************************************************
		#region additional constructors


		#endregion

		//***************************************************************************
		#region additional properties

		public Image PictureImg
		{
			get
			{
				if (this.Picture == null)
					return null;
				else
				{
					MemoryStream memStream = new MemoryStream(this.Picture);
					return Image.FromStream(memStream);
				}
			}
			set
			{
				if (value == null)
					this.Picture = null;
				else
				{
					MemoryStream memStream = new MemoryStream();
					value.Save(memStream, ImageFormat.Png);
					this.Picture = memStream.GetBuffer();
				}
			}
		}

		public List<Article> Articles { get; set; }


		#endregion

	}
}
