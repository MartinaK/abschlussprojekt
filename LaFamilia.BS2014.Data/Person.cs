using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;

namespace LaFamilia.BS2014.Data
{
    public enum PersonType
    {
        Alle,
        Mitarbeiter,
        Kunden
    }

    public partial class Person : DBItem
    {
        //***************************************************************************
        #region additional constants


        #endregion

        //***************************************************************************
        #region additional private members


        #endregion

        //***************************************************************************
        #region additional static methods

        public static Person Get(Order order)
        {
            return order.Connection.ExecuteQuery<Person>(order, DEFAULTSELECT + " where PERSON_id = :pid",
                new KeyValuePair<string, object>("pid", order.PersonId.Value))[0];
        }

        public static Person Get(Invoice invoice)
        {
            return invoice.Connection.ExecuteQuery<Person>(invoice, DEFAULTSELECT + " where PERSON_id = :pid",
                new KeyValuePair<string, object>("pid", invoice.PersonId.Value))[0];
        }

        #endregion

        //***************************************************************************
        #region additional constructors


        #endregion

        //***************************************************************************
        #region additional properties


        private Address address = null;

        public Address Address
        {
            get
            {
                if (this.address == null)
                    this.address = Address.Get(this);
                return address;
            }
            set
            {
                address = value;
            }
        }

        #endregion

    }
}
