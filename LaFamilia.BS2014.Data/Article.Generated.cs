using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;



namespace LaFamilia.BS2014.Data
{

    partial class Article
    {
        //***************************************************************************
        #region constants

        protected const string TABLE = "bs2014.article";
        protected const string COLUMNS = "article_id, supplier_id, special_id, article_number, title, image, price, supply_date, brand, type, uvp, status";
        protected const string DEFAULTSELECT = "select " + COLUMNS + " from " + TABLE;

        #endregion

        //***************************************************************************
        #region private members


        #endregion

        //***************************************************************************
        #region static methods

        public static List<Article> GetList()
        {
            DBConnectionSingleton connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
            return connection.ExecuteQuery<Article>(DEFAULTSELECT);
        }
        #endregion

        //***************************************************************************
        #region constructors

        public Article()
            : base()
        {
            this.connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
        }

        public Article(DBConnectionSingleton connection, Dictionary<string, object> rowData)
            : base(connection, rowData)
        {
        }

        public Article(DBItem parent, Dictionary<string, object> rowData)
            : base(parent, rowData)
        {
        }

        public Article(DBItem parent)
            : base(parent)
        {
        }

        #endregion

        //***************************************************************************
        #region properties

        public decimal? ArticleId
        {
            get { return GetValue<decimal?>("article_id"); }
        }

        public decimal? SupplierId
        {
            get { return GetValue<decimal?>("supplier_id"); }
            set { SetValue("supplier_id", value); }
        }

        public decimal? SpecialId
        {
            get { return GetValue<decimal?>("special_id"); }
            set { SetValue("special_id", value); }
        }

        public string ArticleNumber
        {
            get { return GetValue<string>("article_number"); }
            set { SetValue("article_number", value); }
        }

        public string Title
        {
            get { return GetValue<string>("title"); }
            set { SetValue("title", value); }
        }

        public byte[] Picture
        {
            get { return GetValue<byte[]>("image"); }
            set { SetValue("image", value); }
        }

        public decimal Price
        {
            get { return GetValue<decimal>("price"); }
            set { SetValue("price", value); }
        }

        public DateTime? SupplyDate
        {
            get { return GetValue<DateTime?>("supply_date"); }
            set { SetValue("supply_date", value); }
        }

        public string Brand
        {
            get { return GetValue<string>("brand"); }
            set { SetValue("brand", value); }
        }

        public string Type
        {
            get { return GetValue<string>("type"); }
            set { SetValue("type", value); }
        }

        public decimal Uvp
        {
            get { return GetValue<decimal>("uvp"); }
            set { SetValue("uvp", value); }
        }


        public string Status
        {
            get { return GetValue<string>("status"); }
            set { SetValue("status", value); }
        }

        #endregion

        //***************************************************************************
        #region public methods

        public int Save()
        {
            if (this.ArticleId.HasValue)
            {
                return this.connection.ExecuteCommand("update " + TABLE + " set supplier_id = :supplier_id, special_id = :special_id, article_number = :article_number, title = :title, image = :image, price = :price, supply_date = :supply_date, brand = :brand, type = :type, uvp = :uvp, status = :status where article_id = :article_id",
                    new KeyValuePair<string, object>("supplier_id", this.SupplierId),
                    new KeyValuePair<string, object>("special_id", this.SpecialId),
                    new KeyValuePair<string, object>("article_number", this.ArticleNumber),
                    new KeyValuePair<string, object>("title", this.Title),
                    new KeyValuePair<string, object>("image", this.Picture),
                    new KeyValuePair<string, object>("price", this.Price),
                    new KeyValuePair<string, object>("supply_date", this.SupplyDate),
                    new KeyValuePair<string, object>("brand", this.Brand),
                    new KeyValuePair<string, object>("type", this.Type),
                    new KeyValuePair<string, object>("uvp", this.Uvp),
                    new KeyValuePair<string, object>("status", this.Status),
                    new KeyValuePair<string, object>("article_id", this.ArticleId));
            }
            else
            {
                SetValue("article_id", this.connection.GetId(TABLE));
                return this.connection.ExecuteCommand("insert into " + TABLE + " (" + COLUMNS + " ) values (:article_id, :supplier_id, :special_id, :article_number, :title, :image, :price, :supply_date, :brand, :type, :uvp, :status)",
                    new KeyValuePair<string, object>("article_id", this.ArticleId),
                    new KeyValuePair<string, object>("supplier_id", this.SupplierId),
                    new KeyValuePair<string, object>("special_id", this.SpecialId),
                    new KeyValuePair<string, object>("article_number", this.ArticleNumber),
                    new KeyValuePair<string, object>("title", this.Title),
                    new KeyValuePair<string, object>("image", this.Picture),
                    new KeyValuePair<string, object>("price", this.Price),
                    new KeyValuePair<string, object>("supply_date", this.SupplyDate),
                    new KeyValuePair<string, object>("brand", this.Brand),
                    new KeyValuePair<string, object>("type", this.Type),
                    new KeyValuePair<string, object>("uvp", this.Uvp),
                    new KeyValuePair<string, object>("status", this.Status));
            }
        }
        public int Delete()
        {
            return this.connection.ExecuteCommand("delete from " + TABLE + " where article_id = :article_id",
                    new KeyValuePair<string, object>("article_id", this.ArticleId));
        }

        #endregion

    } // class:Article
} // namespace 
