using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LaFamilia.Data;
using System.Configuration;


namespace LaFamilia.BS2014.Data
{

	partial class Person
	{
		//***************************************************************************
		#region constants

		private const string TABLE = "bs2014.person";
		private const string COLUMNS = "person_id, identification_number, firstname, lastname, phone, mail, username, password, lastlogin, is_employee, is_customer, name_shortcut";
		private const string DEFAULTSELECT = "select " + COLUMNS + " from " + TABLE;

		#endregion

		//***************************************************************************
		#region private members


		#endregion

		//***************************************************************************
		#region static methods

		public static List<Person> GetList()
		{
			DBConnectionSingleton connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
			return connection.ExecuteQuery<Person>(DEFAULTSELECT);
		}
		#endregion

		//***************************************************************************
		#region constructors

		public Person() : base()
		{
            this.connection = DBConnectionSingleton.GetDbInstance(ConfigurationManager.AppSettings["ConnectString"]);
		}

		public Person(DBConnectionSingleton connection, Dictionary<string, object> rowData)	: base(connection, rowData)
		{
		}

		public Person(DBItem parent, Dictionary<string, object> rowData): base(parent, rowData)
		{
		}

		public Person(DBItem parent) : base(parent)
		{
		}

		#endregion

		//***************************************************************************
		#region properties

		public decimal? PersonId
		{
			get { return GetValue<decimal?>("person_id"); }
		}

		public string IdentificationNumber
		{
			get { return GetValue<string>("identification_number"); }
			set { SetValue("identification_number", value); }
		}

		public string Firstname
		{
			get { return GetValue<string>("firstname"); }
			set { SetValue("firstname", value); }
		}

		public string Lastname
		{
			get { return GetValue<string>("lastname"); }
			set { SetValue("lastname", value); }
		}

		public string Phone
		{
			get { return GetValue<string>("phone"); }
			set { SetValue("phone", value); }
		}

		public string Mail
		{
			get { return GetValue<string>("mail"); }
			set { SetValue("mail", value); }
		}

		public string Username
		{
			get { return GetValue<string>("username"); }
			set { SetValue("username", value); }
		}

        public string Password
        {
            get { return GetValue<string>("password"); }
            set { SetValue("password", value); }
        }

		public DateTime? Lastlogin
		{
			get { return GetValue<DateTime?>("lastlogin"); }
			set { SetValue("lastlogin", value); }
		}

		public bool? IsEmployee
		{
			get { return GetValue<bool?>("is_employee"); }
			set { SetValue("is_employee", value); }
		}

		public bool? IsCustomer
		{
			get { return GetValue<bool?>("is_customer"); }
			set { SetValue("is_customer", value); }
		}

        public string NameShortcut
        {
            get { return GetValue<string>("name_shortcut"); }
            set { SetValue("name_shortcut", value); }
        }

		#endregion

		//***************************************************************************
		#region public methods

		public int Save()
		{
			if (this.PersonId.HasValue)
			{
				return this.connection.ExecuteCommand("update " + TABLE + " set identification_number = :identification_number, firstname = :firstname, lastname = :lastname, phone = :phone, mail = :mail, username = :username, password = :password, lastlogin = :lastlogin, is_employee = :is_employee, is_customer = :is_customer, name_shortcut = :name_shortcut where person_id = :person_id",
					new KeyValuePair<string, object>("identification_number", this.IdentificationNumber), 
					new KeyValuePair<string, object>("firstname", this.Firstname), 
					new KeyValuePair<string, object>("lastname", this.Lastname), 
					new KeyValuePair<string, object>("phone", this.Phone), 
					new KeyValuePair<string, object>("mail", this.Mail), 
					new KeyValuePair<string, object>("username", this.Username), 
					new KeyValuePair<string, object>("password", this.Password), 
					new KeyValuePair<string, object>("lastlogin", this.Lastlogin), 
					new KeyValuePair<string, object>("is_employee", this.IsEmployee), 
					new KeyValuePair<string, object>("is_customer", this.IsCustomer), 
                    new KeyValuePair<string, object>("name_shortcut", this.NameShortcut),
					new KeyValuePair<string, object>("person_id", this.PersonId));
			}
			else
			{
				SetValue("person_id", this.connection.GetId(TABLE));
				return this.connection.ExecuteCommand("insert into " + TABLE + " (" + COLUMNS + " ) values (:person_id, :identification_number, :firstname, :lastname, :phone, :mail, :username, :password, :lastlogin, :is_employee, :is_customer, :name_shortcut)",
					new KeyValuePair<string, object>("person_id", this.PersonId),
					new KeyValuePair<string, object>("identification_number", this.IdentificationNumber),
					new KeyValuePair<string, object>("firstname", this.Firstname),
					new KeyValuePair<string, object>("lastname", this.Lastname),
					new KeyValuePair<string, object>("phone", this.Phone),
					new KeyValuePair<string, object>("mail", this.Mail),
					new KeyValuePair<string, object>("username", this.Username),
					new KeyValuePair<string, object>("password", this.Password),
					new KeyValuePair<string, object>("lastlogin", this.Lastlogin),
					new KeyValuePair<string, object>("is_employee", this.IsEmployee),
					new KeyValuePair<string, object>("is_customer", this.IsCustomer),
                    new KeyValuePair<string, object>("name_shortcut", this.NameShortcut));
			}
		}
		public int Delete()
		{
			return this.connection.ExecuteCommand("delete from " + TABLE + " where person_id = :person_id",
					new KeyValuePair<string, object>("person_id", this.PersonId));
		}

		#endregion

	} // class:Person
} // namespace 
